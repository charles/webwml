#use wml::debian::template title="Proyecto de documentación de Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="a63b4ba503c2c880b94edc8f521b5d8f769fa2c6"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#ddp_work">Nuestro trabajo</a></li>
</ul>


<aside>
<p><span class="fas fa-caret-right fa-3x"></span> El proyecto de documentación de Debian (DDP) se ocupa de la documentación de Debian como, por ejemplo, los manuales de usuario y desarrollador, varias guías («handbooks»), las FAQ y las notas de publicación. Esta página ofrece una visión general del trabajo del DDP.</p>
</aside>

<h2><a id="ddp_work">Nuestro trabajo</h2>

<p>
<div class="line">
  <div class="item col50">

    <h3>Manuales</h3>
    <ul>
      <li><strong><a href="user-manuals">Manuales de usuario</a></strong></li>
      <li><strong><a href="devel-manuals">Manuales de desarrollador</a></strong></li>
      <li><strong><a href="misc-manuals">Manuales varios</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Normativa de la documentación</h3>
    <ul>
      <li>La licencia de los manuales ha de cumplir las DFSG.</li>
      <li>Usamos restructuredText o Docbook XML (obsoleto) para nuestros documentos.</li>
      <li>Todos los fuentes están disponibles en nuestro <a href="https://salsa.debian.org/ddp-team">repositorio Git</a>.</li>
      <li>La URL oficial será <tt>www.debian.org/doc/&lt;nombre-del-manual&gt;</tt>.</li>
      <li>Todos los documentos deberían mantenerse actualizados.</li>
    </ul>

    <h3>Acceso mediante Git</h3>
    <ul>
      <li>Cómo acceder al <a href="vcs">repositorio Git del DDP</a>.</li>
    </ul>

  </div>


</div>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> Póngase en contacto con el equipo en <a href="https://lists.debian.org/debian-doc/">debian-doc</a></button></p>
