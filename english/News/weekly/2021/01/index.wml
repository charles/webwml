#use wml::debian::projectnews::header PUBDATE="2021-03-18" SUMMARY="Welcome to the DPN, DPL, Freeze Softly, Secure GRUB2, Homeworld Theme, RC Bugs, Website update, APT2.2, QEMU, mime types, rust-coreutils, debuginfod, Google and Debian, Freexian funding, New signing keys, BSPs, and Contributors."
#use wml::debian::acronyms

# Status: [published]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<shortintro issue="first"/>

<h2>Debian Project Leader Elections 2021</h2>

<p>The <a href="https://lists.debian.org/debian-devel-announce/2021/03/msg00001.html"> 
call for nominations</a> has been announced for the election 
of the <a href="https://www.debian.org/devel/leader">Debian Project Leader</a>. 
The election process starts six weeks prior to a 
post vacancy, with the term starting on 2021-04-21. The process proceeds as follows:
Nominations are accepted March 7 through March 13, Campaigns run Sunday 
March 14 through April 3, and Voting begins April 4 through April 17.</p>


<h2>Bullseye Soft Freeze</h2>

<p>
The Release Team announced that <a href="https://lists.debian.org/debian-devel-announce/2021/02/msg00002.html">
bullseye reached the soft freeze milestone on 2021-02-12</a>. This freeze 
allows only small targeted fixes for the next release. New transitions or 
versions of packages that may be disruptive are no longer allowed. You can 
follow the freeze schedule on the <a href="https://release.debian.org/bullseye/freeze_policy.html">Bullseye Freeze 
Timeline and Policy</a> page.
</p>

<introtoc/>

<toc-display/>

<toc-add-entry name="security">Important Debian Security Advisories</toc-add-entry>

<p> Debian's Security Team releases current advisories on a daily basis
(<a href="$(HOME)/security/2021/">Security Advisories 2021</a>). Please
read them carefully and subscribe to the <a href="https://lists.debian.org/debian-security-announce/">security mailing
list</a> to keep your systems updated against any vulnerabilities.</p>

## Pull the below data directly from $(HOME)/security/2021/. Swap out the 
## dsa-XXXX for the current advisory # , be sure to change the name as well. 

<p>Some recently released advisories concern these packages:
<a href="$(HOME)/security/2021/dsa-4870">pygments</a>,
<a href="$(HOME)/security/2021/dsa-4869">tiff</a>,
<a href="$(HOME)/security/2021/dsa-4868">flatpack</a>,
<a href="$(HOME)/security/2021/dsa-4867">grub2</a>,
<a href="$(HOME)/security/2021/dsa-4866">thunderbird</a>,
<a href="$(HOME)/security/2021/dsa-4865">docker.io</a>,
<a href="$(HOME)/security/2021/dsa-4862">firefox</a>,
<a href="$(HOME)/security/2021/dsa-4861">screen</a>,
<a href="$(HOME)/security/2021/dsa-4858">chromium</a>,
<a href="$(HOME)/security/2021/dsa-4854">webkit2gtk</a>,
<a href="$(HOME)/security/2021/dsa-4852">openvswitch</a>,
<a href="$(HOME)/security/2021/dsa-4849">firejail</a>, and
<a href="$(HOME)/security/2021/dsa-4847">connman</a>.
</p>

<p>The Debian website also <a href="https://www.debian.org/lts/security/">archives</a>
the security advisories issued by the Debian Long Term Support team and posted to the
<a href="https://lists.debian.org/debian-lts-announce/">debian-lts-announce mailing list</a>.
</p>

<toc-add-entry name="secureboot">Securing GRUB2 UEFI SecureBoot 2021</toc-add-entry>

<p>Since the <a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot">BootHole</a>
group of bugs announced in GRUB2 in July 2020, security researchers and
developers in Debian and elsewhere have continued to look for further issues
that might allow for circumvention of UEFI Secure Boot. Several more have been
found. See <a href="https://www.debian.org/security/2021/dsa-4867">Debian Security Advisory 4867-1</a>
for more complete details. Debian published a very informative
<a href="https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot/">statement</a>
aiming to explain the consequences of these security vulnerabilities, and what
steps have been taken to address them.
</p>

<toc-add-entry name="bullseye">News on Debian <q>bullseye</q></toc-add-entry>
## The first sub section is dedicated to news about bullseye in order to 
## build anticipation for our $Next_Release. 

## As we get closer to the release date, this section will home the 
## Release-Critical bugs report. 

<p><b>Homeworld, the default artwork and theme for <q>bullseye</q></b></p>
<p>
We send congratulations to Juliette Taka for her winning submission of
<a href="https://wiki.debian.org/DebianArt/Themes/Homeworld">Homeworld</a>, 
which will be the default theme and artwork for Debian 11 <q>bullseye</q>. Over
5,613 votes were cast between <a 
href="https://wiki.debian.org/DebianDesktop/Artwork/Bullseye">18
submissions</a>. We thank all who contributed and voted in the process which
was a delightful refresher showing how much of a community Debian truly is.
</p>

<p>Release Critical Bug report for Week 11</p>

<p>The <a href="https://udd.debian.org/bugs.cgi">bug webinterface of the
Ultimate Debian Database</a>
currently knows about the following release critical bugs:</p>
<table>
  <tr><th>In Total:</th><td><a href="https://udd.debian.org/bugs.cgi?release=any&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">1110</a></td></tr>
  <tr><th>Affecting Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">216</a></td></tr>
  <tr><th>Bullseye Only:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_not_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">51</a></td></tr>
  <tr><th>Remaining to Be Fixed in Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>165</b></a></td></tr>
</table>

<p>Of these <b><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">165</a></b> bugs, the following tags are set:</p>

<table>
  <tr><th>Pending in Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=only&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">4</a></td></tr>
  <tr><th>Patched in Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=only&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">23</a></td></tr>
  <tr><th>Duplicates in Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=only&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">10</a></td></tr>
  <tr><th>Can Be Fixed in a Security Update:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=only&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">15</a></td></tr>
  <tr><th>Contrib or Non-free in Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=only&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">4</a></td></tr>
  <tr><th>Claimed in Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=only&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">0</a></td></tr>
  <tr><th>Delayed in Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=only&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">1</a></td></tr>
  <tr><th>Otherwise Fixed in Bullseye:</th><td><a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=&amp;pending=&amp;security=&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=&amp;done=only&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1">8</a></td></tr>
</table>

<p>Ignoring all the above (multiple tags possible) <a href="https://udd.debian.org/bugs.cgi?release=bullseye_and_sid&amp;patch=ign&amp;pending=ign&amp;security=ign&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=ign&amp;deferred=ign&amp;notmain=ign&amp;notbullseye=&amp;base=&amp;standard=&amp;merged=ign&amp;done=ign&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>111</b></a>
bugs need to be fixed by Debian Contributors to get Debian 11.0 <q>bullseye</q> released.</p>

<p>However, in the view of the Release Managers,
<a href="https://udd.debian.org/bugs.cgi?release=bullseye&amp;patch=&amp;pending=&amp;security=ign&amp;wontfix=&amp;upstream=&amp;unreproducible=&amp;forwarded=&amp;claimed=&amp;deferred=&amp;notmain=ign&amp;notbullseye=ign&amp;base=&amp;standard=&amp;merged=ign&amp;done=&amp;outdatedbullseye=&amp;outdatedsid=&amp;needmig=&amp;newerubuntu=&amp;fnewer=&amp;fnewerval=7&amp;rc=1&amp;sortby=source&amp;sorto=asc&amp;cpopcon=1&amp;cseverity=1&amp;ctags=1"><b>182</b></a>
need to be dealt with for the release to happen.</p>

<p>Please see <q><a
href="https://wiki.debian.org/ProjectNews/RC-Stats">Interpreting the
release critical bug statistics</a></q> for an explanation of the different numbers.</p>

<toc-add-entry name="web">New Website Design</toc-add-entry>

<p>We are very proud of our WWW team and their work in <a href="https://bits.debian.org/2020/12/debian-web-new-homepage.html">updating</a> the 
 <a href="https://www.debian.org">Debian website</a> into a modern streamlined 
look. Take a peek! This is just the beginning of the process as we continue to remove outdated 
information, update the site with new information, and improve the overall end 
user experience. As always more hands and eyes are helpful, let us know if you 
would care to contribute to this new chapter in our development.</p>

<toc-add-entry name="apt">apt-2.2</toc-add-entry>

<p>Julian Andres Klode <a href="https://blog.jak-linux.org/2021/02/18/apt-2.2/">shared</a> that APT 2.2 
has been released. New features include <i>--error-on=any</i> and <i>rred</i> as 
a standalone to merge pdiff files.</p>


<toc-add-entry name="keys">New archive signing keys</toc-add-entry>

<p>The new archive signing keys for Debian 11 have been generated for future use 
(shortly). The keys will be included in Debian 11, <q>bullseye</q> and for future 
point releases for Debian 10, <q>buster</q>. The keys will go into use at the release 
of bullseye or the expiry of the old keys on 2027-04-12.</p>

The new keys are:

<table>
<tr><th>pub</th>   <th>rsa4096 2021-01-17 [SC] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>1F89 983E 0081 FDE0 18F3  CC96 73A4 F27B 8DD4 7936</th></tr>
<tr><th>uid</th>   <th>Debian Archive Automatic Signing Key (11/bullseye) &#60;ftpmaster@debian.org&#62;</th></tr>
<tr><th>sub</th>   <th> rsa4096 2021-01-17 [S] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>A723 6886 F3CC CAAD 148A  27F8 0E98 404D 386F A1D9</th></tr>
</table>


<table>
<tr><th>pub</th>   <th>rsa4096 2021-01-17 [SC] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>AC53 0D52 0F2F 3269 F5E9  8313 A484 4904 4AAD 5C5D</th></tr>
<tr><th>uid</th>   <th>Debian Security Archive Automatic Signing Key (11/bullseye) &#60;ftpmaster@debian.org&#62;</th></tr>
<tr><th>sub</th>   <th>rsa4096 2021-01-17 [S] [expires: 2029-01-15]</th></tr>
<tr><th>   </th>   <th>ED54 1312 A33F 1128 F10B  1C6C 5440 4762 BBB6 E853</th></tr>
</table>

<p>
<b>Keys:</b>
<p><a href="https://ftp-master.debian.org/keys/archive-key-11.asc">https://ftp-master.debian.org/keys/archive-key-11.asc</a></p>
<p><a href="https://ftp-master.debian.org/keys/archive-key-11-security.asc">https://ftp-master.debian.org/keys/archive-key-11-security.asc</a></p>

<toc-add-entry name="dqib">Debian Quick Image Baker pre-baked images are available</toc-add-entry>
<p>DQIB (Debian Quick Image Baker) provides weekly-generated 
<a href="https://people.debian.org/~gio/dqib/">Debian QEMU sid images</a> for many architectures. Each download provides a root 
filesystem, kernel, initrd, and a README sample QEMU command that will launch 
the image with information about how to log in.

<toc-add-entry name="other">Other items of interest</toc-add-entry> 
<ul>
<li>Charles Plessy <a href="https://lists.debian.org/debian-devel/2021/01/msg00057.html">added 
hundreds of media types to /etc/mime.types</a>.</li>
<li>New mailing list: <a href="https://lists.debian.org/debian-localgroups/">debian-localgroups@lists.debian.org</a> has been set up to facilitate communication 
and provide support for local events and local activities.</li>
</ul>

<toc-add-entry name="rust">Debian running on Rust coreutils</toc-add-entry>
<p>Sylvestre Ledru <a href="https://sylvestre.ledru.info/blog/2021/03/09/debian-running-on-rust-coreutils">shared details</a>
of the working of <a href="https://tracker.debian.org/pkg/rust-coreutils">rust-coreutils</a> in
Debian.  The implementation is able to support and boot with GNOME, install the
top 1,000 packages, and build Firefox.
</p>

<toc-add-entry name="debuginfod">New service: debuginfod</toc-add-entry>
<p>Sergio Durigan Junior announced a <a href="https://wiki.debian.org/Debuginfod">debuginfod service in Debian</a>. <a 
href="https://sourceware.org/elfutils/Debuginfod.html">debuginfod</a> allows 
developers to forego the need to install debuginfo 
packages to debug software. It works as a client/server to provide 
debugger tools over HTTP.
</p>




####Here is where we move to EXTERNAL NEWS about Debian####
## News about Debian, a Debian influence in F/OSS, F/OSS, mentions of Debian 
## in the news or other media.
<toc-add-entry name="bazel">Google and Debian collaborate to package the 
Bazel build system in Debian</toc-add-entry>

<p>
Debian Developer Olek Wojnar and Google Software Engineer Yun Peng worked with 
the <a href="https://blog.bazel.build/2021/03/04/bazel-debian-packaging.html">Bazel 
team</a> to package 
<a href="https://opensourcelive.withgoogle.com/events/bazel/register?after-register=%2Fevents%2Fbazel%2Fwatch%3Ftalk%3Ddebian-ubuntu-packaging">Bazel
in Debian</a> toward assisting the medical community with COVID-19 research. Olek 
shares some of the technical challenges and details of the project in a 
<a href="https://www.youtube.com/watch?v=jLSgky4ISj0&amp;t=23s">video talk.</a></p>


<toc-add-entry name="freexian">Freexian to provide funding for some Debian projects</toc-add-entry>

<p>
Raphael Hertzog shared details of Freexian's LTS intention to 
<a href="https://lists.debian.org/debian-project/2020/11/msg00002.html"> provide funding</a> 
for some Debian projects with a portion of the money collected from 
its own sponsors. This generous contribution will allow teams to propose 
requests for funding within their spheres which overall will give back to the 
entire community. 
</p>

<toc-add-entry name="events">BSPs, Events, MiniDebCamps, and MiniDebConfs</toc-add-entry>

<p><b>Bug Squashing Parties</b></p>

<p><b>Upcoming events</b></p>

<p>
There will be a <a href="https://lists.debian.org/debian-devel-announce/2021/02/msg00000.html">Virtual Bug Squashing Party in Salzburg/Austria</a> held April 24&ndash;25 2021.
Some details are still in the planning stage, for now, please  save the date.</p>

<p><b>Past events</b></p>

<p>
The Debian Brazil community held <a href="https://mdcobr2020.debian.net/">MiniDebConf Online Brasil 2020</a>,  
November 28&ndash;29, 2020. Talks from the event in Portuguese available for
<a href="https://peertube.debian.social/videos/watch/playlist/875b2409-2d73-4993-8471-2923c27b8a7e"> viewing.</a>
</p>

<p>The Debian India community held <a href="https://in2021.mini.debconf.org/">MiniDebConf Online India 2021</a>, 
January 23&ndash;24, 2021. Talks were available in over 6 languages, with around 45 
total events from talks, to BOFs and Workshops. Talks and <a 
href="https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-India/">Video</a> 
from the event are available for viewing.  
</p>


<toc-add-entry name="reports">Reports</toc-add-entry>

## Team and progress reports. Consider placing reports from teams here rather 
## than inside of the internal news sections. 



## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#


<p><b>LTS Freexian Monthly Reports</b></p>

<p>Freexian issues <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">monthly reports</a>
about the work of paid contributors to Debian Long Term Support.
</p>

<p><b>Reproducible Builds status update</b></p>

<p>Follow the <a
href="https://reproducible-builds.org/blog/">Reproducible
Builds blog</a> to get the weekly reports on their work in the <q>buster</q> cycle.
</p>


<p><b>Packages needing help:</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2021/03/msg00063.html"
        orphaned="1204"
        rfa="209" />

<p><b>Newcomer bugs</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
Debian has a <q>newcomer</q> bug tag, used to indicate bugs which are suitable for new
contributors to use as an entry point to working on specific packages.

There are currently <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">189</a>
bugs available tagged <q>newcomer</q>.
</p>


<toc-add-entry name="code">Code, coders, and contributors</toc-add-entry>
<p><b>New Package Maintainers since 9 september 2020</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD
<p>
Please welcome: Adrien Nayrat, Georgy Komarov, Alex Doyle, Johann Queuniet,
Stephen Gildea, Christoph Groth, Jhon Alejandro Marin Rodriguez, Adrià
García-Alzórriz, Romain Porte, Jakub Ružička, skyper, James Turton, Alois
Schlögl, Judit Foglszinger, Aaron Boxer, Kevin Wu, Anthony Perkins, Felix
Delattre, Ken Ibbotson, Andrei Rozanski, Nis Martensen , qinxialei, Laurin
Hagemann, Jai Flack, Johann Elsass, Fred Le Meur, Vivek K J, Thiago da Silva
Gracini, Jobin J, Selvamani Kannan, Calum McConnell, Dhyey Patel, Ed Neville,
Leonidas S. Barbosa, Lucca Braga Godoy Mendonça, Chris Keller, Guinness,
Sergio de Almeida Cipriano Junior, Sahil Dhiman, Michel Le Bihan, Fabio
Fantoni, Mark Pearson, Matija Nalis, David Bannon, Federico Grau, Lisa Julia
Nebel, Patrick Jaap, Francisco Emanoel Ferreira, Peymaneh Nejad, Daniel Milde,
Stefan Kropp, Frédéric Pierret, Vipul Kumar, Jarrah Gosbell, John Zaitseff,
Badreddin Aboubakr, Sam Reed, Scupake, Clay Stan, Klaumi Klingsporn, Vincent
Smeets, Emerson dos Santos Queiroz, Alexander Sulfrian, bill-auger, Marcelo
Henrique Cerri, Dan Streetman, Hu Feng, Andrea Righi, Matthias Klein, Eric
Brown, Mayco Souza Berghetti, Robbi Nespu, Simon Tatham, and Brian Potkin.
</p>

<p><b>New Debian Maintainers</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD
<p>
Please welcome: Ricardo Ribalda Delgado, Pierre Gruet, Henry-Nicolas Tourneur,
Aloïs Micard, Jérôme Lebleu, Nis Martensen, Stephan Lachnit, Felix Salfelder,
Aleksey Kravchenko, Étienne Mollier, Timo Röhling, Fabio Augusto De Muzio
Tobich, Arun Kumar Pariyar, Francis Murtagh, William Desportes, Robin
Gustafsson, Nicholas Guriev, Xiang Gao, Maarten L. Hekkelman, qinxialei,
Boian Bonev, Filip Hroch, and Antonio Valentino.
</p>

<p><b>New Debian Developers</b></p>
<p>
Please welcome: Benda XU, Joseph Nahmias, Marcos Fouces, Hayashi Kentaro,
James Valleroy, Helge Deller, Nicholas D Steeves, Nilesh Patra, David Suárez
Rodríguez, and Pierre Gruet.
</p>
<p><b>Contributors</b></p>
## Visit the link below and pull the information manually.

<p>
954 people and 9 teams are currently listed on the
<a href="https://contributors.debian.org/">Debian Contributors</a> page for 2021.
</p>

<p><b>Statistics</b></p>
##Pull this information from sources.d.o
##https://sources.debian.org/stats/sid/
##https://sources.debian.org/stats/buster/
<p><b><em>buster</em></b></p>
<ul style="list-style-type:none">
<li>Source files: 12,323,884</li>
<li>Source packages: 28,925</li>
<li>Disk usage: 264,071,084 kB</li>
<li>Ctags: 9,487,034</li>
<li>Source lines of code: 1,077,110,982</li>
</ul>

<p><b><em>sid</em></b></p>
<ul style="list-style-type:none">
<li>Source files: 16,868,320</li>
<li>Source packages: 33,215</li>
<li>Disk usage: 364,735,804 kB</li>
<li>Ctags: 3,343,666</li>
<li>Source lines of code: 1,510,195,519</li>
</ul>

<p><b>Popular packages</b></p>
##Taken via UDD query from popcon
<ul>
<li><a href="https://packages.debian.org/buster/tar">tar</a>: 99,100 daily users</li>
<li><a href="https://packages.debian.org/buster/popularity-contest">popularity-contest</a>: 96,010 daily users</li>
<li><a href="https://packages.debian.org/buster/debianutils">debianutils</a>: 112,659 installations</li>
<li><a href="https://packages.debian.org/buster/gparted">gparted</a>: 14,909 installations</li>
<li><a href="https://packages.debian.org/buster/grub2-common">grub2-common</a> 54,040 recent upgrades</li>
</ul>

<p><b>New and noteworthy packages</b></p>

<p>
Here is a small sample of the many packages <a href="https://packages.debian.org/unstable/main/newpkg">
added to the unstable Debian archive</a> in the past few weeks:</p>

<ul>
<li><a href="https://packages.debian.org/unstable/main/dnsperf">dnsperf -DNS Performance Testing Tool</a></li>
<li><a href="https://packages.debian.org/unstable/main/ptpython">ptpython -Alternative Python Prompt with auto-completion</a></li>
<li><a href="https://packages.debian.org/unstable/main/science-datamanagement">science-datamanagement -Debian Science Data Management packages</a></li>
<li><a href="https://packages.debian.org/unstable/main/dnscap">dnscap -Network capture utility designed specifically for DNS traffic</a></li>
<li><a href="https://packages.debian.org/unstable/main/logiops">logiops -Configuration utility for Logitech mice and keyboards</a></li>
<li><a href="https://packages.debian.org/unstable/main/node-cron-validator">node-cron-validator -- cron-validator is a utility that allows one to validate cron expression</a></li>
</ul>

<toc-add-entry name="discuss">Discussions</toc-add-entry>

<p>Debian Developer Stephan Lachnit asked: <a href="https://lists.debian.org/debian-devel/2021/02/msg00282.html"><i>Is it 
possible to contribute to Debian without using one's real name out of privacy concerns</i></a>?</p>


<p>William Torrez Corea asked: <a href="https://lists.debian.org/debian-user/2021/01/msg00321.html"><i>How to 
upgrade the O/S to Debian buster</i></a>? The thread discussion regards 
reinstalling vs. upgrading vs. rebuilding, and a synopsis of Sid/Stable/Testing. 
</p>


<p>Jerry Mellon asked: <a  href="https://lists.debian.org/debian-user/2021/01/msg00584.html"><i>How to add 
a hard drive to an existing system</i></a>? Easy read which shows some pitfalls
and workarounds to this very common task.
</p>

<p>Dan Hitt asked:  <a href="https://lists.debian.org/debian-user/2021/01/msg00746.html"><i>How to install 
Debian 10 without CD or USB access but with ethernet and Hard Disk
usage</i></a>? This discussion covers a very fast bios, pxeboot options, grub
menuentry entries, netboot, and a hd-media kernel solution.</p>

<p>John Berden asked: <a href="https://lists.debian.org/debian-user/2021/02/msg00164.html"><i>How to fix
an incorrect password in Debian 10.8 after installation</i></a>?. This thread
touches on grub editing at startup, grub persistence, a miniature emacs and grub
editor syntax lesson, and canonical shell behaviour.</p>

<p><b>Tips and Tricks</b></p>

<ul>
<li>Craig Small, explains the <a href="https://dropbear.xyz/2021/01/18/percent-cpu-for-processes/">CPU/pcpu 
field of the ps program</a>, most of us run it as ps aux. Here is a great
explanation of how the calculations are collated and presented.</li>

<li>Vincent Fourmond shares all of his <a href="https://vince-debian.blogspot.com/2021/03/all-tips-and-tricks-about-qsoas.html">tips and tricks about QSoas</a>.</li>

<li>Bastian Venthur details <a href="https://venthur.de/2021-02-10-installing-debian-on-a-thinkpad-t14s.html">Installing Debian on a Thinkpad T14s</a>.</li>
</ul>

<p><b>Once upon a time in Debian:</b></p>

## Items pulled from the Timeline https://timeline.debian.net
## Jump to any random year/ same month/ same week.
## Provide link and link description.
## This may work better with a script at some point, but for now let us see
## what the ease of work is.

## Format - YYYY-MM-DD text

<ul>
<li>2010-03-13 to 2010-03-19 <a href="https://wiki.debian.org/DebianThailand/MiniDebCamp2010">Thailand MiniDebCamp 2010</a></li>

<li>2014-03-15 to 2014-03-16 <a href="https://wiki.debian.org/DebianWomen/Projects/MiniDebconf-Women/2014/">Women Mini-DebConf, Barcelona, Spain</a></li>

<li>2000-03-16 <a href="https://www.debian.org/vote/2000/vote_0007">Wichert Akkerman re-elected as DPL</a></li>

<li>1997-03-17 <a href="https://www.debian.org/News/1997/19970317">Ian Murdock elected as chairman of Board of Directors</a></li>

<li>2005-03-17 <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=300000">Debian Bug #300000 reported by Florian Zumbiehl</a></li>

<li>2008-03-17 <a href="https://www.debian.org/devel/debian-installer/News/2008/20080317">First beta of lenny's debian-installer released</a></li>
</ul>

<toc-add-entry name="continuedpn">Want to continue reading DPN?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Subscribe or Unsubscribe</a> from the Debian News mailing list</p>

#use wml::debian::projectnews::footer editor="The Publicity Team with contributions from Jean-Pierre Giraud, Justin B Rye, Thiago Pezzo, Paulo Santana, Donald Norwood"
# No need for the word 'and' for the last contributor, it is added at build time.
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
