# Status: published
# $Id$
# $Rev$
<define-tag pagetitle>Debian 10 Long Term Support reaching end-of-life</define-tag>
<define-tag release_date>2024-06-15</define-tag>
#use wml::debian::news

##
## Translators: 
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file 
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>
The Debian Long Term Support (LTS) Team hereby announces that Debian 10
<q>buster</q> support will reach its end-of-life on June 30, 2024, nearly five
years after its initial release on July 6th, 2019.
</p>

<p>
Starting in July, Debian will not provide further security updates for
Debian 10. A subset of <q>buster</q> packages will be supported by external
parties. Detailed information can be found at
<a href="https://wiki.debian.org/LTS/Extended"> Extended LTS</a>.
</p>

<p>
The Debian LTS Team will prepare afterwards the transition to Debian 11
<q>bullseye</q>, the current oldstable release. Thanks to the combined effort
of different teams including the Security Team, the Release Team, and the LTS
Team, the Debian 11 life cycle will also encompass five years. To make the
life cycle of Debian releases easier to follow, the related Debian teams
have agreed on the following schedule: three years of regular support plus
two years of Long Term Support. The LTS Team will take over support from
the Security and the Release Teams on August 14, 2024, three years after
the initial release on August 14, 2021. The final point update release for
<q>bullseye</q> will be published soon after the final Debian 11 Security
Advisory (DSA) will be issued.
</p>

<p>
Debian 11 will receive Long Term Support until August 31, 2026. The supported
architectures remain amd64, i386, arm64 and armhf.
</p>

<p>
For further information about using <q>bullseye</q> LTS and upgrading from
<q>buster</q> LTS, please refer to
<a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.
</p>

<p>
Debian and its LTS Team would like to thank all contributing users, developers,
sponsors and other Debian teams who are making it possible to extend the life
of previous stable releases.
</p>

<p>
If you rely on Debian LTS, please consider
<a href="https://wiki.debian.org/LTS/Development">joining the team</a>,
providing patches, testing or
<a href="https://wiki.debian.org/LTS/Funding">funding the efforts</a>.
</p>


<h2>About Debian</h2>

<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>

<h2>Subscribe / Unsubscribe</h2>
<p><a href="https://lists.debian.org/debian-announce/">Subscribe or Unsubscribe</a> from the Debian Announcements mailing list</p>
