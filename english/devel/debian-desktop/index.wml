#use wml::debian::template title="Debian on the Desktop" MAINPAGE="true"
#use wml::debian::recent_list

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#philosophy">Our Philosophy</a></li>
<li><a href="#help">How you can help</a></li>
<li><a href="#join">Join us</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian Desktop is a group of volunteers who want to create the best possible operating system for home and corporate workstation use. Our motto: <q>Software that just works.</q> Our goal: bringing Debian, GNU, and Linux to the mainstream world.</p>
</aside>

<h2><a id="philosophy">Our Philosophy</a></h2>

<p>
We recognize that many
<a href="https://wiki.debian.org/DesktopEnvironment">Desktop
Environments</a> exist and will support the use of them - this includes
making sure they work well on Debian. Our goal is to make the graphical
interfaces easy to use for beginners while allowing advanced users and
experts to tweak things if they like.
</p>

<p>
We will try to ensure that software is configured for the most common
desktop use. For example, the regular user account created during the
installation should have permission to play audio and video, print,
and manage the system through sudo. Also, we would like to keep
<a href="https://wiki.debian.org/debconf">debconf</a> (the Debian
configuration management system) questions to the absolute minimum.
There is no need to present difficult technical details during the
installation. Instead, we will try to ensure that debconf questions
make sense to the users. A novice may not even understand what these
questions are about. An expert, however, is perfectly happy to configure
the desktop environment after the installation is complete.
</p>

<h2><a id="help">How you can help</a></h2>

<p>
We're looking for motivated people who make things happen. You don't have
to be a Debian Developer (DD) to submit patches or make packages. The
core Debian Desktop team will ensure that your work is integrated. So,
here are some things you can do to help:
</p>

<ul>
  <li>Test the default desktop environment (or any of the other desktops) for the upcoming release. Get one of the <a href="$(DEVEL)/debian-installer/">testing images</a> and send feedback to the <a href="https://lists.debian.org/debian-desktop/">debian-desktop mailing list</a>.</li>
  <li>Join the <a href="https://wiki.debian.org/DebianInstaller/Team">Debian Installer Team</a> and help to improve the <a href="$(DEVEL)/debian-installer/">debian-installer</a> – the GTK+ frontend needs you.</li>
  <li>You can help the <a href="https://wiki.debian.org/Teams/DebianGnome">Debian GNOME Team</a>, the <a href="https://qt-kde-team.pages.debian.net/"> Debian Qt/KDE Maintainers and Debian KDE Extras Team</a>, or the <a href="https://salsa.debian.org/xfce-team/">Debian Xfce Group</a> with packaging, bug fixing, documentation, tests and more.</li>
  <li>Help to improve <a href="https://packages.debian.org/debconf">debconf</a> by lowering the priority of questions or removing unnecessary ones from packages. Make the necessary debconf questions easier to understand.</li>
  <li>Got some designer skills? Then why not work on the <a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian Desktop Artwork</a>.</li>
</ul>

<h2><a id="join">Join us</a></h2>

<aside class="light">
  <span class="fa fa-users fa-4x"></span>
</aside>

<ul>
  <li><strong>Wiki:</strong> Visit our <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a> Wiki (some articles may be outdated).</li>
  <li><strong>Mailing List:</strong> Discuss with us on the <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a> mailing list.</li>
  <li><strong>IRC Channel:</strong> Chat with us on IRC. Join the #debian-desktop channel on <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org)</li>
</ul>

