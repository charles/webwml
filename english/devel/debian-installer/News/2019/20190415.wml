<define-tag pagetitle>Debian Installer Buster RC 1 release</define-tag>
<define-tag release_date>2019-04-15</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the first release candidate of the installer for Debian 10
<q>Buster</q>.
</p>


<h2>Important change in this release</h2>

<p>This release includes Secure Boot support on amd64, which should
  work out of the box on most Secure Boot enabled machines. This means
  there should be no need to disable Secure Boot support in the
  firmware configuration anymore.</p>


<h2>Improvements in this release</h2>

<ul>
  <li>apt-setup:
    <ul>
      <li>Adjust handling of media sets, improving support for Debian Edu.</li>
    </ul>
  </li>
  <li>brltty:
    <ul>
      <li>Add support for Hedo MobiLine.</li>
    </ul>
  </li>
  <li>cdebconf:
    <ul>
      <li>Disable paging in text frontend for now, espeakup does not
        pronounce the keys to change pages (<a href="https://bugs.debian.org/690343">#690343</a>) and thus users
        are confounded and think the first page are the only choices.</li>
    </ul>
  </li>
  <li>cryptsetup:
    <ul>
      <li>The on-disk LUKS format version now defaults to LUKS2
        (<a href="https://bugs.debian.org/919725">#919725</a>).</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Switch to using buster artwork for the splash screen.</li>
      <li>Bump Linux kernel ABI from 4.19.0-1 to 4.19.0-4.</li>
      <li>Improve build reproducibility (<a href="https://bugs.debian.org/900918">#900918</a>, <a href="https://bugs.debian.org/920631">#920631</a>, <a href="https://bugs.debian.org/920676">#920676</a>).</li>
      <li>gen-tarball: stop using the deprecated GZIP variable.</li>
      <li>gen-tarball: use pigz automatically if it is available.</li>
      <li>Add dark theme submenu and shortcut: 'd' (<a href="https://bugs.debian.org/820911">#820911</a>).</li>
      <li>Fix loading background image on netboot grub.</li>
    </ul>
  </li>
  <li>debian-installer-utils:
    <ul>
      <li>Stop propagating BOOTIF to the installed system (<a href="https://bugs.debian.org/921444">#921444</a>).</li>
    </ul>
  </li>
  <li>espeakup:
    <ul>
      <li>Fix writing down sound card ID when the installation finishes.</li>
    </ul>
  </li>
  <li>finish-install:
    <ul>
      <li>Add support for multiple consoles.</li>
    </ul>
  </li>
  <li>grub-installer:
    <ul>
      <li>On Linux, mount/unmount /run to work around timeouts during
        LVM initialization (<a href="https://bugs.debian.org/918590">#918590</a>).</li>
    </ul>
  </li>
  <li>grub2:
    <ul>
      <li>Fix setup on Secure Boot systems where cryptodisk is in use
        (<a href="https://bugs.debian.org/917117">#917117</a>).</li>
      <li>Make grub-efi-amd64-signed recommend shim-signed (<a href="https://bugs.debian.org/919067">#919067</a>).</li>
      <li>Add help and ls modules to signed UEFI images (<a href="https://bugs.debian.org/919955">#919955</a>).</li>
      <li>grub-install: Check for arm-efi as a default target (<a href="https://bugs.debian.org/922104">#922104</a>).</li>
      <li>util: Detect more I/O errors, like insufficient space
        (<a href="https://bugs.debian.org/922741">#922741</a>).</li>
      <li>Use libefi* libraries to minimise writes to EFI variable storage
        (<a href="https://bugs.debian.org/891434">#891434</a>), instead of depending on efibootmgr. (The latter is
        expected to come back in Recommends in a later release, since
        that's a useful debugging tool.)</li>
    </ul>
  </li>
  <li>hw-detect:
    <ul>
      <li>Use 'modprobe -qn' to query available modules (<a href="https://bugs.debian.org/870448">#870448</a>).</li>
    </ul>
  </li>
  <li>libxinerama:
    <ul>
      <li>Fix udeb dependencies (<a href="https://bugs.debian.org/921715">#921715</a>).</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>certs: Replace test signing certificate with production signing certificate.</li>
      <li>certs: include both root CA and direct signing certificate (<a href="https://bugs.debian.org/924545">#924545</a>).</li>
    </ul>
  </li>
  <li>live-installer:
    <ul>
      <li>Add calamares-settings-debian as package to be removed at the
        end of the installation.</li>
    </ul>
  </li>
  <li>nano:
    <ul>
      <li>Add patch to avoid showing <tt>^S</tt> and <tt>^Q</tt>
        options in nano-tiny, as using those can cause problems on
        some d-i environments, like installs over serial console
        (<a href="https://bugs.debian.org/915017">#915017</a>).</li>
    </ul>
  </li>
  <li>network-console:
    <ul>
      <li>Drop “beep” support for arm*/ixp4xx and arm*/iop32x; it was
        used to notify users their devices was ready to be installed,
        but support for those platforms was removed from the Linux
        kernel and therefore d-i (<a href="https://bugs.debian.org/921951">#921951</a>).</li>
    </ul>
  </li>
  <li>partman-base:
    <ul>
      <li>Make sure UUIDs are available in /dev/disk/by-uuid/ so that
        the bootloader can be configured to use them (<a href="https://bugs.debian.org/852323">#852323</a>).</li>
    </ul>
  </li>
  <li>partman-lvm:
    <ul>
      <li>Really fix invalid characters in volume group names
        (<a href="https://bugs.debian.org/911036">#911036</a>), the the previous attempt was not entirely
        functional (<a href="https://bugs.debian.org/922230">#922230</a>, <a href="https://bugs.debian.org/922100">#922100</a>).</li>
    </ul>
  </li>
  <li>partman-partitioning:
    <ul>
      <li>Use GPT by default on armhf/efi, MSDOS otherwise.</li>
    </ul>
  </li>
  <li>rootskel:
    <ul>
      <li>Make theme=dark install and enable Compiz in MATE.</li>
      <li>Make theme=dark make grub use a dark theme.</li>
      <li>Run d-i on multiple consoles independently where configured.</li>
      <li>Add error-handling to steal-ctty.</li>
      <li>Add early logging so we can later see what consoles are detected
        and chosen.</li>
    </ul>
  </li>
  <li>rootskel-gtk:
    <ul>
      <li>Integrate Buster theme: futureprototype.</li>
    </ul>
  </li>
  <li>systemd:
    <ul>
      <li>udev: Run programs in the specified order (<a href="https://bugs.debian.org/925190">#925190</a>).</li>
      <li>udev: Prevent mass killings when not running under systemd
        (<a href="https://bugs.debian.org/918764">#918764</a>).</li>
    </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>base-installer:
    <ul>
      <li>Make armhf/efi work like armhf/generic for picking
        kernels.</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>armel: Delete unused package list for ixp4xx (<a href="https://bugs.debian.org/921951">#921951</a>).</li>
      <li>armhf: Add images for Novena.</li>
      <li>armhf: Add images for BananaPiM2Berry.</li>
    </ul>
  </li>
  <li>flash-kernel:
    <ul>
      <li>Add machine db entry for Rock64 (<a href="https://bugs.debian.org/906696">#906696</a>).</li>
      <li>Add machine db entry for Banana Pi M2 Berry (<a href="https://bugs.debian.org/923483">#923483</a>).</li>
      <li>Add machine db entry for Pine A64 LTS board (<a href="https://bugs.debian.org/923655">#923655</a>).</li>
      <li>Add machine db entry for Olimex A64 Teres-I (<a href="https://bugs.debian.org/926071">#926071</a>).</li>
      <li>Add machine db entries for Raspberry Pi 1, Pi Zero and Pi 3
        Compute Module (<a href="https://bugs.debian.org/921518">#921518</a>).</li>
    </ul>
  </li>
  <li>libdebian-installer:
    <ul>
      <li>Make all arm32 machines with EFI show up as subarch efi.</li>
    </ul>
  </li>
  <li>linux:
    <ul>
      <li>[armel] add spi-orion to mtd.udeb to be able to access spi
        flash on e.g. qnap ts-21x (<a href="https://bugs.debian.org/920607">#920607</a>).</li>
      <li>[armel] udeb: Add mmc-core-modules.</li>
      <li>[arm64,armhf] udeb: Add mmc-core-modules to Provides of
        kernel-image.</li>
      <li>[armhf,riscv64,s390x] udeb: Add cdrom-core-modules.</li>
      <li>[arm64,armhf,ia64,riscv64,sparc64] udeb: Add
        usb-serial-modules (<a href="https://bugs.debian.org/903824">#903824</a>).</li>
      <li>[arm64] udeb: Use generic ata-modules.</li>
      <li>[arm64] udeb: Remove redundant lines from nic-modules.</li>
      <li>udeb: Add virtio-gpu into d-i to get graphical output in VM
        instances.</li>
      <li>udeb: Add scsi-nic-modules containing Chelsio and Qlogic
        iSCSI/FC drivers.</li>
      <li>udeb: Add fb-modules and include drm and drm_kms_helper on
        most architectures.</li>
      <li>udeb: Move basic PV modules from {hyperv,virtio}-modules to
        kernel-image.</li>
      <li>udeb: Move drivers from {hyperv,virtio}-modules to
        {fb,input,nic,scsi}-modules.</li>
      <li>udeb: Make nic-wireless-modules depend on mmc-core-modules,
        not mmc-modules; move crc7 to crc-modules to avoid
        duplication.</li>
      <li>udeb: Make serial_cs optional in serial-modules.</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>76 languages are supported in this release.</li>
  <li>Full translation for 38 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
