#use wml::debian::template title="Getting Debian"
#include "$(ENGLISHDIR)/releases/images.data"

<p>This page has options for installing Debian Stable.


<ul>
<li> <a href="../CD/http-ftp/#mirrors">Download mirrors</a> of installation images
<li> <a href="../releases/stable/installmanual">Installation Manual</a> with detailed installation instructions
<li> <a href="../releases/stable/releasenotes">Release notes</a>
<li> <a href="../devel/debian-installer/">ISO images for Debian testing</a>
<li> <a href="../CD/verify">Verifying authenticity of Debian images</a>
</ul>
</p>


<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Download an installation image</a></h2>
    <ul>
      <li>A <a href="netinst"><strong>small installation image</strong></a>:
	    can be downloaded quickly and should be recorded onto a removable
	    disk. To use this, you will need a machine with an Internet
	    connection.
	<ul class="quicklist downlist">
	  <li><a title="Download installer for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bit
	      PC netinst iso</a></li>
	  <li><a title="Download installer for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bit
	      PC netinst iso</a></li>
	  <li><a title="Download CD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bit PC netinst torrents</a></li>
	  <li><a title="Download CD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-cd/">32-bit PC netinst torrents</a></li>
	</ul>
      </li>
      <li>A larger <a href="../CD/"><strong>complete installation
	image</strong></a>: contains more packages, making it easier to install
	machines without an Internet connection.
	<ul class="quicklist downlist">
	  <li><a title="Download installer for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/iso-dvd/debian-<current-tiny-cd-release-filename/>-amd64-DVD-1.iso">64-bit
	      PC DVD-1 iso</a></li>
	  <li><a title="Download installer for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/iso-dvd/debian-<current-tiny-cd-release-filename/>-i386-DVD-1.iso">32-bit
	      PC DVD-1 iso</a></li>
	  <li><a title="Download DVD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-bit PC torrents (DVD)</a></li>
	  <li><a title="Download DVD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-dvd/">32-bit PC torrents (DVD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Try Debian live before installing</a></h2>
    <p>
      You can try Debian by booting a live system from a CD, DVD or USB key
      without installing any files to the computer. You can also
      run the included <a href="https://calamares.io">Calamares Installer</a>. Only available for 64-bit PC.
      Read more <a href="../CD/live#choose_live">information about this method</a>.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Download Gnome live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-gnome.iso">Live Gnome</a></li>
      <li><a title="Download Xfce live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-xfce.iso">Live Xfce</a></li>
      <li><a title="Download KDE live ISO for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/iso-hybrid/debian-live-<current-cd-release/>-amd64-kde.iso">Live KDE</a></li>
      <li><a title="Download other live ISO for 64-bit Intel and AMD PC"
            href="<live-images-url/>/amd64/iso-hybrid/">Other live ISO</a></li>
      <li><a title="Download live torrents for 64-bit Intel and AMD PC"
          href="<live-images-url/>/amd64/bt-hybrid/">live torrents</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Buy CDs, DVDs or USB sticks from a
      vendor of Debian installation media</a></h2>

   <p>
      Many of the vendors sell the distribution for less than US$5 plus
      shipping (check their web page to see if they ship internationally).
   </p>

   <p>Here are the basic advantages of CDs:</p>

   <ul>
     <li>You can install on machines without an Internet connection.</li>
	 <li>You can install Debian without downloading
	 all packages yourself.</li>
   </ul>

   <h2><a href="pre-installed">Buy a computer with Debian
      pre-installed</a></h2>
   <p>There are a number of advantages to this:</p>
   <ul>
    <li>You don't have to install Debian.</li>
    <li>The installation is pre-configured to match the hardware.</li>
    <li>The vendor may provide technical support.</li>
   </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Use a Debian cloud image</a></h2>
    <p>An official <a href="https://cloud.debian.org/images/cloud/"><strong>cloud image</strong></a>, built by the cloud team, can be used on:</p>
    <ul>
      <li>your OpenStack provider, in qcow2 or raw formats.
      <ul class="quicklist downlist">
	   <li>64-bit AMD/Intel (<a title="OpenStack image for 64-bit AMD/Intel qcow2" href="<cloud-current-url/>-generic-amd64.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit AMD/Intel raw" href="<cloud-current-url/>-generic-amd64.raw">raw</a>)</li>
       <li>64-bit ARM (<a title="OpenStack image for 64-bit ARM qcow2" href="<cloud-current-url/>-generic-arm64.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit ARM raw" href="<cloud-current-url/>-generic-arm64.raw">raw</a>)</li>
	   <li>64-bit Little Endian PowerPC (<a title="OpenStack image for 64-bit Little Endian PowerPC qcow2" href="<cloud-current-url/>-generic-ppc64el.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit Little Endian PowerPC raw" href="<cloud-current-url/>-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>a local QEMU virtual machine, in qcow2 or raw formats.
      <ul class="quicklist downlist">
	   <li>64-bit AMD/Intel (<a title="QEMU image for 64-bit AMD/Intel qcow2" href="<cloud-current-url/>-nocloud-amd64.qcow2">qcow2</a>, <a title="QEMU image for 64-bit AMD/Intel raw" href="<cloud-current-url/>-nocloud-amd64.raw">raw</a>)</li>
       <li>64-bit ARM (<a title="QEMU image for 64-bit ARM qcow2" href="<cloud-current-url/>-nocloud-arm64.qcow2">qcow2</a>, <a title="QEMU image for 64-bit ARM raw" href="<cloud-current-url/>-nocloud-arm64.raw">raw</a>)</li>
	   <li>64-bit Little Endian PowerPC (<a title="QEMU image for 64-bit Little Endian PowerPC qcow2" href="<cloud-current-url/>-nocloud-ppc64el.qcow2">qcow2</a>, <a title="QEMU image for 64-bit Little Endian PowerPC raw" href="<cloud-current-url/>-nocloud-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, either as a machine image or via the AWS Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Amazon Machine Images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Amazon Machine Images</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	   </ul>
      </li>
      <li>Microsoft Azure, on the Azure Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 12 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-12?tab=PlansAndPrice">Debian 12 ("Bookworm")</a></li>
	    <li><a title="Debian 11 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
	   </ul>
      </li>
    </ul>
  </div>
</div>
