# Debian organizition
# Copyright (C) YEAR Free Software Foundation, Inc.
# Sangdo Jun <sebuls@gmail.com>, 2020
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2024-06-09 01:18+0900\n"
"Last-Translator: Sangdo Jun <sebuls@gmail.com>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "위임 메일"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "임명 메일"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>위임"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>위임"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>he/him"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>she/her"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>위임"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>they/them"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "현재"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "멤버"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "관리자"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "안정 릴리스 관리자"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "위저드"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "의장"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "보조"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "서기"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "대변인"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "역할"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"다음 목록에서, <q>현재</q>는 임기 중인 경우 (선거로 당선되었거나 임기가 있는 "
"역할에 임명되었을 때) 사용됩니다."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "지도부"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:104
msgid "Distribution"
msgstr "배포판"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:195
msgid "Communication and Outreach"
msgstr "커뮤니케이션 및 다양성 확장"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:198
msgid "Data Protection team"
msgstr "데이터 보호 팀"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:203
msgid "Publicity team"
msgstr "홍보 팀"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:270
msgid "Membership in other organizations"
msgstr "다른 조직 멤버쉽"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:293
msgid "Support and Infrastructure"
msgstr "지원 및 인프라"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "리더"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "기술 위원회"

#: ../../english/intro/organization.data:99
msgid "Secretary"
msgstr "사무국장"

#: ../../english/intro/organization.data:107
msgid "Development Projects"
msgstr "개발 프로젝트"

#: ../../english/intro/organization.data:108
msgid "FTP Archives"
msgstr "FTP 저장소"

#: ../../english/intro/organization.data:110
msgid "FTP Masters"
msgstr "FTP 마스터"

#: ../../english/intro/organization.data:116
msgid "FTP Assistants"
msgstr "FTP 보조"

#: ../../english/intro/organization.data:122
msgid "FTP Wizards"
msgstr "FTP 위저드"

#: ../../english/intro/organization.data:125
msgid "Backports"
msgstr "백포트"

#: ../../english/intro/organization.data:127
msgid "Backports Team"
msgstr "백포트 팀"

#: ../../english/intro/organization.data:131
msgid "Release Management"
msgstr "릴리스 관리"

#: ../../english/intro/organization.data:133
msgid "Release Team"
msgstr "릴리스 팀"

#: ../../english/intro/organization.data:143
msgid "Quality Assurance"
msgstr "품질 보증"

#: ../../english/intro/organization.data:144
msgid "Installation System Team"
msgstr "설치 시스템 팀"

#: ../../english/intro/organization.data:145
msgid "Debian Live Team"
msgstr "데비안 라이브 팀"

#: ../../english/intro/organization.data:146
msgid "CD/DVD/USB Images"
msgstr "CD/DVD/USB 이미지"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "프로덕션"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "테스팅"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr "클라우드 팀"

#: ../../english/intro/organization.data:160
msgid "Autobuilding infrastructure"
msgstr "자동 빌드 인프라"

#: ../../english/intro/organization.data:162
msgid "Wanna-build team"
msgstr "빌드 희망 팀"

#: ../../english/intro/organization.data:169
msgid "Buildd administration"
msgstr "Buildd 관리"

#: ../../english/intro/organization.data:186
msgid "Documentation"
msgstr "문서"

#: ../../english/intro/organization.data:190
msgid "Work-Needing and Prospective Packages list"
msgstr "작업 필요 및 예정 퍄키지 목록"

#: ../../english/intro/organization.data:206
msgid "Press Contact"
msgstr "언론 연락처"

#: ../../english/intro/organization.data:208
msgid "Web Pages"
msgstr "웹 페이지"

#: ../../english/intro/organization.data:216
msgid "Planet Debian"
msgstr "플래닛 데비안"

#: ../../english/intro/organization.data:221
msgid "Outreach"
msgstr "다양성 확장"

#: ../../english/intro/organization.data:226
msgid "Debian Women Project"
msgstr "데비안 여성 프로젝트"

#: ../../english/intro/organization.data:234
msgid "Community"
msgstr "커뮤니티"

#: ../../english/intro/organization.data:241
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey."
"txt\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"커뮤니티 팀의 모든 멤버에게 비밀 메시지를 보내려면, <a href=\"community-team-"
"pubkey.txt\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a> GPG 키를 사용하십시"
"오."

#: ../../english/intro/organization.data:243
msgid "Events"
msgstr "행사"

#: ../../english/intro/organization.data:250
msgid "DebConf Committee"
msgstr "DebConf 위원회"

#: ../../english/intro/organization.data:257
msgid "Partner Program"
msgstr "파트너 프로그램"

#: ../../english/intro/organization.data:261
msgid "Hardware Donations Coordination"
msgstr "하드웨어 기부 협조"

#: ../../english/intro/organization.data:276
msgid "GNOME Foundation"
msgstr "GNOME Foundation"

#: ../../english/intro/organization.data:278
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:279
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:281
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:282
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:283
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:286
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:289
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:296
msgid "Bug Tracking System"
msgstr "버그 추적 시스템"

#: ../../english/intro/organization.data:301
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "메일링 리스트 관리 및 메일링 리스트 아카이브"

#: ../../english/intro/organization.data:309
msgid "New Members Front Desk"
msgstr "새로운 멤버 프론트 데스크"

#: ../../english/intro/organization.data:318
msgid "Debian Account Managers"
msgstr "개발자 계정 관리자"

#: ../../english/intro/organization.data:324
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"모든 DAM에게 비밀 메시지를 보내려면, "
"57731224A9762EA155AB2A530CA8D15BB24D96F2 GPG 키를 사용하십시오."

#: ../../english/intro/organization.data:325
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "키링 관리자(PGP와 GPG)"

#: ../../english/intro/organization.data:329
msgid "Security Team"
msgstr "보안 팀"

#: ../../english/intro/organization.data:341
msgid "Policy"
msgstr "정책"

#: ../../english/intro/organization.data:344
msgid "System Administration"
msgstr "시스템 관리"

#: ../../english/intro/organization.data:345
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"암호 문제 또는 특정 패키지 설치가 필요할 때 등, 데비안 서버에 문제가 발생했"
"을 때 사용할 주소입니다."

#: ../../english/intro/organization.data:355
msgid ""
"If you have hardware problems with Debian machines, please see <a "
"href=\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it "
"should contain per-machine administrator information."
msgstr ""
"데비안 서버에 하드웨어 문제가 발생한 경우, <a href=\"https://db.debian.org/"
"machines.cgi\">Debian Machines</a> 페이지를 참고하십시오. 해당 페이지에 각 컴"
"퓨터마다 관리자 정보가 들어 있습니다."

#: ../../english/intro/organization.data:356
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP 개발자 디렉터리 관리자"

#: ../../english/intro/organization.data:357
msgid "Mirrors"
msgstr "미러"

#: ../../english/intro/organization.data:360
msgid "DNS Maintainer"
msgstr "DNS 관리자"

#: ../../english/intro/organization.data:361
msgid "Package Tracking System"
msgstr "패키지 추적 시스템"

#: ../../english/intro/organization.data:363
msgid "Treasurer"
msgstr "총무"

#: ../../english/intro/organization.data:368
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr "<a name=\"trademark\" href=\"m4_HOME/trademark\">상표권</a> 사용 요청"

#: ../../english/intro/organization.data:371
msgid "Salsa administrators"
msgstr "Salsa 관리자"

#~ msgid "Release Notes"
#~ msgstr "릴리스 노트"
