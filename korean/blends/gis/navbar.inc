#use wml::debian::blends::gis

{#alternate_navbar#:
   <div id="second-nav">
      <p><a href="$(HOME)/blends/gis/">데비안&nbsp;GIS&nbsp;블렌드</a></p>
    <ul>
      <li><a href="$(HOME)/blends/gis/about">블렌드에 대하여</a></li>
      <li><a href="$(HOME)/blends/gis/contact">연락하기</a></li>
      <li><a href="$(HOME)/blends/gis/get/">블렌드 구하기</a>
         <ul>
         <li><a href="$(HOME)/blends/gis/get/metapackages">메타패키지 사용하기</a></li>
	 </ul>
      </li>
      <li><a href="$(HOME)/blends/gis/deriv">파생</a></li>
      <li><a href="https://wiki.debian.org/DebianGis">개발</a>
        <ul>
        <li><a href="<gis-policy-html/>">GIS 팀 정책</a></li>
        </ul>
      </li>
    </ul>
   </div>
:#alternate_navbar#}
