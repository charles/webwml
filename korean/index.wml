#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"
#use wml::debian::translation-check translation="ccdb794b3ff8618ce3bde5f028d73ec0859cc827"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div id="splash">
  <h1>데비안</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>커뮤니티</h1>
      <h2>데비안은 사람들의 커뮤니티입니다!</h2>

#include "$(ENGLISHDIR)/index.inc"

    <div class="row">
      <div class="community column">
        <a href="intro/people" aria-hidden="true">
          <img src="Pics/users.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/people">사람들</a></h2>
        <p>우리는 누구이며, 어떤 일을 하는가</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/philosophy" aria-hidden="true">
          <img src="Pics/heartbeat.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/philosophy">철학</a></h2>
        <p>우리는 왜 데비안을 하며, 어떻게 하는가</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/" aria-hidden="true">
          <img src="Pics/user-plus.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">참여 및 기여</a></h2>
        <p>참여하는 방법!</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#community" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#community">더 많은 정보...</a></h2>
        <p>데비안 커뮤니티에 대한 추가 정보</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>운영체제</h1>
      <h2>데비안은 완전한 자유 운영체제입니다!</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
        <a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso" class="os-dl-btn"><download></a>
        <a href="distrib" class="os-other-download">기타 다운로드</a>
</div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">왜 데비안인가</a></h2>
        <p>무엇이 데비안을 특별하게 만드는가</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">사용자 지원</a></h2>
        <p>도움과 문서 구하기</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">보안 업데이트</a></h2>
        <p>데비안 보안 권고</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#software" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#software">더 많은 정보...</a></h2>
        <p>다운로드 및 소프트웨어 관련 더 많은 링크</p>
      </div>
    </div>
  </div>
</div>

<hr>

<!-- An optional row highlighting events happening now, such as releases, point releases, debconf and minidebconfs, and elections (dpl, GRs...). -->
# <!-- <div class="row">
#    <div class="column styled-href-blue column-left">
#     <div style="text-align: center">
#       <h2><a href="https://debconf22.debconf.org/">DebConf22</a> 진행중!</h2>
#       <p>데비안 컨퍼런스가 코소보 프리즈렌에서 2022년 7월 17일부터 24일까지 열립니다.</p>
#     </div>
#   </div>
# </div>-->

<!-- The next row of columns on the site. -->
<!-- The News will be selected by the press team. -->


<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>데비안 관련 뉴스와 공지사항</h2>
    </div>

    <:= get_top_news() :>

    <!-- No more News entries behind this line! -->
    <div class="project-news">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">모든 뉴스</a> &emsp;&emsp;
	<a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="데비안 뉴스" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="데비안 프로젝트 뉴스" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="데비안 보안 권고 (제목만)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="데비안 보안 권고 (요약)" href="security/dsa-long">
:#rss#}

