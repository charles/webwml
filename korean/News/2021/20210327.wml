#use wml::debian::translation-check translation="2a3fa2ac1b49f491737f3c89828986a43c00a61e" mindelta="-1"
<define-tag pagetitle>데비안 10 업데이트: 10.9 릴리스</define-tag>
<define-tag release_date>2021-03-27</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>데비안 프로젝트는 안정적인 배포판 데비안 <release> (코드명 <q><codename></q>)의 아홉 번째 업데이트를 알리게 되어 기쁩니다.
 이 포인트 릴리스는 주로 보안 문제에 대한 수정 사항과 함께 심각한 문제에 대한 몇 가지 조정 사항을 더했습니다. 
보안 권고사항은 이미 별도로 게시되었으며 사용 가능한 곳에서  참조될 수 있습니다.</p>

<p>포인트 릴리스는 데비안 <release>의 새 버전을 만드는 것이 아니며, 
포함된 일부 패키지만 업데이트 한다는 것을 주의하세요.
이전 버전의 <q><codename></q> 미디어를 버릴 필요 없습니다. 
설치 후, 최신 데비안 미러를 써서 패키지를 현재 버전으로 업그레이드 가능합니다.</p>

<p>security.debian.org의 업데이트를 자주 설치하는 사람들은 패키지를 많이 업데이트하지 않아도 되며, 
해당 업데이트는 대부분 포인트 릴리스에 들어있습니다.</p>

<p>새 설치 이미지는 정규 위치에 곧 공개될 겁니다.</p>

<p>패키지 관리 시스템이 수많은 데비안 HTTP 미러 중 하나를 가리키게 해서 
기존 설치를 이 개정판으로 업그레이드할 수 있습니다.
포괄적인 미러 서버 목록:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>기타 버그 고침</h2>

<p>이 버전의 안정 업데이트는 아래 패키지에 몇몇 중요한 수정을 더했습니다:</p>

<table border=0>
<tr><th>패키지</th>               <th>까닭</th></tr>
<correction avahi "Remove avahi-daemon-check-dns mechanism, which is no longer needed">
<correction base-files "Update /etc/debian_version for the 10.9 point release">
<correction cloud-init "Avoid logging generated passwords to world-readable log files [CVE-2021-3429]">
<correction debian-archive-keyring "Add bullseye keys; retire jessie keys">
<correction debian-installer "Use 4.19.0-16 Linux kernel ABI">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction exim4 "Fix use of concurrent TLS connections under GnuTLS; fix TLS certificate verification with CNAMEs; README.Debian: document the limitation/extent of server certificate verification in the default configuration">
<correction fetchmail "No longer report <q>System error during SSL_connect(): Success</q>; remove OpenSSL version check">
<correction fwupd "Add SBAT support">
<correction fwupd-amd64-signed "Add SBAT support">
<correction fwupd-arm64-signed "Add SBAT support">
<correction fwupd-armhf-signed "Add SBAT support">
<correction fwupd-i386-signed "Add SBAT support">
<correction fwupdate "Add SBAT support">
<correction fwupdate-amd64-signed "Add SBAT support">
<correction fwupdate-arm64-signed "Add SBAT support">
<correction fwupdate-armhf-signed "Add SBAT support">
<correction fwupdate-i386-signed "Add SBAT support">
<correction gdnsd "Fix stack overflow with overly-large IPv6 addresses [CVE-2019-13952]">
<correction groff "Rebuild against ghostscript 9.27">
<correction hwloc-contrib "Enable support for the ppc64el architecture">
<correction intel-microcode "Update various microcode">
<correction iputils "Fix ping rounding errors; fix tracepath target corruption">
<correction jquery "Fix untrusted code execution vulnerabilities [CVE-2020-11022 CVE-2020-11023]">
<correction libbsd "Fix out-of-bounds read issue [CVE-2019-20367]">
<correction libpano13 "Fix format string vulnerability">
<correction libreoffice "Do not load encodings.py from current directoy">
<correction linux "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction linux-latest "Update to -15 kernel ABI; update for -16 kernel ABI">
<correction linux-signed-amd64 "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction linux-signed-arm64 "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction linux-signed-i386 "New upstream stable release; update ABI to -16; rotate secure boot signing keys; rt: update to 4.19.173-rt72">
<correction lirc "Normalize embedded ${DEB_HOST_MULTIARCH} value in /etc/lirc/lirc_options.conf to find unmodified configuration files on all architectures; recommend gir1.2-vte-2.91 instead of non-existent gir1.2-vte">
<correction m2crypto "Fix test failure with recent OpenSSL versions">
<correction openafs "Fix outgoing connections after unix epoch time 0x60000000 (14 January 2021)">
<correction portaudio19 "Handle EPIPE from alsa_snd_pcm_poll_descriptors, fixing crash">
<correction postgresql-11 "New upstream stable release; fix information leakage in constraint-violation error messages [CVE-2021-3393]; fix CREATE INDEX CONCURRENTLY to wait for concurrent prepared transactions">
<correction privoxy "Security issues [CVE-2020-35502 CVE-2021-20209 CVE-2021-20210 CVE-2021-20211 CVE-2021-20212 CVE-2021-20213 CVE-2021-20214 CVE-2021-20215 CVE-2021-20216 CVE-2021-20217 CVE-2021-20272 CVE-2021-20273 CVE-2021-20275 CVE-2021-20276]">
<correction python3.7 "Fix CRLF injection in http.client [CVE-2020-26116]; fix buffer overflow in PyCArg_repr in _ctypes/callproc.c [CVE-2021-3177]">
<correction redis "Fix a series of integer overflow issues on 32-bit systems [CVE-2021-21309]">
<correction ruby-mechanize "Fix command injection issue [CVE-2021-21289]">
<correction systemd "core: make sure to restore the control command id, too, fixing a segfault; seccomp: allow turning off of seccomp filtering via an environment variable">
<correction uim "libuim-data: Perform symlink_to_dir conversion of /usr/share/doc/libuim-data in the resurrected package for clean upgrades from stretch">
<correction xcftools "Fix integer overflow vulnerability [CVE-2019-5086 CVE-2019-5087]">
<correction xterm "Correct upper-limit for selection buffer, accounting for combining characters [CVE-2021-27135]">
</table>


<h2>보안 업데이트</h2>


<p>이 개정판은 아래의 보안 업데이트를 안정 릴리스에 추가합니다.
보안팀은 각 업데이트에 대해서 이미 권고사항을 공개했습니다:</p>

<table border=0>
<tr><th>권고 ID</th>  <th>패키지</th></tr>
<dsa 2021 4826 nodejs>
<dsa 2021 4844 dnsmasq>
<dsa 2021 4845 openldap>
<dsa 2021 4846 chromium>
<dsa 2021 4847 connman>
<dsa 2021 4849 firejail>
<dsa 2021 4850 libzstd>
<dsa 2021 4851 subversion>
<dsa 2021 4853 spip>
<dsa 2021 4854 webkit2gtk>
<dsa 2021 4855 openssl>
<dsa 2021 4856 php7.3>
<dsa 2021 4857 bind9>
<dsa 2021 4858 chromium>
<dsa 2021 4859 libzstd>
<dsa 2021 4860 openldap>
<dsa 2021 4861 screen>
<dsa 2021 4862 firefox-esr>
<dsa 2021 4863 nodejs>
<dsa 2021 4864 python-aiohttp>
<dsa 2021 4865 docker.io>
<dsa 2021 4867 grub-efi-amd64-signed>
<dsa 2021 4867 grub-efi-arm64-signed>
<dsa 2021 4867 grub-efi-ia32-signed>
<dsa 2021 4867 grub2>
<dsa 2021 4868 flatpak>
<dsa 2021 4869 tiff>
<dsa 2021 4870 pygments>
<dsa 2021 4871 tor>
<dsa 2021 4872 shibboleth-sp>
</table>



<h2>데비안 설치관리자</h2>
<p>설치관리자는 포인트 릴리스에서 안정 릴리스에서 병합된 수정을 포함하도록 업데이트되었습니다.</p>

<h2>URL</h2>

<p>이 개정판에서 바뀐 패키지 전체 목록:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>현재 안정 배포판:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>안정 배포판 정보(release notes, 정오표 등.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>보안 알림 및 정보:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>데비안은</h2>

<p>데비안 프로젝트는 완전한 자유 운영체제인 데비안을 제작하기 위해 자신의 시간과 노력을 자원하는 자유 소프트웨어 개발자 모임입니다.</p>

<h2>연락 정보</h2>

<p>더 많은 정보를 위해 <a href="$(HOME)/">https://www.debian.org/</a>에 있는 데비안 웹 페이지를 방문하거나, 
&lt;press@debian.org&gt;으로 이메일을 보내세요.
또는 &lt;debian-release@lists.debian.org&gt;로 보내서 안정 릴리스 팀으로 연락하세요.
</p>


