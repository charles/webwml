#use wml::debian::translation-check translation="21882a152ab072f844cf526dc172ff70559e05e7" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag pagetitle>데비안 10 업데이트: 10.1 릴리스</define-tag>
<define-tag release_date>2019-09-07</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>데비안 프로젝트는 안정 배포 데비안 <release> (코드이름 <q><codename></q>)의 첫 업데이트를 알리게 되어 기쁩니다.
이 포인트 릴리스는 주로 보안 이슈에 대한 수정 및 심각한 문제 조정을 추가합니다.
보안 권고는 이미 따로 발표되었고 가능한 곳에서 참조됩니다.
</p>

<p>포인트 릴리스는 데비안 <release>의 새 버전을 구성하지 않으며 포함된 패키지 일부만 업데이트합니다.
옛 <q><codename></q> 미디어를 던져 버릴 필요 없습니다.
설치 후, 패키지를 최신 데비안 미러를 써서 현재 버전으로 업그레이드 할 수 있습니다.
</p>

<p>security.debian.org에서 업데이트를 자주 설치하는 사람들은 많은 패키지를 업데이트하지 않아도 되며, 
그러한 업데이트는 대부분 포인트 릴리스에 포함되어 있습니다.
</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>여러가지 버그 수정</h2>

<p>이 안정 업데이트는 몇 중요한 수정을 아래 패키지에 추가합니다:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction acme-tiny "Handle upcoming ACME protocol change">
<correction android-sdk-meta "New upstream release; fix regex for adding Debian version to binary packages">
<correction apt-setup "Fix preseeding of Secure Apt for local repositories via apt-setup/localX/">
<correction asterisk "Fix buffer overflow in res_pjsip_messaging [AST-2019-002 / CVE-2019-12827]; fix remote Crash Vulnerability in chan_sip [AST-2019-003 / CVE-2019-13161]">
<correction babeltrace "Bump ctf symbols depends to post merge version">
<correction backup-manager "Fix purging of remote archives via FTP or SSH">
<correction base-files "Update for the point release">
<correction basez "Properly decode base64url encoded strings">
<correction bro "Security fixes [CVE-2018-16807 CVE-2018-17019]">
<correction bzip2 "Fix regression uncompressing some files">
<correction cacti "Fix some issues upgrading from the version in stretch">
<correction calamares-settings-debian "Fix permissions for initramfs image when full-disk encryption is enabled [CVE-2019-13179]">
<correction ceph "Rebuild against new libbabeltrace">
<correction clamav "Prevent extraction of non-recursive zip bombs; new upstream stable release with security fixes - add scan time limit to mitigate against zip-bombs [CVE-2019-12625]; fix out-of-bounds write within the NSIS bzip2 library [CVE-2019-12900]">
<correction cloudkitty "Fix build failures with updated SQLAlchemy">
<correction console-setup "Fix internationalization issues when switching locales with Perl &gt;= 5.28">
<correction cryptsetup "Fix support for LUKS2 headers without any bound keyslot; fix mapped segments overflow on 32-bit architectures">
<correction cups "Fix multiple security/disclosure issues - SNMP buffer overflows [CVE-2019-8696 CVE-2019-8675], IPP buffer overflow, Denial of Service and memory disclosure issues in the scheduler">
<correction dbconfig-common "Fix issue caused by change in bash POSIX behaviour">
<correction debian-edu-config "Use PXE option <q>ipappend 2</q> for LTSP client boot; fix sudo-ldap configuration; fix loss of dynamically allocated v4 IP address; several fixes and improvements to debian-edu-config.fetch-ldap-cert">
<correction debian-edu-doc "Update Debian Edu Buster and ITIL manuals and translations">
<correction dehydrated "Fix fetching of account information; follow-up fixes for account ID handling and APIv1 compatibility">
<correction devscripts "debchange: target buster-backports with --bpo option">
<correction dma "Do not limit TLS connections to using TLS 1.0">
<correction dpdk "New upstream stable release">
<correction dput-ng "Add buster-backports and stretch-backports-sloppy codenames">
<correction e2fsprogs "Fix e4defrag crashes on 32-bit architectures">
<correction enigmail "New upstream release; security fixes [CVE-2019-12269]">
<correction epiphany-browser "Ensure that the web extension uses the bundled copy of libdazzle">
<correction erlang-p1-pkix "Fix handling of GnuTLS certificates">
<correction facter "Fix parsing of Linux route non-key/value flags (e.g. onlink)">
<correction fdroidserver "New upstream release">
<correction fig2dev "Do not segfault on circle/half circle arrowheads with a magnification larger than 42 [CVE-2019-14275]">
<correction firmware-nonfree "atheros: Add Qualcomm Atheros QCA9377 rev 1.0 firmware version WLAN.TF.2.1-00021-QCARMSWP-1; realtek: Add Realtek RTL8822CU Bluetooth firmware; atheros: Revert change of QCA9377 rev 1.0 firmware in 20180518-1; misc-nonfree: add firmware for MediaTek MT76x0/MT76x2u wireless chips, MediaTek MT7622/MT7668 bluetooth chips, GV100 signed firmware">
<correction freeorion "Fix crash when loading or saving game data">
<correction fuse-emulator "Prefer the X11 backend over the Wayland one; show the Fuse icon on the GTK window and About dialog">
<correction fusiondirectory "Stricter checks on LDAP lookups; add missing dependency on php-xml">
<correction gcab "Fix corruption when extracting">
<correction gdb "Rebuild against new libbabeltrace">
<correction glib2.0 "Make GKeyFile settings backend create ~/.config and configuration files with restrictive permissions [CVE-2019-13012]">
<correction gnome-bluetooth "Avoid GNOME Shell crashes when gnome-shell-extension-bluetooth-quick-connect is used">
<correction gnome-control-center "Fix crash when the Details -&gt; Overview (info-overview) panel is selected; fix memory leaks in Universal Access panel; fix a regression that caused the Universal Access -&gt; Zoom mouse tracking options to have no effect; updated Icelandic and Japanese translations">
<correction gnupg2 "Backport many bug fixes and stability patches from upstream; use keys.openpgp.org as the default keyserver; only import self-signatures by default">
<correction gnuplot "Fix incomplete/unsafe initialization of ARGV array">
<correction gosa "Stricter checks on LDAP lookups">
<correction hfst "Ensure smoother upgrades from stretch">
<correction initramfs-tools "Disable resume when there are no suitable swap devices; MODULES=most: include all keyboard driver modules, cros_ec_spi and SPI drivers, extcon-usbc-cros-ec; MODULES=dep: include extcon drivers">
<correction jython "Preserve backward compatibility with Java 7">
<correction lacme "Update for removal of unauthenticated GET support from the Let's Encrypt ACMEv2 API">
<correction libblockdev "Use existing cryptsetup API for changing keyslot passphrase">
<correction libdatetime-timezone-perl "Update included data">
<correction libjavascript-beautifier-perl "Add support for <q>=&gt;</q> operator">
<correction libsdl2-image "Fix buffer overflows [CVE-2019-5058 CVE-2019-5052 CVE-2019-7635]; fix out of bounds access in PCX handling [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction libtk-img "Stop using internal copies of JPEG, Zlib and PixarLog codecs, fixing crashes">
<correction libxslt "Fix security framework bypass [CVE-2019-11068], uninitialized read of xsl:number token [CVE-2019-13117] and uninitialized read with UTF-8 grouping chars [CVE-2019-13118]">
<correction linux "New upstream stable release">
<correction linux-latest "Update for 4.19.0-6 kernel ABI">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction lttv "Rebuild against new libbabeltrace">
<correction mapproxy "Fix WMS Capabilities with Python 3.7">
<correction mariadb-10.3 "New upstream stable release; security fixes [CVE-2019-2737 CVE-2019-2739 CVE-2019-2740 CVE-2019-2758 CVE-2019-2805]; fix segfault on 'information_schema' access; rename 'mariadbcheck' to 'mariadb-check'">
<correction musescore "Disable webkit functionality">
<correction ncbi-tools6 "Repackage without non-free data/UniVec.*">
<correction ncurses "Remove <q>rep</q> from xterm-new and derived terminfo descriptions">
<correction netdata "Remove Google Analytics from generated documentation; opt out of sending anonymous statistics; remove <q>sign in</q> button">
<correction newsboat "Fix use after free issue">
<correction nextcloud-desktop "Add missing dependency on nextcloud-desktop-common to nextcloud-desktop-cmd">
<correction node-lodash "Fix prototype pollution [CVE-2019-10744]">
<correction node-mixin-deep "Fix prototype pollution issue">
<correction nss "Fix security issues [CVE-2019-11719 CVE-2019-11727 CVE-2019-11729]">
<correction nx-libs "Fix a number of memory leaks">
<correction open-infrastructure-compute-tools "Fix container start">
<correction open-vm-tools "Correctly handle OS versions of the form <q>X</q>, rather than <q>X.Y</q>">
<correction openldap "Restrict rootDN proxyauthz to its own databases [CVE-2019-13057]; enforce sasl_ssf ACL statement on every connection [CVE-2019-13565]; fix slapo-rwm to not free original filter when rewritten filter is invalid">
<correction osinfo-db "Add buster 10.0 information; fix URLs for stretch download; fix the name of the parameter used to set the fullname when generating a preseed file">
<correction osmpbf "Rebuild with protobuf 3.6.1">
<correction pam-u2f "Fix insecure debug file handling [CVE-2019-12209]; fix debug file descriptor leak [CVE-2019-12210]; fix out-of-bounds access; fix segfault following a failure to allocate a buffer">
<correction passwordsafe "Install localisation files in the correct directory">
<correction piuparts "Update configurations for the buster release; fix spurious failure to remove packages with names ending with '+'; generate separate tarball names for --merged-usr chroots">
<correction postgresql-common "Fix <q>pg_upgradecluster from postgresql-common 200, 200+deb10u1, 201, and 202 will corrupt the data_directory setting when used *twice* to upgrade a cluster (e.g. 9.6 -&gt; 10 -&gt; 11)</q>">
<correction pulseaudio "Fix mute state restoring">
<correction puppet-module-cinder "Fix attempts to write to /etc/init">
<correction python-autobahn "Fix pyqrcode build dependencies">
<correction python-django "New upstream security release [CVE-2019-12781]">
<correction raspi3-firmware "Add support for Raspberry Pi Compute Module 3 (CM3), Raspberry Pi Compute Module 3 Lite and Raspberry Pi Compute Module IO Board V3">
<correction reportbug "Update release names, following buster release; re-enable stretch-pu requests; fix crashes with package / version lookup; add missing dependency on sensible-utils">
<correction ruby-airbrussh "Don't throw exception on invalid UTF-8 SSH output">
<correction sdl-image1.2 "Fix buffer overflows [CVE-2019-5052 CVE-2019-7635], out-of-bounds access [CVE-2019-12216 CVE-2019-12217 CVE-2019-12218 CVE-2019-12219 CVE-2019-12220 CVE-2019-12221 CVE-2019-12222 CVE-2019-5051]">
<correction sendmail "sendmail-bin.postinst, initscript: Let start-stop-daemon match on pidfile and executable; sendmail-bin.prerm: Stop sendmail before removing the alternatives">
<correction slirp4netns "New upstream stable release with security fixes - check sscanf result when emulating ident [CVE-2019-9824]; fixes heap overflow in included libslirp [CVE-2019-14378]">
<correction systemd "Network: Fix failure to bring up interface with Linux kernel 5.2; ask-password: Prevent buffer overflow when reading from keyring; network: Behave more gracefully when IPv6 has been disabled">
<correction tzdata "New upstream release">
<correction unzip "Fix zip bomb issues [CVE-2019-13232]">
<correction usb.ids "Routine update of USB IDs">
<correction warzone2100 "Fix a segmentation fault when hosting a multiplayer game">
<correction webkit2gtk "New upstream stable version; stop requiring SSE2-capable CPUs">
<correction win32-loader "Rebuild against current packages, particularly debian-archive-keyring; fix build failure by enforcing a POSIX locale">
<correction xymon "Fix several (server only) security issues [CVE-2019-13273 CVE-2019-13274 CVE-2019-13451 CVE-2019-13452 CVE-2019-13455 CVE-2019-13484 CVE-2019-13485 CVE-2019-13486]">
<correction yubikey-personalization "Backport additional security precautions">
<correction z3 "Do not set the SONAME of libz3java.so to libz3.so.4">
</table>


<h2>보안 업데이트</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2019 4477 zeromq3>
<dsa 2019 4478 dosbox>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4480 redis>
<dsa 2019 4481 ruby-mini-magick>
<dsa 2019 4482 thunderbird>
<dsa 2019 4483 libreoffice>
<dsa 2019 4484 linux>
<dsa 2019 4484 linux-signed-i386>
<dsa 2019 4484 linux-signed-arm64>
<dsa 2019 4484 linux-signed-amd64>
<dsa 2019 4486 openjdk-11>
<dsa 2019 4488 exim4>
<dsa 2019 4489 patch>
<dsa 2019 4490 subversion>
<dsa 2019 4491 proftpd-dfsg>
<dsa 2019 4493 postgresql-11>
<dsa 2019 4494 kconfig>
<dsa 2019 4495 linux-signed-amd64>
<dsa 2019 4495 linux-signed-arm64>
<dsa 2019 4495 linux>
<dsa 2019 4495 linux-signed-i386>
<dsa 2019 4496 pango1.0>
<dsa 2019 4498 python-django>
<dsa 2019 4499 ghostscript>
<dsa 2019 4501 libreoffice>
<dsa 2019 4502 ffmpeg>
<dsa 2019 4503 golang-1.11>
<dsa 2019 4504 vlc>
<dsa 2019 4505 nginx>
<dsa 2019 4507 squid>
<dsa 2019 4508 h2o>
<dsa 2019 4509 apache2>
<dsa 2019 4510 dovecot>
</table>


<h2>삭제된 패키지</h2>

<p>아래 패키지는 우리 통제범위를 넘어서 삭제되었습니다.:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction pump "Unmaintained; security issues">
<correction rustc "Remove outdated rust-doc cruft">

</table>

<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URL</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>현재 안정 배포:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>보안 알림 및 정보:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>데비안은</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>연락정보</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>
