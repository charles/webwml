#use wml::debian::template title="데비안 릴리스"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7728705b5a45696500b67406cf935d0e66ad8a0b"

<p>데비안은 적어도 3개 릴리스를 관리합니다:
<q>안정(stable)</q>, <q>테스팅(testing)</q> 그리고 <q>불안정(unstable)</q>입니다.</p>

<dl>
<dt><a href="stable/">안정(stable)</a></dt>
<dd>
<p>
  <q>안정</q> 배포판은 최근 공식적으로 나온 데비안 배포판을 포함합니다.
</p>
<p>이 릴리스는 프로덕션 릴리스로, 주로 이 릴리스를 사용하기 권장합니다.
</p>
<p>
  현재 <q>안정</q> 배포판은 버전
  <:=substr '<current_initial_release>', 0, 2:>, 코드 이름은 <em><current_release_name></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "이 릴리스는 <current_release_date>에 나왔습니다."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "초기 버전 <current_initial_release>은 <current_initial_release_date>에 나왔고,
  최신 업데이트 버전 <current_release>은 <current_release_date>에 나왔습니다."
/>

</p>
</dd>

<dt><a href="testing/">테스팅(testing)</a></dt>
<dd>
<p>
  <q>테스팅(testing)</q> 배포판은 <q>안정</q>판에 아직 받아들여지지 않았지만,
  안정판에 곧 들어갈 예정으로 대기 중인 패키지도 들어있습니다. 테스팅 이용의 가장
  큰 장점은 좀 더 최근 버전의 소프트웨어를 사용한다는 점입니다.
</p>
<p>
  <a href="$(DOC)/manuals/debian-faq/">데비안 FAQ</a>를 보면 <a
  href="$(DOC)/manuals/debian-faq/ftparchives#testing">무엇이
  <q>테스팅</q>인가</a> 및 <a
  href="$(DOC)/manuals/debian-faq/ftparchives#frozen">어떻게 테스팅이
  <q>안정</q>이 되는지</a>에 대한 더 많은 정보가 있습니다.
</p>
<p>
  현재 <q>테스팅</q> 배포판은 <em><current_testing_name></em>입니다.
</p>
</dd>

<dt><a href="unstable/">불안정(unstable)</a></dt>
<dd>
<p>
  <q>불안정</q> 배포판은 데비안에서 개발이 활발히 발생하는 곳입니다.
  이 배포판은 개발자 및 모험을 즐기는 사용자가 주로 씁니다. 불안정 배포판을
  이용하는 사용자는 debian-devel-announce 메일링에 구독해 (뭔가 망가뜨릴 수도
  있는) 주요 변경 사항이 무엇인지 알림을 받기를 권장합니다.
</p>
<p>
  <q>불안정</q> 배포판은 늘 <em>sid</em>로 불립니다.
</p>
</dd>
</dl>

<h2>릴리스 생명 주기</h2>
<p>
  데비안은 정기적으로 새 안정 배포판을 발표합니다. 사용자는 각 배포판에 대해
  3년의 완전 지원과, 2년의 확장 LTS 지원을 기대할 수 있습니다.
</p>

<p>
  <a href="https://wiki.debian.org/DebianReleases">데비안 릴리스</a> 위키 페이지와
 <a href="https://wiki.debian.org/LTS">데비안 LTS</a>에 자세한 정보가 있습니다.
</p>

<h2>릴리스 색인</h2>

<ul>

  <li><a href="<current_testing_name>/">데비안 다음 배포판 코드명 <q><current_testing_name></q></a>
       &mdash; 릴리스 날짜 미정
  </li>

  <li><a href="bookworm/">데비안 12 (<q>bookworm</q>)</a>
      &mdash; <q>현재</q> 안정 배포판
  </li>

  <li><a href="bookworm/">데비안 11 (<q>bookworm</q>)</a>
      &mdash; <q>옛 안정</q> 배포판
  </li>

  <li><a href="buster/">데비안 10 (<q>buster</q>)</a>
      &mdash; <q>옛 안정</q> 배포판, <a href="https://wiki.debian.org/LTS">LTS 지원</a> 중
  </li>

  <li><a href="stretch/">데비안 9 (<q>stretch</q>)</a>
      &mdash; 아카이브된 릴리스, 서드파티 유료 <a href="https://wiki.debian.org/LTS/Extended">확장 LTS 지원</a> 중
  </li>

  <li><a href="jessie/">데비안 8 (<q>jessie</q>)</a>
      &mdash; 아카이브된 릴리스, 서드파티 유료 <a href="https://wiki.debian.org/LTS/Extended">확장 LTS 지원</a> 중
  </li>

  <li><a href="wheezy/">데비안 7 (<q>wheezy</q>)</a>
      &mdash; 종료된 안정 배포판
  </li>

  <li><a href="squeeze/">데비안 6.0 (<q>squeeze</q>)</a>
      &mdash; 종료된 안정 배포판
  </li>

  <li><a href="lenny/">데비안 GNU/리눅스 5.0 (<q>lenny</q>)</a>
      &mdash; 종료된 안정 배포판
  </li>

  <li><a href="etch/">데비안 GNU/리눅스 4.0 (<q>etch</q>)</a>
      &mdash; 종료된 안정 배포판
  </li>
  <li><a href="sarge/">데비안 GNU/리눅스 3.1 (<q>sarge</q>)</a>
      &mdash; 종료된 안정 배포판
  </li>
  <li><a href="woody/">데비안 GNU/리눅스 3.0 (<q>woody</q>)</a>
      &mdash; 종료된 안정 배포판
  </li>
  <li><a href="potato/">데비안 GNU/리눅스 2.2 (<q>potato</q>)</a>
      &mdash; 종료된 안정 배포판
  </li>
  <li><a href="slink/">데비안 GNU/리눅스 2.1 (<q>slink</q>)</a>
      &mdash; 종료된 안정 배포판
  </li>
  <li><a href="hamm/">데비안 GNU/리눅스 2.0 (<q>hamm</q>)</a>
      &mdash; 종료된 안정 배포판
  </li>
</ul>

<p>옛 안정 배포판에 대한 웹 페이지는 그대로 유지되지만, 릴리스 자체는 분리된
<a href="$(HOME)/distrib/archive">아카이브</a>에 있습니다.</p>

<p><a href="$(HOME)/doc/manuals/debian-faq/">데비안 FAQ</a>에는
<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">어디서
코드이름이 유래했는지</a> 설명이 있습니다.</p>

<h2>릴리스의 데이터 무결성</h2>

<p>데이터 무결성은 디지털 서명된 <code>Release</code> 파일을 통해 보장합니다.
무결성을 보장하기 위해 모든 <code>Packages</code> 파일의 체크섬이
<code>Release</code> 파일 안에 들어갑니다.</p>

<p><code>Release</code> 파일을 위한 디지털 서명은 <code>Release.gpg</code>
안에 들어있고, 서명에 아카이브 서명 키의 현재 버전을 사용합니다. <q>안정</q>
및 <q>옛 안정</q>의 경우, 추가로 릴리스를 위한 서명을 남깁니다. <a
href="$(HOME)/intro/organization#release-team">안정 릴리스 팀</a>의 멤버가
특별히 릴리스를 위해 생성된 오프라인 키를 사용해 서명합니다.</p>
