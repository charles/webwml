#use wml::debian::template title="Versies van het installatiesysteem van Debian"
#use wml::debian::translation-check translation="c1221b640653be886b43ce18e8e72c5516aa770f"

<h2>Cd-versies</h2>

<p>
Er zijn een aantal verschillende versies van cd-images van het installatiesysteem van Debian die voor verschillende doeleinden bedoeld zijn.
</p>
<p>
De belangrijkste versie is <a href="index">de huidige officiële release</a>, die momenteel in Debian 6.0 zit. Deze versie is statisch en onveranderlijk, en zal waarschijnlijk voor de meeste mensen werken. Hoewel het testen van dit image nog steeds nuttig is, zijn de meeste problemen hiermee bekend bij het team binnen een paar weken na de release. Zie de <a href="errata">errata-pagina</a> voor de ergste bekende problemen.
</p>
<p>
De andere meest gebruikte versie zijn de dagelijks gebouwde versies. Dit zijn nieuwere images die getest moeten worden in de hoop dat ze ooit een officiële release zullen worden. Ze zijn gewoon een link naar één van de twee types images die hieronder beschreven worden; naar welke ervan gelinkt wordt, hangt af van waar we ons in onze releasecyclus bevinden.
<a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">Installatierapporten</a> met deze images zijn erg waardevol voor ons.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/daily-builds/sid_d-i/">De images in sid_d-i zijn nieuwe cd-images die elke dag geproduceerd worden. Voor deze images wordt de versie van het installatiesysteem uit de distributie unstable gebruikt, hoewel men er nog steeds de distributie testing mee installeert. Gewoonlijk verwijzen de links naar de dagelijks gebouwde cd-images naar deze images.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/daily-builds/jessie_d-i/">De images in jessie_d-i</a> worden ook dagelijks geproduceerd. Voor deze images wordt de versie van het installatiesysteem uit de distributie testing gebruikt, waarmee de distributie testing wordt geïnstalleerd. Op het moment van de release wordt een van deze images gekozen en wordt dit de officiële release-image. Dichtbij een release zullen de links naar de dagelijks gebouwde cd-images worden omgeschakeld om naar deze images te verwijzen, zodat ze kunnen worden getest.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/weekly-builds/i386/iso-cd/">De wekelijkse volledige cd-</a>
en <a href="https://cdimage.debian.org/cdimage/weekly-builds/i386/iso-dvd/">dvd</a>-versies vergen verschillende dagen om gebouwd te worden en worden dus maar één keer per week opnieuw gegenereerd. De versie van het installatiesysteem hierop varieert, maar is over het algemeen de versie die we op dat moment willen testen.
</p>

<h2>initrd-versies</h2>

<p>
Alle andere images met het installatiesysteem van Debian, waaronder netboot, staan gezamenlijk bekend als de <q>initrd-images</q>. Ook hier worden verschillende versies gebruikt.
</p>
<p>
Net als bij de cd-images is de belangrijkste initrd-versie die van <a href="index">de huidige officiële release</a>.
</p>
<p>
De andere meest gebruikte initrd-versies zijn de dagelijks gebouwde versies. Deze images worden ongeveer één keer per dag gebouwd door sommige ontwikkelaars van het installatiesysteem van Debian, meestal op hun eigen persoonlijke machines. Ze bevatten altijd de nieuwste versie van het installatiesysteem uit de distributie unstable.
</p>
<p>
Van tijd tot tijd wordt er een officiële initrd-versie van het installatiesysteem van Debian gebouwd, als onderdeel van een release van het pakket <tt>debian-installer</tt>. Deze images worden gebouwd op het Debian autobuilder-netwerk zoals elk ander pakket, en worden geplaatst in de onderliggende map <tt>dists/unstable/main/binary-&lt;arch&gt;/current</tt>.
</p>
<p>
Wanneer het installatiesysteem van Debian wordt uitgebracht, wordt een van deze officiële versies gekopieerd naar de onderliggende map <tt>dists/testing/main/binary-&lt;arch&gt;/current</tt>.
</p>

<h2>Statuspagina gebouwde versies</h2>

<p>
De status van alle periodiek gebouwde images wordt bijgehouden en verzameld op <a href="https://d-i.debian.org/daily-images/build-logs.html">de statuspagina van gebouwde versies</a>. Deze pagina kan niet zeggen of de images werken, alleen of ze succesvol gebouwd werden.
</p>
