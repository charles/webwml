#use wml::debian::template title="Pakketten"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::toc
#use wml::debian::translation-check translation="7f237cf353af6f040ca7a1ab3fb8d9038c051063"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<toc-display/>


<toc-add-entry name="note">Inleidende opmerkingen</toc-add-entry>

<p>
Alle pakketten die zijn opgenomen in de officiële Debian-distributie
zijn vrij volgens de <a
href="../social_contract#guidelines">Debian Richtlijnen voor Vrije
Software (DFSG)</a>. Hierdoor kunt u er zeker van zijn dat al deze
pakketten en hun broncode vrijelijk mogen gebruikt en gedistribueerd worden. De
officiële Debian-distributie bestaat uit datgene wat zich in de sectie
&quot;<em>main</em>&quot; van het Debian-archief bevindt.
</p>

<p>
Als een service aan onze gebruikers stellen we ook, in aparte
secties, pakketten beschikbaar die vanwege beperkende licenties of wettelijke
regels niet in het <em>main</em>-archief kunnen worden opgenomen. Deze
secties zijn:
</p>

<div class="centerblock">
  <dl>
    <dt><em>Contrib</em></dt>
      <dd>Pakketten in deze sectie zijn zelf onder een vrije licentie
	  beschikbaar, maar zijn voor hun werking afhankelijk van andere
	  programma's die niet-vrij zijn.</dd>
    <dt><em>Non-Free</em></dt>
      <dd>Pakketten in deze sectie hebben een licentieclausule die het gebruik
	  of de herdistributie van de software inperkt.</dd>
    <dt><em>Non-Free-Firmware</em></dt>
    <dd>
    Deze sectie is beperkt tot apparaatfirmware. Hoewel pakketten in dit gebied in principe dezelfde onderliggende licentiebeperkingen hebben als die in non-free, werd er een speciale uitzondering voor gemaakt: zelfs als ze niet DFSG-vrij zijn, mogen pakketten uit non-free-firmware worden opgenomen in de officiële installatie-images.
    </dd>
  </dl>
</div>

<p>
Merk op dat dezelfde pakketten zich in verschillende distributies
kunnen bevinden, maar met verschillende versienummers.
</p>

<hr />

<toc-add-entry name="view">De lijst van pakketten bekijken</toc-add-entry>

<dl>
  <dt><a href="//packages.debian.org/stable/">De pakketten in
      de distributie <strong>stable</strong> bekijken</a></dt>
    <dd>
    <p>Dit is de meest recente officiële release van de Debian-distributie.
    Dit is stabiele en goed geteste software, die
    alleen verandert als nieuwere pakketten die oplossingen bieden voor
    beveiligings- of bruikbaarheidsproblemen, worden opgenomen.</p>

    <p>Zie de <a href="$(HOME)/releases/stable/">pagina over de stabiele
    release</a> voor nadere informatie.</p>
    </dd>

  <dt><a href="//packages.debian.org/testing/">De pakketten in
      de distributie <strong>testing</strong> bekijken</a></dt>
    <dd>
    <p>Deze sectie bevat pakketten waarvan het de bedoeling is dat ze deel
    gaan uitmaken van de volgende stabiele distributie. Er zijn strikte
    criteria waar een pakket in <q>unstable</q> (zie hieronder) aan moet
    voldoen voordat het kan worden opgenomen in <q>testing</q>.
    Merk op dat er voor <q>testing</q> geen beveiligingsupdates worden gemaakt
    <a href="../security/faq#testing">door het beveiligingsteam</a>.</p>

    <p>Zie de <a href="$(HOME)/releases/testing/">pagina's over de
    testing-distributie</a> voor nadere informatie.</p>
    </dd>

  <dt><a href="//packages.debian.org/unstable/">De pakketten
      in de distributie <strong>unstable</strong> bekijken</a></dt>
    <dd>
    <p>Deze sectie bevat de nieuwste pakketten in Debian. Als een pakket
    eenmaal aan onze criteria voor stabiliteit en kwaliteit
    voldoet, wordt het opgenomen in <q>testing</q>. Net als <q>testing</q>,
    wordt ook <q>unstable</q> niet ondersteund
    <a href="../security/faq#unstable">door het beveiligingsteam</a>.</p>

    <p>Pakketten in unstable zijn het minst getest en kunnen problemen
    bevatten en veroorzaken die ernstig genoeg zijn om de stabiliteit van uw
    systeem aan te tasten. Alleen ervaren gebruikers zouden het gebruik van
    deze distributie moeten overwegen.</p>

    <p>Zie de <a href="$(HOME)/releases/unstable/">pagina's over de
    unstable-distributie</a> voor meer informatie.</p>
    </dd>
</dl>

<p>Merk op dat dezelfde pakketten kunnen voorkomen in verschillende
distributies, maar met verschillende versienummers.</p>

<hr />

<toc-add-entry name="search_packages">De lijst van
pakketten doorzoeken</toc-add-entry>

#include "$(ENGLISHDIR)/distrib/search_packages-form.inc"

<p>Voor een aantal zoektypes zijn snelkoppelingen aanwezig:</p>

<ul>
  <li><code>https://packages.debian.org/<var>naam</var></code> om te
  zoeken op de naam van pakketten.</li>
  <li><code>https://packages.debian.org/src:<var>naam</var></code> om
  te zoeken op de naam van bronpakketten.</li>
</ul>

<hr />

<toc-add-entry name="search_contents">De inhoud van de
pakketten doorzoeken</toc-add-entry>

<p>
Deze zoekmachine geeft u de mogelijkheid om de inhoud van Debian-distributies
voor willekeurige bestanden (of delen van bestandsnamen) die
deel uitmaken van pakketten.
U kunt ook een volledige lijst verkrijgen van bestanden in een bepaald pakket.
</p>

#include "$(ENGLISHDIR)/distrib/search_contents-form.inc"

<p>Er is ook een snelkoppeling beschikbaar:</p>

<ul>
  <li><code>https://packages.debian.org/file:<var>zoekterm</var></code>
  om te zoeken naar paden eindigend met de zoekterm.</li>
</ul>
