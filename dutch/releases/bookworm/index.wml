#use wml::debian::template title="Debian &ldquo;bookworm&rdquo; release-informatie"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="4a4923573595a8539cd9aca5189940d45bcbbb29"

<if-stable-release release="bookworm">

<p>Debian <current_release_bookworm> werd uitgebracht op
<a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "Debian 12.0 werd oorspronkelijk uitgebracht op <:=spokendate('2023-06-10'):>."
/>
De release bevatte verschillende ingrijpende wijzigingen,
beschreven in ons
<a href="$(HOME)/News/2023/20230610">persbericht</a> en de
<a href="releasenotes">Notities bij de release</a>.</p>

#<p><strong>Debian 12 werd vervangen door
#<a href="../trixie/">Debian 13 (<q>trixie</q>)</a>.
#Er worden geen beveiligingsupdates meer uitgebracht sinds <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### Deze paragraaf is indicatief; kijk ze na voor publicatie!
#<p><strong>Bookworm geniet evenwel van langetermijnondersteuning (Long Term Support - LTS) tot
#30 juni 2028. De LTS is beperkt tot i386, amd64, armel, armhf and arm64.
#Alle andere architecturen worden niet langer ondersteund in bookworm.
#Raadpleeg voor meer informatie de <a
#href="https://wiki.debian.org/LTS">sectie over LTS op de Wiki van Debian</a>.
#</strong></p>

<p>Raadpleeg de <a href="debian-installer/">installatie-informatie</a>-pagina
en de <a href="installmanual">Installatiehandleiding</a> over het verkrijgen
en installeren van Debian. Zie de instructies in de
<a href="releasenotes">Notities bij de release</a> om van een oudere Debian
release op te waarderen.</p>

### Wat volgt activeren wanneer de LTS-periode begint.
#<p>Ondersteunde architecturen tijdens de langetermijnondersteuning:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Ondersteunde computerarchitecturen bij de initiële release van bookworm:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>In tegenstelling tot wat we zouden wensen, kunnen er enkele problemen bestaan
in de release, ondanks dat deze <em>stabiel</em> wordt genoemd. We hebben
<a href="errata">een overzicht van de belangrijkste bekende problemen</a>
gemaakt en u kunt ons altijd
<a href="../reportingbugs">andere problemen rapporteren</a>.</p>

<p>Tot slot, maar niet onbelangrijk, een overzicht van de
<a href="credits">mensen</a> die deze release mogelijk maakten.</p>

</if-stable-release>
