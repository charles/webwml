#use wml::debian::blend title="Specifieke uitgave van Debian voor GIS" NOHEADER="true" BLENDHOME="true" BARETITLE="true"
#use wml::debian::recent_list
#use wml::debian::blends::gis
#use wml::debian::translation-check translation="794fc406da417697ec055ef6c78e0cff7510f391"

# translators: you need to translate the navbar.inc file so this page (when translated) is correctly built and shown

<div id="splash">
        <h1 id="gis">Specifieke uitgave van Debian voor GIS</h1>
</div>

<p>De specifieke uitgave van Debian voor GIS is een zogenaamde
"<a href="../">Debian Pure Blend</a>" (een doelgroepspecifiek geheel
van Debian-pakketten) met als doel Debian te ontwikkelen tot de beste
distributie voor toepassingen en gebruikers van <strong>Geografische
InformatieSystemen</strong>.</p>

<p>Veel GIS-gerelateerde software en bibliotheken (zoals GRASS GIS, GDAL en
PROJ.4) zijn al aanwezig in Debian.</p>

<p>Dankzij de inspanningen van het Debian GIS-team hebben we momenteel veel
interessante GIS-pakketten in het hoofdarchief van Debian. Andere programma's
zijn goed op weg om opgenomen te worden in het archief.</p>

<p>De Debian-bronpakketten die door het Debian GIS-team worden onderhouden, worden ook gebruikt bij inspanningen om de huidige GIS-software te ondersteunen voor op van Debian afgeleide distributies, zoals <a href="https://wiki.ubuntu.com/UbuntuGIS">UbuntuGIS</a>, dat aan Ubuntu aangepaste versies van GIS-pakketten biedt, en voor <a href="http://live.osgeo.org/">OSGeo-Live</a>, een op Ubuntu gebaseerde distributie.</p>

<div id="hometoc">
<ul id="hometoc-cola">
  <li><a href="./about">Over de specifieke uitgave</a></li>
  <li><a href="./contact">Contact opnemen met ons</a></li>
</ul>
<ul id="hometoc-colb">
  <li><a href="./get/">De specifieke uitgave verkrijgen</a>
  <ul>
    <li><a href="./get/metapackages">Het gebruik van de metapakketten</a></li>
  </ul></li>
</ul>
<ul id="hometoc-colc">
  <li><a href="./deriv">Afgeleide distributies</a></li>
</ul>
<ul id="hometoc-cold">
  <li><a href="https://wiki.debian.org/DebianGis">Ontwikkeling</a>
    <ul>
      <li><a href="<gis-policy-html/>">Beleid van het GIS-Team</a></li>
    </ul>
  </li>
</ul>
</div>


