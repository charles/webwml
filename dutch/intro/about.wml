#use wml::debian::template title="Over Debian"
#use wml::debian::translation-check translation="6bc817b4c53e0c14085dc707e8e595da263a7e8f"
#include "$(ENGLISHDIR)/releases/info"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<ul class="toc">
<li><a href="#what">Wat is Debian eigenlijk?</a></li>
<li><a href="#free">Is het allemaal gratis?</a></li>
<li><a href="#CD">Je zegt gratis maar de cd’s en bandbreedte kosten geld!</a>
<li><a href="#disbelief">De meeste software kost honderden euro’s. Hoe
    kunnen jullie dat zomaar weggeven?</a></li>
<li><a href="#hardware">Welke hardware wordt ondersteund?</a></li>
<li><a href="#info">Ik heb meer informatie nodig voor ik
    beslis.</a></li>
<li><a href="#why">Ik ben nog steeds niet overtuigd. Wat zijn de voor-
    en nadelen van Debian?</a></li>
<li><a href="#install">Hoe kom ik aan Debian?</a></li>
<li><a href="#supp">Ik kan het niet allemaal zelf installeren. Waar kan
    ik ondersteuning krijgen voor Debian?</a></li>
<li><a href="#who">Wie zijn jullie eigenlijk?</a></li>
<li><a href="#users">Wie gebruikt er Debian?</a>
<li><a href="#history">Hoe is het allemaal begonnen?</a></li>
</ul>


<h2><a name="what">Wat is Debian eigenlijk?</a></h2>

<p>Het <a href="$(HOME)/">Debian Project</a> is een samenwerkingsverband
tussen individuen die samen een <a href="free">vrij</a> besturingssysteem
willen maken. Dat besturingssysteem dat we hebben ontwikkeld heet
<strong>Debian</strong>.

<p>Een besturingssysteem is de verzameling van basis- en hulpprogramma’s die
ervoor zorgen dat uw computer werkt. De kern van een besturingssysteem is de
kernel. De kernel is het meest fundamentele programma op de computer. Hij zorgt
voor het beheer van de computer op een basisniveau en maakt het mogelijk om
andere programma’s te starten. Momenteel gebruiken
Debian-systemen de <a href="https://www.kernel.org/">Linux</a>-kernel
of de <a href="https://www.freebsd.org/">FreeBSD</a>-kernel. Linux is een stuk
software dat gestart werd door <a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus
Torvalds</a> en ondersteund wordt door duizenden programmeurs van over de
hele wereld. FreeBSD is een besturingssysteem bestaande uit een kernel en
andere software.

Er wordt aan gewerkt om Debian geschikt te maken voor andere kernels, in eerste
instantie <a href="https://www.gnu.org/software/hurd/hurd.html">de Hurd</a>. De
Hurd is een verzameling servers die bovenop een microkernel (zoals Mach)
draaien om verschillende functies te implementeren. De Hurd is vrije software,
gemaakt door het <a href="https://www.gnu.org/">GNU project</a>.

<p>Een groot deel van de basisprogramma’s waar het besturingssysteem uit
bestaat komen voort uit het <a href="https://www.gnu.org/">GNU project</a>;
vandaar de namen: GNU/Linux en GNU/Hurd. Deze programma’s zijn
ook vrij.

<p>Wat mensen willen is natuurlijk applicatiesoftware: programma’s die
hen helpen gedaan te krijgen wat ze willen doen, van het bewerken van
documenten, het beheren van een bedrijf, het spelen van spelletjes, tot
het schrijven van meer software. Debian bestaat uit meer dan
<packages_in_stable>&nbsp;<a href="$(DISTRIB)/packages">pakketten</a>
(voorgecompileerde software, samengebundeld voor een eenvoudige
installatie op uw machine), een pakketbeheerprogramma (APT) en
andere hulpprogramma's die het beheer van duizenden
pakketten op duizenden computers even gemakkelijk maakt als
het installeren van één enkele toepassing.
En allemaal <a href="free">vrij</a>.

<p> Het lijkt een beetje op een toren. De voet is de kernel.
Daarbovenop alle basisprogramma’s en vervolgens de software die u op uw
computer draait.  De top is Debian &mdash; voorzichtig regelend en
organiserend zodat alles samenwerkt.

<h2>Is het  allemaal <a href="free" name="free">gratis?</a></h2>

<p>Misschien vraagt u zichzelf af: waarom steken mensen zoveel van hun
eigen tijd in het schrijven en zorgvuldig verpakken van software, om deze
vervolgens weg te <em>geven</em>?  De antwoorden zijn zo verschillend
als de mensen die bijdragen.  Sommigen zijn graag behulpzaam.  Velen
schrijven programma’s om meer te leren over computers.  Steeds meer zijn
er op zoek naar manieren om de hoge prijzen van software te vermijden.
Een groeiende groep helpt als dank voor de prachtige vrije software die
ze van anderen hebben gekregen.  Veel academici schrijven vrije software
om de resultaten van hun onderzoek wijder te verspreiden.  Bedrijven
helpen met het onderhouden van vrije software om invloed te kunnen
hebben op de verdere ontwikkeling; er is geen snellere manier om een
nieuwe functie te krijgen dan hem zelf te implementeren!  En natuurlijk
zijn er een hoop ontwikkelaars onder ons die het gewoon leuk vinden.

<p>Debian is zo gebonden aan vrije software dat het ons nuttig
leek deze band te formaliseren in een document. Zo is ons
<a href="$(HOME)/social_contract">Sociale Contract</a> geboren.

<p>Hoewel Debian gelooft in vrije software, zijn er gevallen waarin mensen
niet-vrije software op hun computer willen of moeten gebruiken. Waar mogelijk
ondersteunt Debian dit. Er is een groeiend aantal pakketten die enkel en alleen
de taak hebben om niet-vrije software te installeren op een Debian systeem.

<h2><a href="free" name="CD">Vrij?</a> Maar de cd’s kosten geld.</h2>

<p>Mogelijk vraagt u zich af: waarom moet ik een verkoper betalen voor
een cd met vrije software of een internetprovider voor het downloaden
ervan?

<p>Als u een cd koopt, betaalt u voor de tijd die erin gestoken is om de
cd te persen, het geld dat nodig is om de schijfjes te maken en het risico (in
het geval dat niet alle cd’s worden verkocht). In andere woorden: u betaalt
voor een fysiek medium dat wordt gebruikt om de software te verspreiden, niet
voor de software zelf.

<p>Als gezegd wordt dat software "vrij" is, bedoelen we de <strong>vrije beschikbaarheid</strong> ervan. Dit betekent dan ook niet per se dat de
software gratis is. U kunt meer lezen over <a href="free">wat we precies
met vrije software bedoelen</a> en <a
href="https://www.gnu.org/philosophy/free-sw">wat de Free Software
Foundation hierover zegt</a>.

<h2><a name="disbelief">De meeste software kost meer dan 100 euro. Hoe
kunnen jullie dat zomaar weggeven?</a></h2>

<p>Een betere vraag is hoe het komt dat softwarebedrijven zo veel
kunnen vragen?  Het maken van software is niet als het maken van een
auto. Als er eenmaal een exemplaar van de software is, zijn de productiekosten
om er nog een miljoen te maken minimaal (er is een reden
waarom Microsoft zo veel miljarden op de bank heeft).

<p>Bekijk het van de andere kant: als u een eindeloze voorraad zand in de
tuin had liggen zou u misschien wel bereid zijn om zand weg te geven.
Het zou echter dom zijn om voor de vrachtwagen te betalen die het zand naar
anderen brengt. U zou het hen zelf laten komen halen (equivalent met
downloaden van Internet), of ze kunnen iemand anders betalen om het bij
hen op de stoep te laten leggen (equivalent met het kopen van een cd).
Dit is precies zoals Debian werkt en waarom Debian cd’s en dvd’s over het
algemeen zo goedkoop zijn (slechts ongeveer 10 euro voor 4 dvd’s).

<p>Debian verdient geen geld aan de verkoop van cd’s. Toch is er geld
nodig voor uitgaven zoals domeinregistratie en hardware. Daarom vragen
we om uw cd’s te kopen bij een van de <a href="../CD/vendors/">cd-verkopers</a> die een gedeelte van het aankoopbedrag aan Debian
<a href="$(HOME)/donations">doneren</a>.

<h2><a name="hardware">Welke hardware wordt ondersteund?</a></h2>

<p>Debian draait op vrijwel alle pc’s, inclusief de meeste oudere
modellen. Over het algemeen ondersteunt elke nieuwe release van Debian
een groter aantal computerachitecturen. Voor een volledig overzicht van
degene die op dit moment worden ondersteund, kunt u de
<a href="../releases/stable/">documentatie van de stabiele release</a>
raadplegen.

<p>Vrijwel alle algemeen voorkomende hardware wordt ondersteund. Als u
zeker wilt weten dat alle apparaten die op uw machine zijn aangesloten,
worden ondersteund, kunt u de webpagina's
<a href="https://wiki.debian.org/InstallingDebianOn/">wikipagina DebianOn</a> en
<a href="https://linux-hardware.org/">Hardware voor Linux</a> raadplegen.

<p>Er is een aantal bedrijven dat ondersteuning moeilijk maakt door
specificaties van hun hardware niet beschikbaar te maken. Dit betekent
dat u waarschijnlijk zulke hardware niet kunt gebruiken met GNU/Linux.
Sommige bedrijven leveren niet-vrije stuurprogramma’s voor hun hardware,
maar daarbij kunnen problemen ontstaan doordat het bedrijf failliet gaat
of stopt met het ondersteunen van uw hardware. We raden u aan om alleen
hardware te kopen van fabrikanten die <a href="free">vrije</a>
stuurprogramma’s voor hun hardware leveren.

<h2><a name="info">Ik ben op zoek naar meer informatie.</a></h2>

<p>Voor nadere informatie kunt u onze <a href="$(DOC)/manuals/debian-faq/">Veel
Voorkomende Vragen (FAQ)</a> bekijken.

<h2><a name="why">Ik ben nog steeds niet overtuigd.</a></h2>

<p>Geloof ons niet op ons woord &mdash; probeer Debian zelf uit. Aangezien
hardeschijfruimte goedkoper is geworden, heeft u vast wel 2GB over.
Als u geen grafische desktop wilt of nodig heeft, dan is 600MB voldoende.
Debian kan eenvoudig in deze extra ruimte worden geïnstalleerd en
samenleven met uw huidige besturingssysteem. Als u ooit meer ruimte nodig
heeft, kunt u eenvoudig een van uw besturingssystemen weggooien (en nadat
u de kracht van het Debian systeem heeft gezien, zijn we er zeker van dat u
Debian niet zult verwijderen).

<p>Aangezien het uitproberen van een nieuw besturingssysteem wat van uw
kostbare tijd kost, is het begrijpelijk dat u nog ietwat terughoudend
bent. Daarom hebben we een overzicht gemaakt met de
<a href="why_debian">voor- en nadelen van Debian</a>. Dit kan u helpen te
bepalen of u het de moeite waard vindt. We hopen dat u onze eerlijkheid
en openheid waardeert.

<h2><a name="install">Hoe kom ik aan Debian?</a></h2>

<p>De meest populaire keuze is om Debian van cd’s te installeren. Deze
cd’s kunt u vaak tegen kostprijs van de cd-schijf kopen. Als u een
snelle internetverbinding heeft, kunt u Debian ook downloaden en
installeren via het internet.</p>

<p>Zie de <a href="../distrib/">pagina over het verkrijgen van
Debian</a> voor meer informatie.</p>

<h2><a name="support">Ik kan het niet allemaal zelf installeren.
    Hoe krijg ik ondersteuning?</a></h2>

<p>U kunt hulp krijgen door het lezen van de documentatie, die zowel op
het www beschikbaar is als in de vorm van pakketten die u op uw eigen
systeem kunt installeren. U kunt ook contact met ons opnemen via een van
de mailinglijsten of via IRC. U kunt zelfs een consultant inhuren om het
werk voor u te doen.</p>

<p>Zie onze <a href="../doc/">documentatie-</a> en <a
href="../support">ondersteuningspagina’s</a> voor meer informatie.</p>

<p>Met uw vragen kunt u ook terecht op de Nederlandstalige mailinglijst voor
<a href="https://lists.debian.org/debian-user-dutch/">gebruikers van Debian</a>.

<h2><a name="who">Wie zijn jullie eigenlijk?</a></h2>

<p>Debian wordt gemaakt door bijna duizend actieve
ontwikkelaars, verspreid
<a href="$(DEVEL)/developers.loc">over de hele wereld</a>,
die als vrijwilliger meewerken in hun vrije tijd.
Weinig van die ontwikkelaars hebben elkaar in levenden lijve ontmoet.
Communicatie verloopt voornamelijk via e-mail (mailinglijsten op
lists.debian.org) en IRC (#debian kanaal op irc.debian.org).
</p>

<p> Het Debian Project heeft een zorgvuldige <a
href="organization">organisatiestructuur</a>. Voor meer informatie over
hoe Debian er van binnenuit uitziet, kunt u eens rondkijken in <a
href="$(DEVEL)/">het ontwikkelaarshoekje</a>.</p>

<p>De volledige lijst van officiële leden van Debian is te vinden op
<a href="https://nm.debian.org/members">nm.debian.org</a>, waar
het lidmaatschapsbeheer gebeurt. Een ruimere lijst van mensen die
bijdragen leveren aan Debian, vindt men op
<a href="https://contributors.debian.org">contributors.debian.org</a>.</p>


<h2><a name="users">Wie gebruikt er Debian?</a></h2>

<p>Hoewel er geen precieze statistieken beschikbaar zijn (omdat Debian
niet vereist dat gebruikers zich registreren), is er duidelijk bewijs dat
een grote verscheidenheid aan organisaties, groot en klein, Debian gebruikt,
net als vele duizenden individuen. Op de <a href="../users">Wie gebruikt er
Debian?</a> pagina kunt u een overzicht vinden van bekende gebruikers van Debian
met een korte omschrijving van hoe en waarom ze Debian gebruiken.

<h2><a name="history">Hoe is het allemaal begonnen?</a></h2>

<p>Het Debian project werd in augustus 1993 gestart door Ian Murdock,
als nieuwe distributie die openlijk ontwikkeld zou worden, in de geest
van Linux en GNU. Het was de bedoeling om Debian voorzichtig en
zorgvuldig op te bouwen en het net zo goed te onderhouden en
ondersteunen. Het begon als een kleine groep Vrije Software hackers en
groeide naar een grote, goed georganiseerde gemeenschap van ontwikkelaars en
gebruikers. Zie ook <a href="$(DOC)/manuals/project-history/">de
gedetailleerde geschiedenis</a>.

<p>Veel mensen vragen naar de juist uitspraak van &quot;Debian&quot;.
&quot;Debian&quot; wordt uitgesproken als /&#712;de.bi.&#601;n/. Het komt van de
namen van de schepper van Debian, Ian Murdock en zijn vrouw, Debra.
