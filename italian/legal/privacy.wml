#use wml::debian::template title="Privacy Policy"
#use wml::debian::translation-check translation="cdca78e365802b139ce77851fc2aa0e6ef0bcee5" maintainer="Ceppo"

<p>
<strong>Attenzione:</strong> questa traduzione è solo informativa e non ha
valore legale. Per la versione legale, leggere <a
href="privacy.en.html">l'originale in inglese</a>.
</p>

<p>
Il <a href="https://www.debian.org/">Progetto Debian</a> è un'associazione
volontaria di individui che hanno fatto fronte comune per creare un sistema
operativo libero, chiamato Debian.
</p>

<p>
Chi desidera usare Debian non è tenuto a fornire al progetto alcuna
informazione personale; il software è scaricabile liberamente senza
registrazione né altre forme di identificazione sia dai mirror ufficiali
gestiti dal progetto che da numerose terze parti.
</p>

<p>
Vari altri aspetti dell'interazione con il Progetto Debian implicano tuttavia
la raccolta di informazioni personali. Questo principalmente sotto forma di
nomi e indirizzi di posta elettronica nelle email ricevute dal progetto; tutte
le liste di messaggi di Debian sono archiviate pubblicamente, così come tutte
le interazioni con il sistema di tracciamento dei bug. Ciò per rispettare il
nostro <a href="https://www.debian.org/social_contract">Contratto Sociale</a>,
in particolare le nostre affermazioni che renderemo alla comunità del free
software (#2) e che non nasconderemo i nostri problemi (#3). Non effettuiamo
altri trattamenti su nessuna delle informazioni che deteniamo, ma ci sono casi
in cui esse sono condivise automaticamente con terze parti (ad esempio email
alle liste o interazioni con il sistema di tracciamento dei bug).
</p>

<p>
La lista seguente classifica i vari servizi forniti dal progetto, le
informazioni usate da tali servizi e il motivo per cui sono richieste.
</p>

<p>
Le domande relative a questi servizi dovrebbero essere rivolte preferibilmente
al proprietario del servizio. Se non è chiaro chi sia, possono essere rivolte
al Data Protection team a <a
href="mailto:data-protection@debian.org">data-protection@debian.org</a>, che
cercherà di inoltrare la richiesta al team corretto.
</p>

<p>
Tenere conte del fatto che (a meno che sia specificato diversamente in questa
pagina) le macchine e i servizi al dominio <strong>debian.net</strong> non sono
parte del progetto Debian ufficiale: sono forniti da individui che hanno un
legame con il progetto, non dal progetto stesso. Domande sui dati esatti che
questi servizi detengono dovrebbero essere indirizzate ai proprietario del
servizio, non al progetto Debian.
</p>

<h2>Collaboratori (<a href="https://contributors.debian.org/">contributors.debian.org</a>)</h2>

<p>
Il sito Debian Contributors fornisce dati aggregati su dove qualcuno ha
contribuito al Progetto Debian, che sia segnalando un bug, facendo un
caricamento nell'archivio, scrivendo in una lista di messaggi o interagendo in
vari altri modi con il Progetto. Esso riceve le informazioni dai servizi in
questione (dettagli su un identificativo come nome utente e momento dell'ultimo
contributo) e fornisce un punto di riferimento unico per vedere dove il
Progetto conserva informazioni su un individuo.
</p>

<h2>L'archivio (<a href="https://ftp.debian.org/debian/">ftp.debian.org</a>)</h2>

<p>
Il metodo di distribuzione principale di Debian è la sua rete di archivi
pubblici. L'archivio è costituito da tutti i pacchetti binari e dal codice
sorgente loro associato, che include informazioni personali sotto forma di nomi
e indirizzi email conservati come parte di changelog, informazioni sul
copyright e documentazione generale. La maggior parte di queste informazioni è
fornita dal codice sorgente distribuito dagli sviluppatori originali, a cui
Debian aggiunge ulteriori informazioni per tracciare la paternità e il
copyright per assicurarsi che le licenze siano documentate correttamente e le
Linee Guida Debian per il Software Libero vengano rispettate.
</p>

<h2>Sistema di tracciamento dei bug (<a href="https://bugs.debian.org/">bugs.debian.org</a>)</h2>

<p>
Con il sistema di tracciamento dei bug si interagisce per email e esso conserva
tutte le email ricevute in relazione a un bug come parte della cronologia di
tale bug. Affinché il progetto possa affrontare efficacemente i problemi
trovati nella distribuzione, e per consentire agli utenti di vedere i dettagli
di questi problemi e se sia disponibile una soluzione o un espediente,
l'interezza del sistema di tracciamento dei bug è accessibile pubblicamente.
Quindi ogni informazione, inclusi nomi e indirizzi email come parte degli
header delle email, inviata al BTS sarà archiviata e pubblicamente disponibile.
</p>

<h2>DebConf (<a href="https://www.debconf.org/">debconf.org</a>)</h2>

<p>
La struttura di registrazione dei DebConf conserva i dettagli del pubblico
delle conferenze. Questi sono necessari per determinare l'idoneità dei borsisti
e l'associazione al progetto e per contattare il pubblico con i dettagli
appropriati. Potrebbero anche essere condivisi con i fornitori della
conferenza, ad esempio il nome e la data di partecipazione di chi soggiorna
negli alloggi forniti dalla conferenza saranno condivisi con il fornitore
dell'alloggio.
</p>

<h2>Amministratori degli Account Debian</h2>

<p>
Gli Amministratori degli Account Debian (Debian Account Manager, DAM) detengono
informazioni sui membri del progetto che sono esposte perlopiù dalle interfacce
del sito Nuovi Membri (<a
href="https://nm.debian.org/">https://nm.debian.org/</a>), descritte sotto. In
rari casi detengono anche informazioni nei loro archivi interni, come quando
vengono presentati reclami relativi a membri del progetto ed è necessario
avviare un'indagine. Dato che è importante essere in grado di fare riferimento
a questi archivi in futuro (ad esempio per sapere, nel caso di un ex membro che
desidera rientrare nel progetto, se ci sono fatti risalenti alla sua uscita
rilevanti per la nuova candidatura) non c'è una data di scadenza prestabilita
per questi archivi.
</p>

<h2>LDAP degli sviluppatori (<a href="https://db.debian.org">db.debian.org</a>)</h2>

<p>
I dettagli dei collaboratori del progetto (sviluppatori e altri con account
ospite) che hanno accesso a un account sulle macchine all'interno
dell'infrastruttura di Debian sono conservati nell'infrastruttura LDAP del
progetto. Essa conserva principalmente nome, nome utente e informazioni di
autenticazione. Tuttavia dà agli sviluppatori anche la possibilità facoltativa
di fornire informazioni aggiuntive come genere, informazioni di messaggistica
istantanea (IRC/XMPP), nazione e indirizzo o numero di telefono, nonché un
messaggio se sono in vacanza.
</p>

<p>
Nome, nome utente e alcune informazioni fornite volontariamente sono
disponibili liberamente dall'interfaccia web o con una ricerca LDAP. Dettagli
aggiuntivi sono condivisi solo con altri individui che hanno accesso
all'infrastruttura di Debian e si intende fornire ai membri del progetto un
luogo centralizzato per scambiare queste informazioni di contatto. Tali
dettagli non sono raccolti esplicitamente in nessun momento e possono sempre
essere rimossi accedendo all'interfaccia di db.debian.org o inviando un
messaggio firmato all'interfaccia email. Per più informazioni si vedano <a
href="https://db.debian.org/">https://db.debian.org/</a> e <a
href="https://db.debian.org/doc-general.html">https://db.debian.org/doc-general.html</a>.
</p>

<h2>Gitlab (<a href="https://salsa.debian.org/">salsa.debian.org</a>)</h2>

<p>
salsa.debian.org fornisce un'istanza del sistema di gestione del ciclo di vita
DevOps <a href="https://about.gitlab.com/">GitLab</a>. È usato principalmente
dal Progetto per consentire ai collaboratori del Progetto di ospitare
repository di software usando Git e incoraggiare la collaborazione. Di
conseguenza richiede diverse informazioni personali per la gestione degli
account. Per i membri del Progetto è collegato al sistema LDAP centrale di
Debian, ma gli ospiti possono registrare un account e devono fornire nome e
indirizzo email per facilitare la creazione e l'uso dell'account.
</p>

<p>
Lo scopo principale di salsa è gestire la nostra cronologia git. Si veda sotto.
</p>

<h2><a name="git">git</a></h2>

<p>
Debian mantiene diversi server git, che contengono codice sorgente e materiale
simile, e le loro revisioni e cronologie dei contributi. Inoltre, tipicamente
singoli collaboratori di Debian hanno informazioni gestite in git.
</p>

<p>
La cronologia di git contiene il nome e l'indirizzo email dei collaboratori
(inclusi segnalatori di bug, autori, revisori e così via). git conserva
queste informazioni in modo permanente in una cronologia a cui possono solo
essere aggiunti nuovi elementi. Usiamo una cronologia di questo tipo perché ha
importanti caratteristiche di integrità, tra cui in particolare una notevole
protezione dalle modifiche non tracciate. La manteniamo indefinitamente in modo
da poter sempre verificare il copyright e altre informazioni sullo stato legale
dei contributi e per poter sempre identificare il creatore per ragioni di
integrità del software.
</p>

<p>
La caratteristica della sola estensibilità del sistema git implica che
qualunque modifica dei dettagli di questi commit una volta incorporati nel
repository sia estremamente distruttiva e in alcuni casi (come quando sono
usati commit firmati) impossibile. Quindi evitiamo le modifiche tranne che in
casi estremi. Dove appropriato, usiamo funzioni di git (ad esempio
<code>mailmap</code>) per fare sì che le informazioni personali storiche,
sebbene mantenute, possano essere omesse o corrette quando mostrate o usate.
</p>

<p>
A meno che ci siano ragioni eccezionali per fare diversamente, le cronologie
git di Debian, incluse le informazioni personali sui collaboratori, sono
completamente pubbliche.
</p>

<h2>Gobby (<a href="https://gobby.debian.org/">gobby.debian.org</a>)</h2>

<p>
Gobby è un editor di testo online, che attribuisce i contributi e le modifiche
agli utenti connessi. Non è necessaria autenticazione per connettersi al
sistema e gli utenti possono scegliere il nome utente che preferiscono.
Tuttavia, sebbene dal servizio non sia fatto alcun tentativo di tracciare chi
possiede i nomi utente, dovrebbe essere compreso che potrebbe essere possibile
ricondurre il nome utente all'individuo basandosi sull'uso comune di tale nome
utente o sul contenuto pubblicato attraverso il sistema in un documento
collaborativo.
</p>

<h2>Liste di messaggi (<a href="https://lists.debian.org/">lists.debian.org</a>)</h2>

<p>
Le liste di messaggi sono il principale meccanismo di comunicazione nel
Progetto Debian. Quasi tutte le liste legate al progetto sono aperte e quindi
disponibili a tutti per leggerle e/o scrivervi. Tutte le liste sono anche
archiviate; per le liste pubbliche questo significa che sono accessibili
attraverso il web. Questo soddisfa l'impegno alla trasparenza del progetto e
aiuta i nostri utenti e sviluppatori a capire cosa succede nel progetto o a
capire le ragioni storiche di certi suoi aspetti. A causa della natura delle
email questi archivi potrebbero quindi contenere informazioni personali, come
nomi e indirizzi email.
</p>


<h2>Liste Alioth
(<a href="https://alioth-lists.debian.net/">alioth-lists.debian.net</a>)</h2>

<p>
Le liste Alioth forniscono liste di messaggi aggiuntive, maggiormente specifiche
, per il progetto Debian. La maggior parte delle liste di messaggi in
questo progetto di sistema è aperta e quindi disponibile a tutti per leggere
e/o scrivere. Molte liste sono anche archiviate; per le liste pubbliche questo
significa che sono accessibili attraverso il web. Questo soddisfa l'impegno
alla trasparenza del progetto e aiuta i nostri utenti e sviluppatori a capire
cosa succede nel progetto o a capire le ragioni storiche di certi suoi aspetti.
A causa della natura delle email questi archivi potrebbero quindi contenere
informazioni personali, come nomi e indirizzi email.
</p>

<p>
Il servizio delle liste Alioth è conosciuto anche come
<tt>lists.alioth.debian.org</tt>.
</p>

<h2>Sito Nuovi Membri (<a href="https://nm.debian.org/">nm.debian.org</a>)</h2>

<p>
I collaboratori al Progetto Debian che desiderano formalizzare il loro
coinvolgimento possono scegliere di candidarsi al processo Nuovi Membri. Questo
consente loro di ottenre la possibilità di caricare i propri pacchetti
(attraverso la Debian Maintainership) o di diventare membri del progetto con
diritto di voto e all'account (Sviluppatore Debian, nelle varianti caricatore e
non caricatore). Come parte di questo processo sono raccolti vari dettagli
personali, a partire da nome, indirizzo email e dettagli delle chiavi di
cifratura e firma. Le candidature Full Project prevedono anche che il candidato
contatti un Amministratore delle Candidature che intratterrà con lui una
conversazione via email per assicurarsi che il nuovo membro comprenda i
principi su cui si basa Debian e abbia le capacità appropriate per interagire
con l'infrastruttura del Progetto. Questa conversazione via email è archiviata
e disponibile per il candidato e gli Amministratori delle Candidature
attraverso l'interfaccia nm.debian.org. Dettagli aggiuntivi delle candidature
prominenti sono visibili sul sito, consentendo a chiunque di vedere lo stato
dell'elaborazione dei Nuovi Membri all'interno del Progetto per assicurare un
livello di trasparenza adeguato.
</p>

<h2>Concorso di popolarità (<a href="https://popcon.debian.org/">popcon.debian.org</a>)</h2>

<p>
«popcon» traccia i pacchetti installati in un sistema Debian, per consentire la
raccolta di statistiche sui pacchetti ampiamente diffusi e su quelli non più in
uso. Esso usa il pacchetto facoltativo «popularity-contest» per raccogliere
queste informazioni, richiedendo un'adesione esplicita per farlo. Questo
fornisce indicazioni utili su dove investire le risorse degli sviluppatori, ad
esempio quando bisogna fare il port di vecchie applicazioni affinché usino
nuove versioni di una libreria. Ogni istanza di popcon genera un ID unico di
128 bit casuali che viene usato per tracciare i dati inviati da una stessa
macchina. Non è fatto nessun tentativo di ricondurre questo ID a un indiviuo.
Le informazioni vengono inviate per email o attraverso HTTP e quindi è
possibile che informazioni personali trapelino sotto forma di indirizzi IP o
header delle email. Queste informazioni sono disponibili solo agli
Amministratori del Sistema Debian e agli amministratori di popcon; tutti i
metadati di questo tipo sono rimossi prima che gli invii di dati siano resi
accessibili al progetto nel suo insieme. Tuttavia gli utenti dovrebbero essere
consapevoli che firme dei pacchetti uniche (come quelle di pacchetti creati
localmente o installati da pochi utenti) potrebbero rendere possibile dedurre
l'appartenenza delle macchine a un individuo specifico.
</p>

<p>
I dati grezzi sono conservati per 24 ore, per consentire repliche in caso di
problemi con i meccanismi di elaborazione. Gli invii anonimizzati sono
conservati al massimo per 20 giorni. I rapporti riassuntivi, che non contengono
informazioni personali, sono mantenuti indefinitamente.
</p>

<h2>Istantanee (<a href="http://snapshot.debian.org/">snapshot.debian.org</a>)</h2>

<p>
L'archivio delle istantanee fornisce una prospettiva storica dell'archivio di
Debian (ftp.debian.org, sopra), consentendo di accedere a vecchi pacchetti
basandosi sulle date e i numeri di versione. Non contiene informazioni
aggiuntive rispetto all'archivio principale (e quindi può contenere
informazioni personali sotto forma di nomi e indirizzi email all'interno dei
changelog, informazioni sul copyright e altra documentazione), ma può contenere
pacchetti che non sono più parte dei rilasci distribuiti di Debian. Questo
fornisce a sviluppatori e utenti una risorsa utile per tracciare regressioni in
pacchetti software o fornire uno specifico ambiente per eseguire
un'applicazione specifica.
</p>

<h2>Voti (<a href="https://vote.debian.org/">vote.debian.org</a>)</h2>

<p>
Il sistema di tracciamento dei voti (devotee) traccia lo stato delle
Risoluzioni Generali in corso e i risultati dei voti passati. Nella maggior
parte dei casi questo significa che una volta che il periodo di voto è
terminato i dettagli di chi ha votato (nomi utente e mappatura dei nomi) e come
ha votato diventano visibili pubblicamente. Per devotee solo i membri del
Progetto sono votanti validi e solo i voti validi sono tracciati dal sistema.
</p>

<h2>Wiki (<a href="https://wiki.debian.org/">wiki.debian.org</a>)</h2>

<p>
La Debian Wiki fornisce risorse di supporto e documentazione per il Progetto,
modificabili da chiunque. Come parte di questo, i contributi sono tracciati nel
tempo e associati con account utente sulla wiki; ogni modifica a una pagina è
tracciata per consentire di annullare quelle errate ed esaminare facilmente le
informazioni aggiornate. Questo tracciamento fornisce dettagli sull'utente
responsabile della modifica o sull'indirizzo IP da cui la modifica è stata
effettuata. Gli account utente consentono inoltre agli utenti di iscriversi
alle pagine per restare informati sulle relative modifiche o vedere i dettagli
delle modifiche nell'intera wiki dall'ultima volta che hanno acceduto. Di norma
gli account utente hanno lo stesso nome dell'utente, ma non viene effettuata
alcuna validazione sui nomi degli account e l'utente può scegliere qualunque
nome libero. Un indirizzo email è richiesto allo scopo di fornire un meccanismo
per recuperare la password dell'account e notificare l'utente di qualunque
modifica alle pagine a cui è iscritto.
</p>

<h2>Server git di dgit (<a href="https://browse.dgit.debian.org/">*.dgit.debian.org</a>)</h2>

<p>
Il server git di dgit è il repository delle cronologie git (controllo delle
revisioni) dei pacchetti caricati su Debian con <code>dgit push</code>, usate
con <code>dgit clone</code>. Contiene informazioni simili all'archivio di
Debian e all'istanza gitlab Salsa: copie (e istantanee passate) delle stesse
informazioni - nel formato di git. Tranne che per i pacchetti che sono per la
prima volta sotto valutazione da parte degli ftpmaster di Debian per
l'inclusione in Debian, tutte queste informazioni sono sempre pubbliche e
conservate per sempre. Si veda <a href="#git">git</a>, sopra.
</p>

<p>
A scopo di controllo degli accessi, c'è una lista gestita manualmente delle
chiavi ssh pubbliche dei Manutentori Debian che hanno chiesto di essere
aggiunti. Questa lista gestita manualmente non è pubblica. Si spera di
sostituirla con dati completi gestiti da un altro servizio di Debian.
</p>

<h2>
Echelon
</h2>

<p>
Echelon è un sistema usato dal Progetto  per tracciare l'attività dei membri;
in particolare controlla le infrastrutture delle liste di messaggi e
dell'archivio, cercando messaggi e caricamenti per registrare che un membro di
Debian è attivo. Quindi traccia solo i dettagli di individui che hanno un
account nell'infrastruttura di Debian. Queste informazioni sono usate per
determinare se un membro del progetto è inattivo o assente e quindi potrebbe
esserci la necessità tecnica di bloccare il suo account o ridurre in altro modo
i suoi permessi di accesso per assicurarsi che i sistemi di Debian siano
mantenuti in sicurezza.
</p>

<h2>Registri relativi ai servizi</h2>

<p>
In aggiunta ai servizi esplicitamente elencati sopra, l'infrastruttura di
Debian registra dettagli riguardanti l'accesso ai sistemi allo scopo di
assicurare disponibilità e affidabilità dei servizi e per consentire di fare
debug e diagnosi dei problemi che potrebbero verificarsi. Queste registrazioni
includono dettagli delle email inviate o ricevute attraverso l'infrastruttura
di Debian, le richieste di accesso a pagine web inviate all'infrastruttura di
Debian e informazioni di login ai sistemi di Debian (come accessi SSH alle
macchine del progetto). Nessuna di queste informazioni viene usata per scopi
diversi dalle esigenze operative e i dati sono conservati per 15 giorni nel
caso dei registri del server web, 10 giorni nel caso dei registri delle email e
4 settimane nel caso dei registri di autenticazione o ssh.
</p>
