#use wml::debian::template title="Informazioni sul rilascio di Debian &ldquo;buster&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7730de84fec07cc853e4d91fab127a0371a9067b" maintainer="Luca Monducci"


<p>Debian <current_release_buster> è stata rilasciata il
<a href="$(HOME)/News/<current_release_newsurl_buster/>">
<current_release_date_buster></a>.
<ifneq "10.0" "<current_release>"
	"Il rilascio iniziale di Debian 10.0 fu fatto il <:=spokendate('2019-07-06'):>."
/>
Questo rilascio contiene importanti cambiamenti descritti
nel <a href="$(HOME)/News/2019/20190706">comunicato stampa</a> e
nelle <a href="releasenotes">Note di rilascio</a>.</p>

<p><strong>Debian 10 è stata sostituita da
<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
Gli aggiornamenti per la sicurezza sono stati interrotti
dal <:=spokendate('2022-06-30'):>.
</strong></p>

<p><strong>Tuttavia, buster beneficia del Supporto a Lungo Termine (LTS
Long Term Support) fino al 30 giugno 2024. Tale supporto è limitato alle
architetture i386, amd64, armhf e arm64; tutte le altre architetture non
hanno supporto.
Per ulteriori informazioni fare riferimento alla <a
href="https://wiki.debian.org/LTS">sezione LTS del Wiki Debian</a>.
</strong></p>

<p>Per aggiornare da un precedente rilascio di Debian, consultare le
<a href="releasenotes">Note di rilascio</a>.</p>

<p>Architetture gestite durante il Long Term Support:</p>
<ul>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/arm64/">64-bit ARM (AArch64)</a>
<li><a href="../../ports/armhf/">Hard Float ABI ARM (armhf)</a>
</ul>

<p>Nonstante la nostra volontà, questo rilascio potrebbe avere problemi,
anche se è dichiarato <em>stable</em>. Esiste un <a href="errata">elenco
dei principali problemi conosciuti</a>, ed è possibile <a
href="../reportingbugs">segnalare altri problemi</a>.</p>
