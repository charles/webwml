#use wml::debian::template title="Informazioni sul rilascio Debian &ldquo;etch&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="28d2aaba8d42e9ca9808154a8580598385f19912" maintainer="Luca Monducci"

<p>Debian <current_release_etch> è stata rilasciata <a
href="$(HOME)/News/<current_release_newsurl_etch/>"><current_release_date_etch></a>.
Debian 4.0 è stata inizialmente rilasciata il <:=spokendate('2007-04-08'):>."
Il rilascio includeva molte modifiche significative, descritte nel nostro
<a href="$(HOME)/News/2007/20070408">comunicato stampa</a> e nelle <a
href="releasenotes">Note di Rilascio</a>.</p>

<p><strong>Debian 4.0 è stata sostituita da
<a href="../buster/">Debian 5.0 (<q>lenny</q>)</a>.
Gli aggiornamenti di sicurezza sono stati interrotti dal
<:=spokendate('2010-02-28'):>.</strong></p>

<p>Per ottenere e installare Debian, consulta la pagina delle informazioni
sull'installazione e la Guida all'installazione. Per effettuare 
l'aggiornamento da una versione precedente di Debian, segui le istruzioni
nelle <a href="releasenotes">Note di Rilascio</a>.</p>

<p>Architetture supportate al rilascio iniziale di etch:</p>
<ul>
<li><a href="../../ports/alpha/">Alpha</a>
<li><a href="../../ports/amd64/">64-bit PC (amd64)</a>
<li><a href="../../ports/arm/">ARM</a>
<li><a href="../../ports/hppa/">HP PA-RISC</a>
<li><a href="../../ports/i386/">32-bit PC (i386)</a>
<li><a href="../../ports/ia64/">Intel Itanium IA-64</a>
<li><a href="../../ports/mips/">MIPS (big endian)</a>
<li><a href="../../ports/mipsel/">MIPS (little endian)</a>
<li><a href="../../ports/powerpc/">PowerPC</a>
<li><a href="../../ports/s390/">IBM S/390</a>
<li><a href="../../ports/sparc/">SPARC</a>
</ul>

<p>Contrariamente a quanto ci auguriamo, è possibile che esistano alcuni
problemi, nonostante sia dichiarata <em>stabile</em>. Abbiamo compilato <a
href="errata">una lista dei principali problemi noti</a>, e potete sempre
segnalarci altri problemi.</p>
