#use wml::debian::template title="Contribuire: come posso aiutare Debian?" MAINPAGE="true"
#use wml::debian::translation-check translation="3a5c68f115956c48aa60c6aa793eb4d802665b23" maintainer="Giuseppe Sacco"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#coding">Programmare e mantenere pacchetti</a></li>
    <li><a href="#testing">Controllare e ricercare bug</a></li>
    <li><a href="#documenting">Scrivere documentazione e etichettare pacchetti</a></li>
    <li><a href="#translating">Tradurre e localizzare</a></li>
    <li><a href="#usersupport">Aiutare altri utenti</a></li>
    <li><a href="#events">Organizzare eventi</a></li>
    <li><a href="#donations">Donare denaro, hardware o banda di rete</a></li>
    <li><a href="#usedebian">Usare Debian</a></li>
    <li><a href="#organizations">Come la tua organizzazione può aiutare Debian</a></li>
  </ul>
</div>

<p>Debian non è semplicemente un sistema operativo, è una comunità. Molte
persone con diverse capacità contribuiscono al progetto: il nostro
software, le immagini, il wiki e altra documentazione sono il risultato
di uno sforzo congiunto di un grande gruppo di individui. Non tutti
sono sviluppatori e non è richiesto di saper programmare per partecipare.
Ci sono molti e diversi modi perché si possa aiutare a rendere Debian
ancora migliore. Se la cosa interessa qui ci sono alcuni suggerimenti
per utenti esperti e alle prime armi.</p>

<h2><a id="coding">Programmare e mantenere pacchetti</a></h2>

<aside class="light">
  <span class="fa fa-code-branch fa-5x"></span>
</aside>

<p>Forse si vuole scrivere un'applicazione da zero, o magari si vuole
aggiungere una nuova funzionalità ad un programma esistente. Se si è
uno sviluppatore e si vuole contribuire a Debian, si può anche aiutare
a prepare il software perché sia semplice installarlo; chiamiamo
questa operazione «impacchettare». La lista che segue presenta alcune
idee per iniziare:</p>

<ul>
  <li>Impacchettare applicazioni, per esempio quelle che si conoscono bene o che si considera importanti per Debian. Per maggiori informazioni su come diventare un manutentore di pacchetti, visitare l'<a href="$(HOME)/devel/">angolo dello sviluppatore Debian</a>.</li>
  <li>Aiutare a mantenere applicazioni esistenti, per esempio realizzando correzioni (patch) o informazioni addizionali nel <a href="https://bugs.debian.org/">sistema di tracciamento dei bug</a>. In alternativa, ci si può unire ad un gruppo di manutenzione o ad un progetto software su <a href="https://salsa.debian.org/">Salsa</a> (la nostra istanza GitLab).</li>
  <li>Assistere <a href="https://security-tracker.debian.org/tracker/data/report">tracciando</a> e <a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">risolvendo</a> <a href="$(HOME)/security/">problemi di sicurezza</a> in Debian.</li>
  <li>Si può anche aiutare irrobustendo <a href="https://wiki.debian.org/Hardening">pacchetti</a>, <a href="https://wiki.debian.org/Hardening/RepoAndImages">repository e immagini</a> e <a href="https://wiki.debian.org/Hardening/Goals">altri componenti</a>.</li>
  <li>C'è l'interesse a <a href="$(HOME)/ports/">portare</a> Debian su architetture alle quali si è familiari? Si può iniziare un nuovo «port» o contribuire ad uno esistente.</li>
  <li>Aiutare a migliorare <a href="https://wiki.debian.org/Services">servizi</a> inerenti Debian o crearne e mantenerne di nuovi tra quelli <a href="https://wiki.debian.org/Services#wishlist">suggeriti o richiesti</a> dalla comunità.</li>
</ul>

<h2><a id="testing">Controllare e ricercare bug</a></h2>

<aside class="light">
  <span class="fa fa-bug fa-5x"></span>
</aside>

<p>Come ogni altro progetto legato al software, Debian ha bisogno di utenti che
verifichino il sistema operativo e le sue applicazioni. Un modo per contribuire
è di installare l'ultima versione e far sapere agli sviluppatori se qualcosa
non funziona come dovrebbe. Ci servono anche persone per verificare il nostro
supporto d'installazione, il «secure boot» e il caricatore U-Boot su hardware
diversi.
</p>

<ul>
  <li>Si può usare il nostro <a href="https://bugs.debian.org/">sistema di tracciamento dei bug</a> per segnalare qualsiasi problema si trovi in Debian. Prima di farlo, verificare che il probema non sia già stato segnalato.</li>
  <li>Sfogliare l'elenco dei bug cercando quelli sui pacchetti che si usano per cercare di fornire informazioni aggiuntive e riprodurre il problema segnalato.</li>
  <li>Verificare la <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">procedura di installazione Debian e le immagini ISO «live»</a>, il <a href="https://wiki.debian.org/SecureBoot/Testing">supporto al «secure boot»</a> e al caricatore <a href="https://wiki.debian.org/U-boot/Status">U-Boot</a>.</li>
</ul>


<h2><a id="documenting">Scrivere documentazione e etichettare pacchetti</a></h2>

<aside class="light">
  <span class="fa fa-file-alt fa-5x"></span>
</aside>

<p>Se si trova un qualsiasi problema in Debian e non si è in grado di scrivere
codice per risolverlo, c'è sempre la possibilità di prendere appunti e
descrivere la propria soluzione. In questo modo si possono aiutare altri utenti
che potrebbero avere problemi simili. Tutta la documentazione Debian è scritta
da membri della comunità e ci sono molti metodi per contribuire.</p>

<ul>
  <li>Unirsi al <a href="$(HOME)/doc/ddp">progetto di documentazione Debian</a> per aiutare con la documentazione Debian.</li>
  <li>Contribuire al <a href="https://wiki.debian.org/">Wiki Debian</a>.</li>
  <li>Assegnare delle etichette e categorie ai pacchetti sul sito web <a href="https://debtags.debian.org/">Debtags</a> in modo che gli utenti Debian possano trovare facilmente il software che stanno cercando.</li>
</ul>

<h2><a id="translating">Tradurre e localizzare</a></h2>

<aside class="light">
  <span class="fa fa-language fa-5x"></span>
</aside>

<p>
L'inglese non è la propria madrelingua, ma lo si padroneggia a sufficienza per capirlo
e tradurre software o altre informazioni relative a Debian come le pagine web, la
documentazione, eccetera? Perché non raggiungere il gruppo delle traduzioni e convertire
applicazioni Debian alla propria madrelingua? Siamo anche alla ricerca di persone che
verifichino le traduzioni attuali e segnalino bug se necessario.
</p>

<ul>
  <li>Tutto ciò che riguarda l'internazionalizzazione di Debian è discusso nella <a href="https://lists.debian.org/debian-i18n/">lista di messaggi i18n</a>.</li>
  <li>Si è madrelingua per una lingua non ancora supportata in Debian? Si può entrare in contatto tramite la pagina <a href="$(HOME)/international/">Debian internazionale</a>.</li>
</ul>

<h2><a id="usersupport">Aiutare altri utenti</a></h2>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>

<p>Si può anche contribuire al progetto aiutando altri utenti Debian. Il progetto
usa vari canali per il supporto, per esempio le liste di messaggi in diverse lingue,
i canali IRC. Per maggiori informazioni visitare le nostre
<a href="$(HOME)/support">pagine del supporto</a>.</p>

# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<ul>
  <li>Il progetto Debian utilizza molte diverse <a href="https://lists.debian.org/">liste di messaggi</a>; alcune sono per gli sviluppatori e altre per gli utenti. Gli utenti con esperienza possono aiutare gli altri nelle <a href="$(HOME)/support#mail_lists">liste di messaggi</a>.</li>
  <li>Persone da tutto il mondo chiacchierano in tempo reale su IRC (Internet Relay Chat). Visitare il canale <tt>#debian</tt> su <a href="https://www.oftc.net/">OFTC</a> per chiacchierare con altri utenti Debian.</li>
  <li>Qui i dettagli della <a href="https://lists.debian.org/debian-italian/">lista degli utenti in italiano</a>. Il canale IRC per gli utenti in italiano è <tt>#debian-it</tt> su <a href="https://www.oftc.net/">OFTC</a>.</li>
</ul>


<h2><a id="events">Organizzare eventi</a></h2>

<aside class="light">
  <span class="fas fa-calendar-check fa-5x"></span>
</aside>

<p>Oltre alla annuale conferenza Debian (DebConf) ci sono svariati piccoli
incontri e altre occasioni in molte nazioni ogni anno. Partecipare o aiutare
a organizzare un <a href="$(HOME)/events/">evento</a> è una grande opportunità
per incontrare utenti e sviluppatori Debian.</p>

<ul>
  <li>Aiutare durante la <a href="https://debconf.org/">conferenza Debian</a> annuale, ad esempio registrando <a href="https://video.debconf.org/">video</a> di interventi e presentazioni, ricevendo gli invitati e supportando i relatori, organizzando eventi speciali durante DebConf (come la festa del formaggio e del vino), aiutando con la preparazione iniziale e finale, eccetera.</li>
  <li>Inoltre ci sono parecchi eventi <a href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a>, incontri locali organizzati dai membri del progetto Debian.</li>
  <li>Si può creare o partecipare ad un <a href="https://wiki.debian.org/LocalGroups">grupppo locale Debian</a> con incontri periodici o altre attività.</li>
  <li>Inoltre, controllare gli altri eventi quali <a href="https://wiki.debian.org/DebianDay">feste del Debian Day</a>, <a href="https://wiki.debian.org/ReleaseParty">feste dei rilasci</a>, <a href="https://wiki.debian.org/BSP">feste alla ricerca di bug</a>, <a href="https://wiki.debian.org/Sprints">«sprint» per lo sviluppo</a> o <a href="https://wiki.debian.org/DebianEvents">altri eventi</a> in tutto il mondo.</li>
</ul>


<h2><a id="donations">Donare denaro, hardware o banda di rete</a></h2>

<aside class="light">
  <span class="fas fa-gift fa-5x"></span>
</aside>

<p>Tutte le donazioni al progetto Debian sono gestite dal nostro Debian Project
Leader (DPL). Con questo tipo di supporto possiamo comprare hardware, domini,
certificati crittografici, eccetera. Usiamo i fondi anche per sponsorizzare gli
eventi DebConf e MiniDebConf, «sprint» per lo sviluppo, la presenza ad altri eventi
e altre cose.</p>

<ul>
  <li>Puoi <a href="$(HOME)/donations">donare</a> denaro, attrezzature o servizi al progetto Debian.</li>
  <li>Noi cerchiamo costantemente <a href="$(HOME)/mirror/">mirror in tutto il mondo</a>.</li>
  <li>Per i nostri «port» Debian, facciamo affidamento sulla <a href="$(HOME)/devel/buildd/">rete degli «autobuilder»</a>.</li>
</ul>



<h2><a id="usedebian">Usare Debian</a></h2>

<aside class="light">
  <span class="fas fa-bullhorn fa-5x"></span>
</aside>

<p>Diffondere la voce e parlare ad altri di Debian e della comunità Debian. Raccomandare
il sistema operativo ad altri utenti e mostrare come si installa. Semplicemente usarlo
con piacere -- questo è probabilmente il metodo più semplice per restituire al progetto
Debian.</p>

<ul>
  <li>Promuovere Debian tenendo interventi e mostrandolo ad altri utenti.</li>
  <li>Contribuire al nostro <a href="https://www.debian.org/devel/website/">sito web</a> e aiutare a migliorare la facciata pubblica di Debian.</li>
  <li>Fare delle <a href="https://wiki.debian.org/ScreenShots">istantanee del proprio schermo</a> e <a href="https://screenshots.debian.net/upload">pubblicarle</a> su <a href="https://screenshots.debian.net/">screenshots.debian.net</a> perché i nostri utenti vedano l'aspetto del software di Debian prima di utilizzarlo.</li>
  <li>Si può partecipare al <a href="https://packages.debian.org/popularity-contest">popularity-contest</a> così da sapere quali pacchetti sono popolari e maggiormente utili.</li>
</ul>


<h2><a id="organizations">Come la tua organizzazione può aiutare Debian</a></h2>

<aside class="light">
  <span class="fa fa-check-circle fa-5x"></span>
</aside>

<p>
Che si lavori in una organizzazione nel settore dell'educazione, del commercio,
no profit o statale, ci sono molti modi per supportarci con varie risorse.
</p>

<ul>
  <li>Ad esempio, l'organizzazione può semplicemente <a href="$(HOME)/donations">donare</a> denaro o hardware.</li>
  <li>O forse potrebbe interessare la <a href="https://www.debconf.org/sponsors/">sponsorizzazione</a> delle nostre conferenze.</li>
  <li>La propria organizzazione potrebbe fornire <a href="https://wiki.debian.org/MemberBenefits">prodotti e servizi a chi contribuisce a Debian</a>.</li>
  <li>Stiamo anche cercando <a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">«hosting» gratuito</a>.</li>
  <li>Naturalmente è ben accetta la gestione di un mirror del nostro <a href="https://www.debian.org/mirror/ftpmirror">software</a>, dei <a href="https://www.debian.org/CD/mirroring/">supporti di installazione</a>, o dei <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">video delle conferenze</a>.</li>
  <li>Si potrebbe considerare la vendita di <a href="https://www.debian.org/events/merchandise">prodotti Debian</a>, <a href="https://www.debian.org/CD/vendors/">supporti di installazione</a> o <a href="https://www.debian.org/distrib/pre-installed">sistemi preinstallati</a>.</li>
  <li>Se l'organizzazione offre <a href="https://www.debian.org/consultants/">consulenza</a> su Debian o <a href="https://wiki.debian.org/DebianHosting">hosting</a>, lo si comunichi.</li>
</ul>

<p>
Siamo sempre interessati a nuovi <a href="https://www.debian.org/partners/">accordi</a>. Se si
può promuovere Debian <a href="https://www.debian.org/users/">fornendo una testimonianza</a>,
usando Debian sui propri server o desktop o semplicemente incoraggiando gli impiegati a
partecipare al nostro progetto, sarebbe fantastico. Si consideri anche l'insegnamento del
sistema operativo Debian o la diffusione della comunità, indirizzando il proprio team a
contribuire durante le ore di lavoro o inviando qualcuno a uno dei nostri <a
href="$(HOME)/events/">eventi</a>.</p>
