#use wml::debian::template title="Зеркала Debian (по всему миру)" MAINPAGE="true"
#use wml::debian::translation-check translation="34422e94132789c2bcd0f6d41dc8680b49c7f3ab" maintainer="Lev Lamberov"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Зеркала Debian по странам</a></li>
  <li><a href="#complete-list">Полный список зеркал</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian распространяется (<a href="https://www.debian.org/mirror/">зеркалированием</a>) сотнями серверов. Если вы планируете <a href="../download">загрузить</a> Debian, сначала попробуйте ближайший к вам сервер. Вероятно, так вы ускорите скачивание и снизите нагрузку на наши центральные серверы.</p>
</aside>

<p class="centerblock">
  Зеркала Debian имеются во многих странах, и для некоторых стран у нас
  имеются псевдонимы вида <code>ftp.&lt;страна&gt;.debian.org</code>. Эти
  псевдонимы обычно указывают на зеркало, которое регулярно сихнронизируется, является быстрым
  и содержит все архитектуры Debian. Архив Debian
  всегда доступен по протоколу <code>HTTP</code> в каталоге <code>/debian</code>
  на сервере.
</p>

<p class="centerblock">
  Другие зеркала могут иметь ограничения на то, что
  они зеркалируют (из-за ограничения свободного места). То, что какой-то сайт
  не имеет псевдоним вида <code>ftp.&lt;страна&gt;.debian.org</code>, не обязательно означает, что
  он медленнее или содержит менее актуальные пакеты, чем зекрало
  <code>ftp.&lt;страна&gt;.debian.org</code>.
  В действительности, зеркало, содержащее вашу архитектуру и находящееся к вам ближе всего как
  к пользователю, а потому более быстрое, почти всегда является предпочтительным по отношению
  к другим зеркалам, находящимся дальше.
</p>

<p>Используйте ближайшее к вам зеркало для более быстрой загрузки, независимо
от того, имеется у него псевдоним страны или нет.
Для определения задержки доступа к сайту можно использовать программу
<a href="https://packages.debian.org/stable/net/netselect-apt">\
<code>netselect-apt</code></a>; для определения скорости скачивания можно
использовать программу
<a href="https://packages.debian.org/stable/web/wget">\
<code>wget</code></a> или
<a href="https://packages.debian.org/stable/net/rsync">\
<code>rsync</code></a>.
Заметим, что географическая близость часто не является наиболее важным фактором при
выборе зеркала.</p>

<p>
Если ваша система часто меняет своё местоположение, то вам лучше всего использовать <q>зеркало</q>,
определяемое с помощью глобальной системы <abbr title="Content Delivery Network">CDN</abbr>.
Для этой цели у Проекта Debian
имеется специальная служба, <code>deb.debian.org</code>,
вы можете использовать его в вашем файле <code>sources.list</code>, подробную информацию
можно найти на <a href="http://deb.debian.org/">веб-сайте этой службы</a>.

<h2 id="per-country">Зеркала Debian по странам</h2>

<table border="0" class="center">
<tr>
  <th>Страна</th>
  <th>Сайт</th>
  <th>Архитектуры</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 id="complete-list">Полный список зеркал</h2>

<table border="0" class="center">
<tr>
  <th>Имя узла</th>
  <th>HTTP</th>
  <th>Архитектуры</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
