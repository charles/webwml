#use wml::debian::links.tags
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/index.def"
#use wml::debian::mainpage title="<motto>"

#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

#use wml::debian::translation-check translation="ccdb794b3ff8618ce3bde5f028d73ec0859cc827" maintainer="Lev Lamberov"

<div id="splash">
  <h1>Debian</h1>
</div>

<!-- The first row of columns on the site. -->
<div class="row">
  <div class="column column-left">
    <div style="text-align: center">
      <h1>Сообщество</h1>
      <h2>Debian &mdash; это сообщество людей!</h2>

#include "$(ENGLISHDIR)/index.inc"

      <div class="row">
      <div class="community column">
        <a href="intro/people" aria-hidden="true">
          <img src="Pics/users.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/people">Люди</a></h2>
        <p>Кто мы и что мы делаем</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/philosophy" aria-hidden="true">
          <img src="Pics/heartbeat.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/philosophy">Наша философия</a></h2>
        <p>Почему мы это делаем и как мы это делаем</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="devel/join/" aria-hidden="true">
          <img src="Pics/user-plus.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="devel/join/">Принять участие, внести свою лепту</a></h2>
        <p>Как вы можете присоединиться к нам!</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#community" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#community">Больше...</a></h2>
        <p>Дополнительная информация о сообществе Debian</p>
      </div>
    </div>
  </div>
  <div class="column column-right">
    <div style="text-align: center">
      <h1>Операционная система</h1>
      <h2>Debian &mdash; это полноценная свободная операционная система!</h2>
      <div class="os-img-container">
        <img src="Pics/debian-logo-1024x576.png" alt="Debian">
        <a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso" class="os-dl-btn"><download></a>
        <a href="distrib" class="os-other-download">Другие загрузки</a>
</div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/why_debian" aria-hidden="true">
          <img src="Pics/trophy.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/why_debian">Почему Debian</a></h2>
        <p>Что делает Debian таким особенным</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="support" aria-hidden="true">
          <img src="Pics/life-ring.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="support">Пользовательская поддержка</a></h2>
        <p>Получить помощь и документацию</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="security/" aria-hidden="true">
          <img src="Pics/security.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="security/">Обновления безопасности</a></h2>
        <p>Анонсы безопасности Debian (DSA)</p>
      </div>
    </div>
    <div class="row">
      <div class="community column">
        <a href="intro/index#software" aria-hidden="true">
          <img src="Pics/list.svg" width="512">
        </a>
      </div>
      <div class="styled-href-blue column-4-parts">
        <h2><a href="intro/index#software">Больше...</a></h2>
        <p>Дополнительные ссылки на загрузку и ПО</p>
      </div>
    </div>
  </div>
</div>

<hr>
<!-- An optional row highlighting events happening now, such as releases, point releases, debconf and minidebconfs, and elections (dpl, GRs...). -->
<!-- <div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h2>Продолжается <a href="https://debconf22.debconf.org/">DebConf22</a>!</h2>
      <p>Конференция Debian проводится в Призрене, Косово с 17 по 24 июля 2022 года.</p>
    </div>
  </div>
</div>-->


<!-- The next row of columns on the site. -->
<!-- The News will be selected by the press team. -->


<div class="row">
  <div class="column styled-href-blue column-left">
    <div style="text-align: center">
      <h1><projectnews></h1>
      <h2>Новости и анонсы о Debian</h2>
    </div>

    <:= get_top_news() :>

    <!-- No more News entries behind this line! -->
    <div class="project-news">
      <div class="end-of-list-arrow"></div>
      <div class="project-news-content project-news-content-end">
        <a href="News">Все новости</a> &emsp;&emsp;
	<a class="rss_logo" style="float: none" href="News/news">RSS</a>
      </div>
    </div>
  </div>
</div>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Новости Debian" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Новости проекта Debian" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Рекомендации по безопасности Debian (только заголовки)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Рекомендации по безопасности Debian (обзоры)" href="security/dsa-long">
:#rss#}
