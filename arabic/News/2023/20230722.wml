#use wml::debian::translation-check translation="1f51232f1964140bbf71ecfd1fc7bdcbd252e744"
<define-tag pagetitle>تحديث دبيان 12: الإصدار 12.1</define-tag>
<define-tag release_date>2023-07-22</define-tag>

#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
يسعد مشروع دبيان الإعلان عن التحديث الأول لتوزيعته المستقرة دبيان <release> (الاسم الرمزي <q><codename></q>).
بالإضافة إلى تسوية بعض المشكلات الحرجة يصلح هذا التحديث بالأساس مشاكلات الأمان. تنبيهات الأمان أعلنت بشكل منفصل ومشار إليها فقط في هذا الإعلان.
</p>

<p>
يرجى ملاحظة أن هذا التحديث لا يشكّل إصدار جديد لدبيان <release> بل فقط تحديثات لبعض الحزم المضمّنة
وبالتالي ليس بالضرورة رمي الوسائط القديمة للإصدار <q><codename></q>، يمكن تحديث الحزم باستخدام مرآة دبيان محدّثة.
</p>

<p>
الذين يثبّتون التحديثات من security.debian.org باستمرار لن يكون عليهم تحديث العديد من الحزم،
أغلب التحديثات مضمّنة في هذا التحديث.
</p>

<p>
صور جديدة لأقراص التثبيت ستكون متوفرة في موضعها المعتاد.
</p>

<p>
يمكن الترقية من  تثبيت آنيّ إلى هذه المراجعة بتوجيه نظام إدارة الحزم إلى إحدى مرايا HTTP الخاصة بدبيان.
قائمة شاملة لمرايا دبيان على المسار:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>إصلاح العديد من العلاّت</h2>

<p>أضاف هذا التحديث للإصدار المستقر بعض الإصلاحات المهمة للحزم التالية:</p>

<table border=0>
<tr><th>الحزمة</th>               <th>السبب</th></tr>
<correction aide "Properly handle creating the system user; fix child directory processing on equal match">
<correction autofs "Fix hang when using Kerberos-authenticated LDAP">
<correction ayatana-indicator-datetime "Fix playing of custom alarm sounds">
<correction base-files "Update for the 12.1 point release">
<correction bepasty "Fix rendering of text uploads">
<correction boost1.81 "Add missing dependency on libboost-json1.81.0 to libboost-json1.81-dev">
<correction bup "Correctly restore POSIX ACLs">
<correction context "Enable socket in ConTeXt mtxrun">
<correction cpdb-libs "Fix a buffer overflow vulnerability [CVE-2023-34095]">
<correction cpp-httplib "Fix CRLF injection issue [CVE-2023-26130]">
<correction crowdsec "Fix default acquis.yaml to also include the journalctl datasource, limited to the ssh.service unit, making sure acquisition works even without the traditional auth.log file; make sure an invalid datasource doesn't make the engine error out">
<correction cups "Security fixes: use-after-free [CVE-2023-34241]; heap buffer overflow [CVE-2023-32324]">
<correction cvs "Configure full path to ssh">
<correction dbus "New upstream stable release; fix denial of service issue [CVE-2023-34969]; stop trying to take DPKG_ROOT into account, restoring copying of systemd's /etc/machine-id in preference to creating an entirely new machine ID">
<correction debian-installer "Increase Linux kernel ABI to 6.1.0-10; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction desktop-base "Remove emerald alternatives on package uninstallation">
<correction dh-python "Re-introduce Breaks+Replaces on python2 needed to help apt in some upgrade scenarios">
<correction dkms "Add Breaks against obsolete, incompatible *-dkms packages">
<correction dnf "Fix default DNF const PYTHON_INSTALL_DIR">
<correction dpdk "New upstream stable release">
<correction exim4 "Fix argument parsing for ${run } expansion; fix ${srs_encode ..} returning incorrect result every 1024 days">
<correction fai "Fix IP address lifetime">
<correction glibc "Fix a buffer overflow in gmon; fix a deadlock in getaddrinfo (__check_pf) with deferred cancellation; fix y2038 support in strftime on 32-bit architectures; fix corner case parsing of /etc/gshadow which can return bad pointers, causing segfaults in applications; fix a deadlock in system() when called concurrently from multiple threads; cdefs: limit definition of fortification macros to __FORTIFY_LEVEL &gt; 0 to support old C90 compilers">
<correction gnome-control-center "New upstream bugfix release">
<correction gnome-maps "New upstream bugfix release">
<correction gnome-shell "New upstream bugfix release">
<correction gnome-software "New upstream release; memory leak fixes">
<correction gosa "Silence PHP 8.2 deprecation warnings; fix missing template in default theme; fix table styling; fix use of debugLevel &gt; 0">
<correction groonga "Fix documentation links">
<correction guestfs-tools "Security update [CVE-2022-2211]">
<correction indent "Restore the ROUND_UP macro and adjust the initial buffer size">
<correction installation-guide "Enable Indonesian translation">
<correction kanboard "Fix malicious injection of HTML tags into DOM [CVE-2023-32685]; fix parameter-based indirect object referencing leading to private file exposure [CVE-2023-33956]; fix missing access controls [CVE-2023-33968, CVE-2023-33970]; fix stored XSS in Task External Link functionality [CVE-2023-33969]">
<correction kf5-messagelib "Search also for subkeys">
<correction libmatekbd "Fix memory leaks">
<correction libnginx-mod-http-modsecurity "Binary rebuild with pcre2">
<correction libreoffice "New upstream bugfix release">
<correction libreswan "Fix potential denial-of-service issue [CVE-2023-30570]">
<correction libxml2 "Fix NULL pointer dereference issue [CVE-2022-2309]">
<correction linux "New upstream stable release; netfilter: nf_tables: do not ignore genmask when looking up chain by id [CVE-2023-31248], prevent OOB access in nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-amd64 "New upstream stable release; netfilter: nf_tables: do not ignore genmask when looking up chain by id [CVE-2023-31248], prevent OOB access in nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-arm64 "New upstream stable release; netfilter: nf_tables: do not ignore genmask when looking up chain by id [CVE-2023-31248], prevent OOB access in nft_byteorder_eval [CVE-2023-35001]">
<correction linux-signed-i386 "New upstream stable release; netfilter: nf_tables: do not ignore genmask when looking up chain by id [CVE-2023-31248], prevent OOB access in nft_byteorder_eval [CVE-2023-35001]">
<correction mailman3 "Drop redundant cron job; handle ordering of services when MariaDB is present">
<correction marco "Show correct window title when owned by superuser">
<correction mate-control-center "Fix several memory leaks">
<correction mate-power-manager "Fix several memory leaks">
<correction mate-session-manager "Fix several memory leaks; allow clutter backends other than x11">
<correction multipath-tools "Hide underlying paths from LVM; prevent initial service failure on new installations">
<correction mutter "New upstream bugfix release">
<correction network-manager-strongswan "Build editor component with GTK 4 support">
<correction nfdump "Return success when starting; fix segfault in option parsing">
<correction nftables "Fix regression in set listing format">
<correction node-openpgp-seek-bzip "Correct installation of files in seek-bzip package">
<correction node-tough-cookie "Fix prototype pollution issue [CVE-2023-26136]">
<correction node-undici "Security fixes: protect <q>Host</q> HTTP header from CLRF injection [CVE-2023-23936]; potential ReDoS on Headers.set and Headers.append [CVE-2023-24807]">
<correction node-webpack "Security fix (cross-realm objects) [CVE-2023-28154]">
<correction nvidia-cuda-toolkit "Update bundled openjdk-8-jre">
<correction nvidia-graphics-drivers "New upstream stable release; security fixes [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla "New upstream stable release; security fixes [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla-470 "New upstream stable release; security fixes [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-modprobe "New upstream bugfix release">
<correction nvidia-open-gpu-kernel-modules "New upstream stable release; security fixes [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-support "Add Breaks against incompatible packages from bullseye">
<correction onionshare "Fix installation of desktop furniture">
<correction openvpn "Fix memory leak and dangling pointer (possible crash vector)">
<correction pacemaker "Fix regression in the resource scheduler">
<correction postfix "New upstream bugfix release; fix <q>postfix set-permissions</q>">
<correction proftpd-dfsg "Do not enable inetd-style socket at installation">
<correction qemu "New upstream stable release; fix USB devices not being available to XEN HVM domUs; 9pfs: prevent opening special files [CVE-2023-2861]; fix reentrancy issues in the LSI controller [CVE-2023-0330]">
<correction request-tracker5 "Fix links to documentation">
<correction rime-cantonese "Sort words and characters by frequency">
<correction rime-luna-pinyin "Install missing pinyin schema data">
<correction samba "New upstream stable release; ensure manpages are generated during build; enable ability to store kerberos tickets in kernel keyring; fix build issues on armel and mipsel; fix windows logon/trust issues with 2023-07 windows updates">
<correction schleuder-cli "Security fix (value escaping)">
<correction smarty4 "Fix arbitrary code execution issue [CVE-2023-28447]">
<correction spip "Various security issues; security fix (authentication data filtering)">
<correction sra-sdk "Fix installation of files in libngs-java">
<correction sudo "Fix event log format">
<correction systemd "New upstream bugfix release">
<correction tang "Fix race condition when creating/rotating keys [CVE-2023-1672]">
<correction texlive-bin "Disable socket in luatex by default [CVE-2023-32668]; make installable on i386">
<correction unixodbc "Add Breaks+Replaces against odbcinst1debian1">
<correction usb.ids "Update included data">
<correction vm "Disable byte compilation">
<correction vte2.91 "New upstream bugfix release">
<correction xerial-sqlite-jdbc "Use a UUID for connection ID [CVE-2023-32697]">
<correction yajl "Memory leak security fix; fix denial of service issue [CVE-2017-16516], integer overflow issue [CVE-2022-24795]">
</table>


<h2>تحديثات الأمان</h2>


<p>
أضافت هذه المراجعة تحديثات الأمان التالية للإصدار المستقر.
سبق لفريق الأمان نشر تنبيه لكل تحديث:
</p>

<table border=0>
<tr><th>معرَّف التنبيه</th>  <th>الحزمة</th></tr>
<dsa 2023 5423 thunderbird>
<dsa 2023 5425 php8.2>
<dsa 2023 5427 webkit2gtk>
<dsa 2023 5428 chromium>
<dsa 2023 5429 wireshark>
<dsa 2023 5430 openjdk-17>
<dsa 2023 5432 xmltooling>
<dsa 2023 5433 libx11>
<dsa 2023 5434 minidlna>
<dsa 2023 5435 trafficserver>
<dsa 2023 5436 hsqldb1.8.0>
<dsa 2023 5437 hsqldb>
<dsa 2023 5439 bind9>
<dsa 2023 5440 chromium>
<dsa 2023 5443 gst-plugins-base1.0>
<dsa 2023 5444 gst-plugins-bad1.0>
<dsa 2023 5445 gst-plugins-good1.0>
<dsa 2023 5446 ghostscript>
<dsa 2023 5447 mediawiki>
<dsa 2023 5448 linux-signed-amd64>
<dsa 2023 5448 linux-signed-arm64>
<dsa 2023 5448 linux-signed-i386>
<dsa 2023 5448 linux>
<dsa 2023 5449 webkit2gtk>
<dsa 2023 5450 firefox-esr>
<dsa 2023 5451 thunderbird>
</table>



<h2>مُثبِّت دبيان</h2>
<p>
حدِّث المُثبِّت ليتضمن الإصلاحات المندرجة في هذا الإصدار المستقر.
</p>

<h2>المسارات</h2>

<p>
القائمة الكاملة للحزم المغيّرة في هذه المراجعة:
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>التوزيعة المستقرة الحالية:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>التحديثات المقترحة للتوزيعة المستقرة:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>معلومات حول التوزيعة المستقرة (ملاحظات الإصدار والأخطاء إلخ):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>معلومات وإعلانات الأمان:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>حول دبيان</h2>

<p>
مشروع دبيان هو اتحاد لمطوري البرمجيات الحرة تطوعوا بالوقت والمجهود لإنتاج نظام تشعيل دبيان حر بالكامل.
</p>

<h2>معلومات الاتصال</h2>

<p>
لمزيد من المعلومات يرجى زيارة موقع دبيان
<a href="$(HOME)/">https://www.debian.org/</a>
أو إرسال بريد إلكتروني إلى &lt;press@debian.org&gt;
أو الاتصال بفريق إصدار المستقرة على
&lt;debian-release@lists.debian.org&gt;.
</p>


