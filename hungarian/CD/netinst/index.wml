#use wml::debian::cdimage title="Telepítés minimál CD-ről"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="40eeef5c10b97819abf8467d7f2a8038a8729ff8" maintainer="Szabolcs Siebenhofer"

<p>A <q>hálózati telepítő</q> vagy <q>netinst</q> CD egyetlen CD, amely
lehetővé teszi, hogy telepítsd az egész operációs rendszert. Ez az egyetlen
CD csak azokat a legszükségesebb szoftvereket tartalmazza, melyekkel a telepítés
megkezdhető, a többi programot az Internetről szerzi be.</p>

<p><strong>Mi a jó nekem &mdash; bootolható minimál CD-ROM vagy egy teljes 
CD?</strong> Attól függ, de véleményünk szerint az esetek többségében
a minimál CD kép a jobb &mdash; mindenekelőtt azért, mert csak azokat a 
csomagokat töltöd le, melyek a kijelölt telepítéshez szükségesek,
megspórolva ezzel időt és sávszélességet. Másrészt, a teljes CD sokkal
jobban használható, amennyiben nem egy gépet telepítesz vagy olyat, 
melynek nincsen Internet kapcsolata.</p>

<p><strong>Melyik hálózati kapcsolatot támogatja a telepítő?</strong>
A telepítő feltételezi, hogy van Internet kapcsolatod. Többféle megoldást támogat:
betárcsázós PPP, ethernet kapcsolat vagy WLAN kártya (bizonyos kötöttségekkel), 
de az ISDN nem támogatott &mdash; elnézést!</p>

<p>Az alábbi minimál CD képeket lehet letölteni:</p>

<ul>

  <li>Hivatalos <q>netinst</q> kép a <q>stabil</q> kiadáshoz &mdash; <a
  href="#netinst-stable">lásd lent</a></li>

  <li><q>Testing</q> kiadás képei, napi frissítéssel, ami működő pillanatkép néven ismert, lásd <a
  href="$(DEVEL)/debian-installer/">Debian-Installer oldal</a>.</li>

</ul>


<h2 id="netinst-stable">Hivatalos netinst képek a <q>stabil</q> kiadáshoz</h2>

<p>Legfejebb 300&nbsp;MB méretű, kép a telepítőt és azokat a csomagokat tartalmazza,
melyek egy (nagyon) alap rendszer telepítéséhez szükségesek.</p>

<div class="line">
<div class="item col50">
<p><strong>netinst CD kép (<a href="$(HOME)/CD/torrent-cd">bittorrenten</a>) keresztül</strong></p>
  <stable-netinst-torrent />
</div>
<div class="item col50 lastcol">
<p><strong>netinst CD kép (álatalában 150-300 MB, architektúrától függően)</strong></p>
  <stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>Információkat arról, hogy ezeket a fájlokat hogyan kell használni, a 
<a href="../faq/">GYIK-ban</a> találsz.</p>

<p>Miután letöltötted a képeket, nézd át a <a href="$(HOME)/releases/stable/installmanual">
részletes információkat a telepítési folyamatról</a>.</p>
