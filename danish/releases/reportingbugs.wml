#use wml::debian::template title="Debian-udgaver -- Problemrapportering" BARETITLE=true
#use wml::debian::translation-check translation="13b65d11958cedf114f8fe989b4d997a6eb96881"

# Translators: copy of buster/reportingbug

<h2><a name="report-release">Med udgivelsesbemærkningerne</a></h2>

<p>Fejl i <a href="releasenotes">udgivelsesbemærkningerne</a> bør
<a href="$(HOME)/Bugs/Reporting">rapporteres som fejl</a> mod pseudopakken 
<tt>release-notes</tt>.  Diskussioner om dokumentet koordineres på postlisten
debian-doc på <a href="mailto:debian-doc@lists.debian.org">\
&lt;debian-doc@lists.debian.org&gt;</a>.
</p>

<h2><a name="report-installation">Med installeringen</a></h2>

<p>Hvis du har et problem med installeringssystemet, så rapporter fejl mod 
pakken <tt>installation-reports</tt>.  Udfyld 
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">rapporteringskabelonen</a> 
for at sikre, at alle nødvendige oplysninger medsendes.</p>

<p>Hvis du har forslag eller rettelser til <a href="installmanual">\
Installeringshåndbogen</a>, bør du <a href="$(HOME)/Bugs/Reporting">indsende 
dem</a> mod <tt>installation-guide</tt>, som er kildepakken, hvori 
dokumentationen vedligeholdes.</p>

<p>Hvis du har problemer med installeringssystemet, som ikke egner sig til en
fejlrapport (fx hvis du ikke er sikker på, om det virkelig er en fejl eller ej,
eller præcis hvor i systemet problemet opstår, osv.) bør du nok i stedet sende 
en engelsksproget mail til listen <a href="mailto:debian-boot@lists.debian.org">\
&lt;debian-boot@lists.debian.org&gt;</a>.</p>

<h2><a name="report-upgrade">Med en opgradering</a></h2>

<p>Hvis du har problemer med at opgradere dit system fra tidligere udgaver, så
indsend en fejl mod pakken <tt>upgrade-reports</tt>, som er pseudopakken, der 
anvendes til at holde styre på sådanne problemer.  For flere oplysninger om 
hvordan man indsender opgraderingsrapporter, læs 
<a href="releasenotes">udgivelsesbemærkningerne</a>.</p>

<h2><a name="report-package">Alle andre problemer</a></h2>

<p>Hvis du har problemer med sytemet efter installeringen, bør du prøve at finde
ud af hvilken pakke, der er skyld i det, og <a href="$(HOME)/Bugs/Reporting">\
indsende en fejlrapport</a> mod den pågældende pakke.</p>
