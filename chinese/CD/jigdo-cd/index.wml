#use wml::debian::cdimage title="使用 jigdo 下载 Debian CD 映像" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="b0492ff8b5c976f85fec93aa66027d5218eff054"

<p>Jigsaw Download，简称 <a 
href="https://www.einval.com/~steve/software/jigdo/">jigdo</a>，\
是一种带宽友好的分发 Debian CD/DVD 映像的方式。</p>

<toc-display/>

<toc-add-entry name="why">为什么 jigdo 比直接下载更好</toc-add-entry>

<p>因为它更快！\
由于各种原因，CD/DVD 映像的镜像站个数远少于<q>正常的</q>Debian 镜像的个数。\
结果是，如果您从 CD 映像的镜像站下载，它不但离您更远，而且会不堪重负，\
尤其是新版本刚[CN:发布:][HKTW:释出:]的时候。</p>

<p>更重要的是，有些类型的映像不提供完整的 \
<tt>.iso</tt> 下载，因为我们的服务器没有足够空间容纳它们。</p>

<p>当然，一个<q>正常的</q> Debian 镜像不包含任何 CD/DVD 映像，所以 \
jigdo 是怎么从它那边下载的呢？jigdo 通过单独下载 CD/DVD 包含的每一个文件\
来达到这个目的。下一步，它会把所有下载的[CN:文件:][HKTW:档案:]\
组装成一个大[CN:文件:][HKTW:档案:]，它和原 CD/DVD 映像完全相同。\
然而，这一切都在幕后发生 &mdash; 对 <em>您</em> 而言，只需告诉下载工具\
<q><tt>.jigdo</tt></q>[CN:文件:][HKTW:档案:]的位置。</p>

<p>更多信息请参阅 \
<a href="https://www.einval.com/~steve/software/jigdo/">jigdo 主页</a>。\
我们随时欢迎愿意帮助 jigdo 开发的志愿者！</p>

<toc-add-entry name="how">如何用 jigdo 下载映像</toc-add-entry>

<ul>

  <li>下载含有 <tt>jigdo-lite</tt> 的软件包。可通过 Debian 和 Ubuntu 发行版中的 \
  <tt>jigdo-file</tt> 软件包直接取得并安装它。FreeBSD 可以从 /usr/ports/net-p2p/jigdo \
  安装，或者用 <tt>pkg_add -r jigdo</tt> 安装软件包。若需要其它安装选项（用于 Windows \
  的二进制文件、源代码），请访问 <a 
  href="https://www.einval.com/~steve/software/jigdo/">jigdo 主页</a>。
  </li>

  <li>运行 <tt>jigdo-lite</tt> 脚本。它会询问<q><tt>.jigdo</tt></q>文件的\
  URL。（您也可以在命令行提供\
  URL，如果您乐意的话。）</li>

  <li>从<a href="#which">下方</a>列出的地址中选择您想下载的\
<q><tt>.jigdo</tt></q>[CN:文件:][HKTW:档:]，并
在 <tt>jigdo-lite</tt> 提示符中输入它们的URL。\
每一个<q><tt>.jigdo</tt></q>[CN:文件:][HKTW:档:]\
对应一个<q><tt>.iso</tt></q>CD/DVD 映像。</li>

  <li>如果您是首次使用，在<q>Files to scan</q>提示符出现时，只需按下回车键。</li>

  <li>在提示符<q>Debian mirror</q>中，输入\
  <kbd>http://deb.debian.org/debian/</kbd> 或者\
  <kbd>http://ftp.<strong><var>XY</var></strong>.debian.org/debian/</kbd>，其中\
  <strong><var>XY</var></strong> 是您国家的双字母代码\
（比如 <tt>us</tt>、<tt>de</tt>、<tt>uk</tt>。参见\
<a href="$(HOME)/mirror/list">可用的 \
ftp.<var>XY</var>.debian.org 地址列表</a>。）

  <li>遵照脚本给出的指示。如果一切顺利的话，脚本最后会打印生成的映像的\
校验和，并告诉您该校验和与原映像[CN:文件:][HKTW:档:]一致。</li>

</ul>

<p>该步骤的详细描述，请参看 \
<a href="https://tldp.org/HOWTO/Debian-Jigdo/">Debian jigdo \
迷你 HOWTO 文档</a>。该 HOWTO 文档也解释了 jigdo 的高级特性，\
例如将旧版本的 CD/DVD 映像升级为最新版本\
（只下载有改动的[CN:文件:][HKTW:档案:]，而不是整个\
映像[CN:文件:][HKTW:档:]）。</p>

<p>在您完成映像[CN:文件:][HKTW:档:]的下载并写入 CD/DVD 之后，请务必\
阅读<a
href="$(HOME)/releases/stable/installmanual">关于安装过程的\
详细信息</a>。</p>

<toc-add-entry name="which">官方映像</toc-add-entry>

<h3><q>稳定（stable）</q>版本的官方 jigdo [CN:文件:][HKTW:档案:]</h3>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <stable-full-cd-jigdo />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <stable-full-dvd-jigdo />
</div>
<div class="clear"></div>
</div>
<div class="line">
<div class="item col50">
<p><strong>蓝光</strong></p>
  <stable-full-bluray-jigdo />
</div>
</div>

<p>请您务必在安装前阅读文档。\
<strong>如果您在安装前只想阅读一份文档</strong>，请阅读我们的\
<a href="$(HOME)/releases/stable/amd64/apa">安装指南</a>，这是一份\
安装过程的简要介绍。其他有用的文档包括：
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">安装手册</a>，
详细的安装步骤</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian 安装[CN:程序:][HKTW:程式:]\
文档</a>，包括常见问题及解答（FAQ）</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian 安装\
[CN:程序:][HKTW:程式:]勘误</a>，安装[CN:程序:][HKTW:程式:]的已知问题列表</li>
</ul>

<h3><q>测试（testing）</q>版本的官方 jigdo [CN:文件:][HKTW:档案:]</h3>
<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <devel-full-cd-jigdo />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <devel-full-dvd-jigdo />
</div>
</div>

<hr />

<toc-add-entry name="search">搜索 CD 映像的内容</toc-add-entry>

<p><strong>某个[CN:文件:][HKTW:档案:]位于哪个 CD/DVD 映像？</strong>\
在下方，您可以在大量 Debian CD/DVD 映像所包含的[CN:文件:][HKTW:档案:]\
列表中搜索。您可输入多个关键词，每个关键词必须匹配[CN:文件:][HKTW:档案:]\
名的子串。添加例如<q>_i386</q>的字符串以搜索一个特定架构。\
添加<q>_all</q>以限制搜索范围为所有架构都相同的软件包。</p>

<form method="get" action="https://cdimage-search.debian.org/"><p>
<input type="hidden" name="search_area" value="release">
<input type="hidden" name="type" value="simple">
<input type="text" name="query" size="20" value="">
# Translators: "Search" is translatable
<input type="submit" value="搜索"></p></form>

<p><strong>某个映像包含哪些文件？</strong>如果
您需要特定 Debian CD/DVD 映像包含的 <em>所有</em> [CN:文件:][HKTW:档案:]\
列表，只需在 <a href="https://cdimage.debian.org/cdimage/">cdimage.debian.org</a> \
查看该映像对应的 <tt>list.gz</tt> [CN:文件:][HKTW:档案:]。</p>

<hr>

<toc-add-entry name="faq">常见问题及解答</toc-add-entry>

<p><strong>如何让 jigdo 使用代理？</strong></p>

<p>在文本编译器中打开 <tt>~/.jigdo-lite</tt>（对于 Windows 版本则是\
<tt>jigdo-lite-settings.txt</tt>）\
并找到以<q>wgetOpts</q>开头的行。可以在该行添加以下选项：</p>

<p><tt>-e ftp_proxy=http://<i>代理地址</i>:<i>端口</i>/</tt>
<br><tt>-e http_proxy=http://<i>代理地址</i>:<i>端口</i>/</tt>
<br><tt>--proxy-user=<i>用户名</i></tt>
<br><tt>--proxy-passwd=<i>密码</i></tt></p>

<p>当然，根据您的代理服务器的情况修改各项的值。\
最后两项只在您的代理服务器需要密码验证时才需要填写。\
以上各项设置需要添加到 \
wgetOpts 行末的 <tt>'</tt> 字符 <em>之前</em>。所有选项必须写在同一行。</p>

<p>作为替代方法，在 Linux 中您也可以设置 \
<tt>ftp_proxy</tt> 和 <tt>http_proxy</tt> 环境变量，比如在\
[CN:文件:][HKTW:档案:] <tt>/etc/environment</tt> 或者 \
<tt>~/.bashrc</tt> 中。</p>

<p><strong>啊！脚本出错了 &mdash; 之前的都白下载了吗？！</strong></p>

<p>虽然这不应该发生（真的），但是仍有可能出现这种情况，就是已经生成了一个\
巨大的<q><tt>.iso.tmp</tt></q>[CN:文件:][HKTW:档案:]，\
而 <tt>jigdo-lite</tt> 似乎出现了问题，反复让您重试下载。\
有几种解决方案可以尝试：</p>

<ul>

  <li>直接按回车键重试。可能有些[CN:文件:][HKTW:档案:]因为超时或者\
某些暂时性的错误而无法下载 &mdash; 这将再次尝试下载所有还没下载的\
[CN:文件:][HKTW:档案:]。</li>

  <li>换一个镜像站。有些 Debian 镜像的内容稍有点旧 &mdash; 或许另一个镜像仍\
保留着您指定的镜像已经删除的[CN:文件:][HKTW:档案:]，或者已经更新了您指定的\
镜像中还不存在的文件。</li>

  <li>用 <tt><a href="https://rsync.samba.org/">rsync</a></tt> 下载\
映像中缺失的部分。首先，您需要获得想下载的映像的正确的 rsync URL：\
选择一个对 <a
  href="../mirroring/rsync-mirrors">stable</a> 或者 <a
  href="../http-ftp/#testing">testing</a> 映像提供 rsync 访问的镜像，\
然后确定正确的路径和[CN:文件:][HKTW:档案:]名。目录列表可以用类似 \
<tt>rsync&nbsp;rsync://cdimage.debian.org/debian-cd/</tt>的命令获得。

  <br>下一步，重命名 <tt>jigdo-lite</tt> 的临时文件，删除\
<q><tt>.tmp</tt></q>扩展名，将远程主机的 URL 和本地[CN:文件:][HKTW:档案:]\
名都传给 rsync：\
<tt>rsync&nbsp;rsync://server.org/path/binary-i386-1.iso
  binary-i386-1.iso</tt>

  <br>您可能想要使用 rsync 的 <tt>--verbose</tt> 和 \
<tt>--progress</tt> 选项获得状态信息，以及用\
<tt>--block-size=8192</tt> 提高下载速度。</li>

  <li>如果其他方法都失败了，您下载的内容仍未丢失：在\
Linux 中，您可以挂载 <tt>.tmp</tt> [CN:文件:][HKTW:档案:]以访问\
已经下载的软件包，并重新利用它们\
以使用一个新的 jigdo [CN:文件:][HKTW:档案:]生成映像（比如一个\
新的 testing 快照，如果您下载失败的映像也是一个 testing 快照的话）。\
要如此做，首先以 root 权限在下载失败的目录下执行以下命令：\
<tt>mkdir&nbsp;mnt;
  mount&nbsp;-t&nbsp;iso9660&nbsp;-o&nbsp;loop&nbsp;*.tmp&nbsp;mnt</tt>。
然后，在另外一个目录开始一个新的下载，并在<q>Files to scan</q>提示符\
输入 <tt>mnt</tt> 目录的路径。</li>

</ul>
