#use wml::debian::template title="Versionsfakta för Debian &ldquo;Trixie&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="ab3c0fa63d12dbcc8e7c3eaf4a72beb7b56d9741"

<if-stable-release release="trixie">

<p>Debian <current_release_trixie> släpptes
<a href="$(HOME)/News/<current_release_newsurl_trixie/>"><current_release_date_trixie></a>.
<ifneq "13.0" "<current_release>"
  "Debian 13.0 släpptes ursprungligen <:=spokendate('XXXXXXXX'):>."
/>
Utgåvan inkluderade många stora
förändringar, vilka beskrivs i vårt
our <a href="$(HOME)/News/XXXX/XXXXXXXX">pressmeddelande</a> och
<a href="releasenotes">versionsfakta</a>.</p>

#<p><strong>Debian 13 har ersatts av
#<a href="../forky/">Debian 14 (<q>Forky</q>)</a>.
#Säkerhetsuppdateringar har upphört från och med  <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Trixie drar dock nytta av långtidsstöd (Long Term Support, LTS) fram
#till slutet på xxxxx 20xx. LTS begränsas till i386, amd64, armel, armhf och arm64.
#Inga av de andra arkitekturerna har stöd i Trixie. För ytterligare information,
#var vänlig se <a
#href="https://wiki.debian.org/LTS">sektionen för LTS-stöd i Debianwikin</a>.
#</strong></p>

<p>För att få tag på och installera Debian, se
sidan för <a href="debian-installer/">installationsinformationen</a> och
<a href="installmanual">installationsguiden</a>. För att uppgradera från en
äldre Debianutgåva, se instruktionerna i
<a href="releasenotes">versionsfakta</a>.</p>

### Activate the following when LTS period starts.
#<p>Datorarkitekturer som stöds under tiden för långtidsstöd:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Datorarkitekturer som stöds i den ursprungliga utgåvan av Trixie:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Tvärt emot våra önskemål finns det en del problem i denna utgåva, även om den
kallas för <em>stabil</em>. Vi har sammanställt
<a href="errata">en lista över de största kända problemen</a>, och du kan alltid
<a href="../reportingbugs">rapportera andra problem</a> till oss.</p>

<p>Sist, men inte minst, har vi en lista över <a href="credits">folk som skall
ha tack</a> för att ha möjliggjort denna utgåva.</p>
</if-stable-release>

<if-stable-release release="bookworm">

<p>Kodnamnet för nästa Debianutgåva efter <a
href="../bookworm/">Bookworm</a> är <q>Trixie</q>.</p>

<p>Denna utgåva startade som en kopia på Bookworm och är för närvarande i ett
läge som kallas <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">testing</a></q>.
Detta betydera att saker inte ska gå sönder så illa som i unstable eller 
den experimentella distributionen, efter som paket tillåts endast att komma in
i denna distribution efter en viss tid har gått, samt när dom inte har några
utgåvekritiska fel rapporterade mot sig.</p>

<p>Vänligen notera att säkerhetsuppdateringar för <q>uttestningsdistributionen</q> 
<strong>inte</strong> hanteras av säkerhetsgruppen ännu. Därmed får <q>uttestningsutgåvan</q>
<strong>inte</strong> säkerhetsuppdateringar i god tid.
# För ytterligare information, var vänlig se
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">tillkännagivandet</a>
# från säkerhetsgruppen för uttestningsutgåvan.
Du uppmuntras att ändra dina inlägg i din
sources.list från testing till Bookworm för närvarande om du behöver
säkerhetsstöd. Se även inlägget i 
<a href="$(HOME)/security/faq#testing">Säkerhetsgruppens FAQ</a> för
<q>testing</q>-distributionen.</p>

<p>Det kan finnas ett <a href="releasenotes">utkast av versionsfakta tillgängligt</a>.
Vänligen <a href="https://bugs.debian.org/release-notes">kontrollera även
föreslagna tillägg till versionsfakta</a>.</p>

<p>För installationsavbildningar och dokumentation om hur du installerar <q>testing</q>,
se <a href="$(HOME)/devel/debian-installer/">sidan för Debianinstalleraren</a>.</p>

<p>För att få reda på mer om hur <q>testing</q>-utgåvan fungerar, se 
<a href="$(HOME)/devel/testing">utvecklarnas information om detta</a>.</p>

<p>Folk frågar ofta om det finns en enkel <q>förloppsmätare</q> för utgåvan.
Oturligt nog finns inte detta, men vi kan referera flera olika ställen som
beskriver vad som behövs göras före utgåvan sker:</p>
<ul>
  <li><a href="https://release.debian.org/">Allmän utgåvestatus</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Utgåve-kritiska fel</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Fel i grundsystemet</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Fel i standard- och uppgiftspaket</a></li>
</ul>

<p>Utöver detta, så skickar den utgåveansvarige allmänna statusrapporter
till <a href="https://lists.debian.org/debian-devel-announce/">\
sändlistan debian-devel-announce</a>.</p>

</if-stable-release>
