#use wml::debian::template title="Hur man använder Debians sökfunktion" MAINPAGE="true"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p>Debianprojektet erbjuder sin egen sökfunktion på
<a href="https://search.debian.org/">https://search.debian.org/</a>.
Här hittar du några tips på hur man kan använda den, och börja med enkla
sökningar såväl som mer komplexa sökningar med booleska uttryck.
</p>


<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <h3>Enkel sökning</h3>
      <p>Det enklaste sättet är att ange ett ensamt ord i sökrutan och
         trycka på Enter (eller klicka på <em>Sök</em>-knappen).
         Sökmaskinen kommer då att returnera alla sidor på webbplatsen som innehåller
         det ordet.
         Detta ger dig oftast bra resultat.</p>
      <p>Alternativt kan du söka efter mer än ett ord. Åter igen bör du
         se alla sidor av Debianwebben som innehåller alla ord som du matade in.
         För att söka efter fraser placera dem i citattecken ("). Vänligen
         notera att sökmotorn inte är skiftlägeskänslig, så en sökning efter
         <code>gcc</code> matchar både "gcc" och "GCC".</p>
      <p>Under sökfältet kan du välja hur många resultat du vill se per sida.
      Det är även möjligt att välja ett annat språk; sökningen på Debians
      webbplats stödjer nästan 40 olika språk.</p>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <h3>Boolesk sökning</h3>
      <p>Om en enkel sökning inte är tillräcklig kan du använda Booleska
         sökoperatorer. Du kan välja mellan <em>AND</em>, <em>OR</em>, och
         <em>NOT</em> eller kombinera dessa tre. Vänligen säkerställ att du
         använder versaler för alla operatorer, så att sökmotorn känner igen dom.</p>

      <ul>
        <lI><b>AND</b> kombinerar två uttryck och returnerar sidor som
            innehåller båda ord. Exempelvis hittar <code>gcc AND patch</code>
            alla sidor som innehåller båda, "gcc" och "patch". I detta fall
            får du samma resultat som när du söker efter <code>gcc patch</code>,
            men en explicit <code>AND</code> kan vara användbar i kombination
            med andra operatorer.</li>
        <li><b>OR</b> returnerar resultatet om något av orden finns på sidan.
            <code>gcc OR patch</code> hittar alla sidor som innehåller antingen "gcc" 
            eller "patch".</li>
        <li><b>NOT</b> används för att exkludera söktermer från resultatet.
            Exempelvis, <code>gcc NOT patch</code> hittar alla resultat som
            innehåller "gcc", men inte "patch". <code>gcc AND NOT patch</code> 
            ger dig samma resultat, men att söka efter <code>NOT patch</code> 
            stöds inte.</li>
        <li><b>(...)</b> kan användas för att gruppera uttryck. Exempelvis
            kommer <code>(gcc OR make) NOT patch</code> att hitta alla sidor
            som innehåller antingen "gcc" eller "make", men som inte
            innehåller "patch".</li>
      </ul>
    </div>
  </div>
</div>

