#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664"
<define-tag pagetitle>Uppdaterad Debian 12; 12.5 utgiven</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin femte uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction apktool "Prevent arbitrary file writes with malicious resource names [CVE-2024-21633]">
<correction atril "Fix crash when opening some epub files; fix index loading for certain epub documents; add fallback for malformed epub files in check_mime_type; use libarchive instead of external command for extracting documents [CVE-2023-51698]">
<correction base-files "Update for the 12.5 point release">
<correction caja "Fix desktop rendering artifacts after resolution changes; fix use of <q>informal</q> date format">
<correction calibre "Fix <q>HTML Input: Don't add resources that exist outside the folder hierarchy rooted at the parent folder of the input HTML file by default</q> [CVE-2023-46303]">
<correction compton "Remove recommendation of picom">
<correction cryptsetup "cryptsetup-initramfs: Add support for compressed kernel modules; cryptsetup-suspend-wrapper: Don't error out on missing /lib/systemd/system-sleep directory; add_modules(): Change suffix drop logic to match initramfs-tools">
<correction debian-edu-artwork "Provide an Emerald theme based artwork for Debian Edu 12">
<correction debian-edu-config "New upstream release">
<correction debian-edu-doc "Update included documentation and translations">
<correction debian-edu-fai "New upstream release">
<correction debian-edu-install "New upstream release; fix security sources.list">
<correction debian-installer "Increase Linux kernel ABI to 6.1.0-18; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-ports-archive-keyring "Add Debian Ports Archive Automatic Signing Key (2025)">
<correction dpdk "New upstream stable release">
<correction dropbear "Fix <q>terrapin attack</q> [CVE-2023-48795]">
<correction engrampa "Fix several memory leaks; fix archive <q>save as</q> functionality">
<correction espeak-ng "Fix buffer overflow issues [CVE-2023-49990 CVE-2023-49992 CVE-2023-49993], buffer underflow issue [CVE-2023-49991], floating point exception issue [CVE-2023-49994]">
<correction filezilla "Prevent <q>Terrapin</q> exploit [CVE-2023-48795]">
<correction fish "Handle Unicode non-printing characters safely when given as command substitution [CVE-2023-49284]">
<correction fssync "Disable flaky tests">
<correction gnutls28 "Fix assertion failure when verifying a certificate chain with a cycle of cross signatures [CVE-2024-0567]; fix timing side-channel issue [CVE-2024-0553]">
<correction indent "Fix buffer under read issue [CVE-2024-0911]">
<correction isl "Fix use on older CPUs">
<correction jtreg7 "New source package to support builds of openjdk-17">
<correction libdatetime-timezone-perl "Update included timezone data">
<correction libde265 "Fix buffer overflow issues [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libfirefox-marionette-perl "Fix compatibility with newer firefox-esr versions">
<correction libmateweather "Fix URL for aviationweather.gov">
<correction libspreadsheet-parsexlsx-perl "Fix possible memory bomb [CVE-2024-22368]; fix XML External Entity issue [CVE-2024-23525]">
<correction linux "New upstream stable release; bump ABI to 18">
<correction linux-signed-amd64 "New upstream stable release; bump ABI to 18">
<correction linux-signed-arm64 "New upstream stable release; bump ABI to 18">
<correction linux-signed-i386 "New upstream stable release; bump ABI to 18">
<correction localslackirc "Send authorization and cookie headers to the websocket">
<correction mariadb "New upstream stable release; fix denial of service issue [CVE-2023-22084]">
<correction mate-screensaver "Fix memory leaks">
<correction mate-settings-daemon "Fix memory leaks; relax High DPI limits; fix handling of multiple rfkill events">
<correction mate-utils "Fix various memory leaks">
<correction monitoring-plugins "Fix check_http plugin when <q>--no-body</q> is used and the upstream response is chunked">
<correction needrestart "Fix microcode check regression on AMD CPUs">
<correction netplan.io "Fix autopkgtests with newer systemd versions">
<correction nextcloud-desktop "Fix <q>fails to sync files with special chars like ':'</q>; fix two-factor authentication notifications">
<correction node-yarnpkg "Fix use with Commander 8">
<correction onionprobe "Fix initialisation of Tor if using hashed passwords">
<correction pipewire "Use malloc_trim() when available to release memory">
<correction pluma "Fix memory leak issues; fix double activation of extensions">
<correction postfix "New upstream stable release; address SMTP smuggling issue [CVE-2023-51764]">
<correction proftpd-dfsg "Implement fix for the Terrapin attack [CVE-2023-48795]; fix out-of-bounds read issue [CVE-2023-51713]">
<correction proftpd-mod-proxy "Implement fix for the Terrapin attack [CVE-2023-48795]">
<correction pypdf "Fix infinite loop issue [CVE-2023-36464]">
<correction pypdf2 "Fix infinite loop issue [CVE-2023-36464]">
<correction pypy3 "Avoid an rpython assertion error in the JIT if integer ranges don't overlap in a loop">
<correction qemu "New upstream stable release; virtio-net: correctly copy vnet header when flushing TX [CVE-2023-6693]; fix null pointer dereference issue [CVE-2023-6683]; revert patch causing regressions in suspend / resume functionality">
<correction rpm "Enable the read-only BerkeleyDB backend">
<correction rss-glx "Install screensavers into /usr/libexec/xscreensaver; call GLFinish() prior to glXSwapBuffers()">
<correction spip "Fix two cross-site scripting issues">
<correction swupdate "Prevent acquiring root privileges through inappropriate socket mode">
<correction systemd "New upstream stable release; fix missing verification issue in systemd-resolved [CVE-2023-7008]">
<correction tar "Fix boundary checking in base-256 decoder [CVE-2022-48303], handling of extended header prefixes [CVE-2023-39804]">
<correction tinyxml "Fix assertion issue [CVE-2023-34194]">
<correction tzdata "New upstream stable release">
<correction usb.ids "Update included data list">
<correction usbutils "Fix usb-devices not printing all devices">
<correction usrmerge "Clean up biarch directories when not needed; don't run convert-etc-shells again on converted systems; handle mounted /lib/modules on Xen systems; improve error reporting; add versioned conflicts with libc-bin, dhcpcd, libparted1.8-10 and lustre-utils">
<correction wolfssl "Fix security issue when client sent neither PSK nor KSE extensions [CVE-2023-3724]">
<correction xen "New upstream stable release; security fixes [CVE-2023-46837 CVE-2023-46839 CVE-2023-46840]">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5578 ghostscript>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5583 gst-plugins-bad1.0>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5589 node-undici>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5593 linux-signed-amd64>
<dsa 2024 5593 linux-signed-arm64>
<dsa 2024 5593 linux-signed-i386>
<dsa 2024 5593 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5601 php-phpseclib3>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5607 chromium>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5609 slurm-wlm>
<dsa 2024 5610 redis>
<dsa 2024 5611 glibc>
<dsa 2024 5612 chromium>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>



<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


