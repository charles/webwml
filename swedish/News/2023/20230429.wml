<define-tag pagetitle>Uppdaterad Debian 11; 11.7 utgiven</define-tag>
<define-tag release_date>2023-04-29</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="b6a020c494e8e886f401ab63178faa0db93e39da" maintainer="Andreas Rönnquist"

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin sjunde uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>).
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction akregator "Fix validity checks, including fixing deletion of feeds and folders">
<correction apache2 "Don't automatically enable apache2-doc.conf; fix regressions in http2 and mod_rewrite introduced in 2.4.56">
<correction at-spi2-core "Set stop timeout to 5 andras, so as not to needlessly block system shutdowns">
<correction avahi "Fix local denial of service issue [CVE-2021-3468]">
<correction base-files "Update for the 11.7 point release">
<correction c-ares "Prevent stack overflow and denial of service [CVE-2022-4904]">
<correction clamav "New upstream stable release; fix possible remote code execution issue in the HFS+ file parser [CVE-2023-20032], possible information leak in the DMG file parser [CVE-2023-20052]">
<correction command-not-found "Add new non-free-firmware component, fixing upgrades to bookworm">
<correction containerd "Fix denial of service issue [CVE-2023-25153]; fix possible privilege escalation via incorrect setup of supplementary groups [CVE-2023-25173]">
<correction crun "Fix capability escalation issue due to containers being incorrectly started with non-empty default permissions [CVE-2022-27650]">
<correction cwltool "Add missing dependency on python3-distutils">
<correction debian-archive-keyring "Add bookworm keys; move stretch keys to the removed keyring">
<correction debian-installer "Increase Linux kernel ABI to 5.10.0-22; rebuild against proposed-updates">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-ports-archive-keyring "Extend the 2023 signing key's expiration by one year; add 2024 signing key; move 2022 signing key to the removed keyring">
<correction dpdk "New upstream stable release">
<correction duktape "Fix crash issue [CVE-2021-46322]">
<correction e2tools "Fix build failure by adding build dependency on e2fsprogs">
<correction erlang "Fix client authentication bypass issue [CVE-2022-37026]; use -O1 optimization for armel because -O2 makes erl segfault on certain platforms, e.g. Marvell">
<correction exiv2 "Security fixes [CVE-2021-29458 CVE-2021-29463 CVE-2021-29464 CVE-2021-29470 CVE-2021-29473 CVE-2021-29623 CVE-2021-32815 CVE-2021-34334 CVE-2021-34335 CVE-2021-3482 CVE-2021-37615 CVE-2021-37616 CVE-2021-37618 CVE-2021-37619 CVE-2021-37620 CVE-2021-37621 CVE-2021-37622 CVE-2021-37623]">
<correction flask-security "Fix open redirect vulnerability [CVE-2021-23385]">
<correction flatpak "New upstream stable release; escape special characters when displaying permissions and metadata [CVE-2023-28101]; don't allow copy/paste via the TIOCLINUX ioctl when running in a Linux virtual console [CVE-2023-28100]">
<correction galera-3 "New upstream stable release">
<correction ghostscript "Fix path for PostScript helper file in ps2epsi">
<correction glibc "Fix memory leak in printf-family functions with long multibyte strings; fix crash in printf-family due to width/precision-dependent allocations; fix segfault in printf handling thousands separator; fix overflow in the AVX2 implementation of wcsnlen when crossing pages">
<correction golang-github-containers-common "Fix parsing of DBUS_SESSION_BUS_ADDRESS">
<correction golang-github-containers-psgo "Do not enter the process user namespace [CVE-2022-1227]">
<correction golang-github-containers-storage "Make previously internal functions publicly accessible, required to allow fixing CVE-2022-1227 in other packages">
<correction golang-github-prometheus-exporter-toolkit "Patch tests to avoid race condition; fix authentication cache poisoning issue [CVE-2022-46146]">
<correction grep "Fix incorrect matching when the last of multiple patterns includes a backreference">
<correction gtk+3.0 "Fix Wayland + EGL on GLES-only platforms">
<correction guix "Fix build failure due to expired keys used in test suite">
<correction intel-microcode "New upstream bug-fix release">
<correction isc-dhcp "Fix IPv6 address lifetime handling">
<correction jersey1 "Fix build failure with libjettison-java 1.5.3">
<correction joblib "Fix arbitrary code execution issue [CVE-2022-21797]">
<correction lemonldap-ng "Fix URL validation bypass issue; fix 2FA issue when using AuthBasic handler [CVE-2023-28862]">
<correction libapache2-mod-auth-openidc "Fix open redirect issue [CVE-2022-23527]">
<correction libapreq2 "Fix buffer overflow issue [CVE-2022-22728]">
<correction libdatetime-timezone-perl "Update included data">
<correction libexplain "Enhance compatibility with newer kernel versions - Linux 5.11 no longer has if_frad.h, termiox removed since kernel 5.12">
<correction libgit2 "Enable SSH key verification by default [CVE-2023-22742]">
<correction libpod "Fix privilege escalation issue [CVE-2022-1227]; fix capability escalation issue due to containers being incorrectly started with non-empty default permissions [CVE-2022-27649]; fix parsing of DBUS_SESSION_BUS_ADDRESS">
<correction libreoffice "Change Croatia's default currency to Euro; avoid empty -Djava.class.path= [CVE-2022-38745]">
<correction libvirt "Fix container reboot-related issues; fix test failures when combined with newer Xen versions">
<correction libxpm "Fix infinite loop issues [CVE-2022-44617 CVE-2022-46285]; fix double free issue in error handling code; fix <q>compression commands depend on PATH</q> [CVE-2022-4883]">
<correction libzen "Fix null pointer dereference issue [CVE-2020-36646]">
<correction linux "New upstream stable release; increase ABI to 22; [rt] update to 5.10.176-rt86">
<correction linux-signed-amd64 "New upstream stable release; increase ABI to 22; [rt] update to 5.10.176-rt86">
<correction linux-signed-arm64 "New upstream stable release; increase ABI to 22; [rt] update to 5.10.176-rt86">
<correction linux-signed-i386 "New upstream stable release; increase ABI to 22; [rt] update to 5.10.176-rt86">
<correction lxc "Fix file existence oracle [CVE-2022-47952]">
<correction macromoleculebuilder "Fix build failure by adding build dependency on docbook-xsl">
<correction mariadb-10.5 "New upstream stable release; revert upstream libmariadb API change">
<correction mono "Remove desktop file">
<correction ncurses "Guard against corrupt terminfo data [CVE-2022-29458]; fix tic crash on very long tc/use clauses">
<correction needrestart "Fix warnings when using <q>-b</q> option">
<correction node-cookiejar "Guard against maliciously-sized cookies [CVE-2022-25901]">
<correction node-webpack "Avoid cross-realm object access [CVE-2023-28154]">
<correction nvidia-graphics-drivers "New upstream release; security fixes [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-450 "New upstream release; security fixes [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-graphics-drivers-tesla-470 "New upstream release; security fixes [CVE-2023-0180 CVE-2023-0184 CVE-2023-0185 CVE-2023-0187 CVE-2023-0188 CVE-2023-0189 CVE-2023-0190 CVE-2023-0191 CVE-2023-0194 CVE-2023-0195 CVE-2023-0198 CVE-2023-0199]">
<correction nvidia-modprobe "New upstream release">
<correction openvswitch "Fix <q>openvswitch-switch update leaves interfaces down</q>">
<correction passenger "Fix compatibility with more recent NodeJS versions">
<correction phyx "Remove unnecessary build dependency on libatlas-cpp">
<correction postfix "New upstream stable release">
<correction postgis "Fix wrong Polar stereographic axis order">
<correction postgresql-13 "New upstream stable release; fix client memory disclosure issue [CVE-2022-41862]">
<correction python-acme "Fix version of created CSRs, to prevent problems with strictly RFC-complying implementations of the ACME API">
<correction ruby-aws-sdk-core "Fix generation of version file">
<correction ruby-cfpropertylist "Fix some functionality by dropping compatibility with Ruby 1.8">
<correction shim "New upstream release; new upstream stable release; enable NX support at build time; block Debian grub binaries with sbat &lt; 4">
<correction shim-helpers-amd64-signed "New upstream stable release; enable NX support at build time; block Debian grub binaries with sbat &lt; 4">
<correction shim-helpers-arm64-signed "New upstream stable release; enable NX support at build time; block Debian grub binaries with sbat &lt; 4">
<correction shim-helpers-i386-signed "New upstream stable release; enable NX support at build time; block Debian grub binaries with sbat &lt; 4">
<correction shim-signed "New upstream stable release; enable NX support at build time; block Debian grub binaries with sbat &lt; 4">
<correction snakeyaml "Fix denial of service issues [CVE-2022-25857 CVE-2022-38749 CVE-2022-38750 CVE-2022-38751]; add documentation regarding security support / issues">
<correction spyder "Fix duplication of code when saving">
<correction symfony "Remove private headers before storing responses with HttpCache [CVE-2022-24894]; remove CSRF tokens from storage on successful login [CVE-2022-24895]">
<correction systemd "Fix information leak issue [CVE-2022-4415], denial of service issue [CVE-2022-3821]; ata_id: fix getting Response Code from SCSI Sense Data; logind: fix getting property OnExternalPower via D-Bus; fix crash in systemd-machined">
<correction tomcat9 "Add OpenJDK 17 support to JDK detection">
<correction traceroute "Interpret v4mapped-IPv6 addresses as IPv4">
<correction tzdata "Update included data">
<correction unbound "Fix Non-Responsive Delegation Attack [CVE-2022-3204]; fix <q>ghost domain names</q> issue [CVE-2022-30698 CVE-2022-30699]">
<correction usb.ids "Update included data">
<correction vagrant "Add support for VirtualBox 7.0">
<correction voms-api-java "Fix build failures by disabling some non-working tests">
<correction w3m "Fix out-of-bounds write issue [CVE-2022-38223]">
<correction x4d-icons "Fix build failure with newer imagemagick versions">
<correction xapian-core "Prevent database corruption on disk exhaustion">
<correction zfs-linux "Add several stability improvements">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2022 5170 nodejs>
<dsa 2022 5237 firefox-esr>
<dsa 2022 5238 thunderbird>
<dsa 2022 5259 firefox-esr>
<dsa 2022 5262 thunderbird>
<dsa 2022 5282 firefox-esr>
<dsa 2022 5284 thunderbird>
<dsa 2022 5300 pngcheck>
<dsa 2022 5301 firefox-esr>
<dsa 2022 5302 chromium>
<dsa 2022 5303 thunderbird>
<dsa 2022 5304 xorg-server>
<dsa 2022 5305 libksba>
<dsa 2022 5306 gerbv>
<dsa 2022 5307 libcommons-net-java>
<dsa 2022 5308 webkit2gtk>
<dsa 2022 5309 wpewebkit>
<dsa 2022 5310 ruby-image-processing>
<dsa 2023 5311 trafficserver>
<dsa 2023 5312 libjettison-java>
<dsa 2023 5313 hsqldb>
<dsa 2023 5314 emacs>
<dsa 2023 5315 libxstream-java>
<dsa 2023 5316 netty>
<dsa 2023 5317 chromium>
<dsa 2023 5318 lava>
<dsa 2023 5319 openvswitch>
<dsa 2023 5320 tor>
<dsa 2023 5321 sudo>
<dsa 2023 5322 firefox-esr>
<dsa 2023 5323 libitext5-java>
<dsa 2023 5324 linux-signed-amd64>
<dsa 2023 5324 linux-signed-arm64>
<dsa 2023 5324 linux-signed-i386>
<dsa 2023 5324 linux>
<dsa 2023 5325 spip>
<dsa 2023 5326 nodejs>
<dsa 2023 5327 swift>
<dsa 2023 5328 chromium>
<dsa 2023 5329 bind9>
<dsa 2023 5330 curl>
<dsa 2023 5331 openjdk-11>
<dsa 2023 5332 git>
<dsa 2023 5333 tiff>
<dsa 2023 5334 varnish>
<dsa 2023 5335 openjdk-17>
<dsa 2023 5336 glance>
<dsa 2023 5337 nova>
<dsa 2023 5338 cinder>
<dsa 2023 5339 libhtml-stripscripts-perl>
<dsa 2023 5340 webkit2gtk>
<dsa 2023 5341 wpewebkit>
<dsa 2023 5342 xorg-server>
<dsa 2023 5343 openssl>
<dsa 2023 5344 heimdal>
<dsa 2023 5345 chromium>
<dsa 2023 5346 libde265>
<dsa 2023 5347 imagemagick>
<dsa 2023 5348 haproxy>
<dsa 2023 5349 gnutls28>
<dsa 2023 5350 firefox-esr>
<dsa 2023 5351 webkit2gtk>
<dsa 2023 5352 wpewebkit>
<dsa 2023 5353 nss>
<dsa 2023 5355 thunderbird>
<dsa 2023 5356 sox>
<dsa 2023 5357 git>
<dsa 2023 5358 asterisk>
<dsa 2023 5359 chromium>
<dsa 2023 5361 tiff>
<dsa 2023 5362 frr>
<dsa 2023 5363 php7.4>
<dsa 2023 5364 apr-util>
<dsa 2023 5365 curl>
<dsa 2023 5366 multipath-tools>
<dsa 2023 5367 spip>
<dsa 2023 5368 libreswan>
<dsa 2023 5369 syslog-ng>
<dsa 2023 5370 apr>
<dsa 2023 5371 chromium>
<dsa 2023 5372 rails>
<dsa 2023 5373 node-sqlite3>
<dsa 2023 5374 firefox-esr>
<dsa 2023 5375 thunderbird>
<dsa 2023 5376 apache2>
<dsa 2023 5377 chromium>
<dsa 2023 5378 xen>
<dsa 2023 5379 dino-im>
<dsa 2023 5380 xorg-server>
<dsa 2023 5381 tomcat9>
<dsa 2023 5382 cairosvg>
<dsa 2023 5383 ghostscript>
<dsa 2023 5384 openimageio>
<dsa 2023 5385 firefox-esr>
<dsa 2023 5386 chromium>
<dsa 2023 5387 openvswitch>
<dsa 2023 5388 haproxy>
<dsa 2023 5389 rails>
<dsa 2023 5390 chromium>
<dsa 2023 5391 libxml2>
<dsa 2023 5392 thunderbird>
<dsa 2023 5393 chromium>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction bind-dyndb-ldap "Trasig med nya versioner av bind9; omöjlig att ge support för i stabila utgåvan">
<correction matrix-mirage "Beroende på python-matrix-nio som är på väg att tas bort">
<correction pantalaimon "Beroende på python-matrix-nio som är på väg att tas bort">
<correction python-matrix-nio "Säkerhetsproblem: fungerar inte med nuvarande Matrixservrar">
<correction weechat-matrix "Beroende på python-matrix-nio som är på väg att tas bort">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


