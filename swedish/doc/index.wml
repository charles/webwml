#use wml::debian::template title="Användardokumentation" MAINPAGE="true"
#use wml::debian::translation-check translation="977e2dde6d14e393da27217063acd116c081d243"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">Snabbstart</a></li>
  <li><a href="#manuals">Manualer</a></li>
  <li><a href="#other">Andra (kortare) dokument</a></li>
</ul>

<p>
Att skapa ett högkvalitativt fritt operativsystem inkluderar även att skriva
tekniska manualer som beskriver hur program fungerar och används.
Debianprojektet anstränger sig för att tillhandahålla alla användare med
god dokumentation i ett lättåtkomligt format. Denna sida innehåller en samling
med länkar, som leder till installationsguider, HOWTOs, FAQs, versionsfakta,
vår Wiki, och mer.
</p>


<h2><a id="quick_start">Snabbstart</a></h2>

<p>
Om du är <em>nybörjare</em> på Debian rekommenderar vi att du börjar med
att följande två guider:</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">installationsguiden</a></li>
  <li><a href="manuals/debian-faq/">Debian GNU/Linux FAQ</a></li>
</ul>

<p>Ha dessa till hands när du installerar Debian för första gången, du
kommer troligen hitta svar på de flesta frågor du har och de kommer
hjälpa dig med att använda ditt nya Debiansystem.
</p>

<p>
Senare kan du vilja gå genom dessa dokument:
</p>

<ul>
  <li><a href="manuals/debian-reference/">Debianreferensen</a>: en kortfattad bruksanvisning, som fokuserar på skalkommandon</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">Versionsfakta</a>: publiceras vanligtvis med Debianuppdateringar, med målgruppen användare som uppgraderar distrbutionen</li>
  <li><a href="https://wiki.debian.org/">Debian Wiki</a>: den officiella Debianwikin</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> <a href="https://www.debian.org/doc/manuals/refcard/refcard">Skriv ut Referenskort för Debian GNU/Linux</a></button></p>

<h2><a id="manuals">Manualer</a></h2>



<p>Huvuddelen av den dokumentation som finns i Debian skrevs för GNU/Linux i
allmänhet, men det finns även en del dokumentation skriven specifikt för
Debian. Dessa dokument kan delas in i följande grundläggande kategorier:
</p>

<ul>
   <li>Manualer: Dessa guider liknar böcker, eftersom de utförligt beskriver viktiga ämnen.
   En hel del av de manualer som listas nedan finns tillgängliga både online
   och i Debianpaket. Faktum är att de flesta manualer på webbplatsen extraheras
   från deras respektive Debianpaket. Välj en manual nedan för dess paketnamn
   och/eller länkar till onlineversionerna.</li>
   <li>HOWTOs: Så som namnet antyder beskriver <a
   href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">HOWTO-dokument</a>
   <em>hur</em> man gör en viss sak, d.v.s. erbjuder de detaljerad och praktiska
   råd om hur man gör något.</li>
   <li>FAQs: Vi har sammanställt flera dokument som besvarar <em>vanliga
   frågor</em>. Debianrelaterade frågor besvaras i <a
   href="manuals/debian-faq/">Debian FAQ</a>. Utöver detta finns det en
   separat <a href="../CD/faq/">FAQ om Debians CD/DVD-avbildningar</a>, som
   besvarar alla typer av frågor rörande installationsmedia.</li>
   <li>Andra (kortare) dokument: Vänligen kolla också på <a
   href="#other">listan</a> över kortare instruktioner.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> För en fullständig lista på
Debianmanualer och annan dokumentation, vänligen besök sidan för
<a href="ddp">Debians dokumentationsprojekt</a>. Utöver detta finns flera
användarorienterade manualer som skrivits för Debian GNU/Linux tillgängliga
som <a href="books">tryckta böcker</a>.</p>
</aside>

<p>Många av manualerna som listas här finns tillgängliga både online och i
Debianpaket: faktum är att de flesta manualerna på webbplatsen extraheras från
deras respektive Debianpaket. Välj en manual nedan för dess paketnamn och/eller
länkar till onlineversionen.</p>

<h3>Manualer specifika för Debian</h3>

<div class="line">
  <div class="item col50">
    
    <h4><a href="user-manuals">Manualer för användare</a></h4>

    <ul>
      <li><a href="https://lescahiersdudebutant.arpinux.org/bookworm-en/">Nybörjarhandboken för Debian Bookworm</a></li>
      <li><a href="user-manuals#faq">Debian GNU/Linux FAQ</a>
          (frågor och svar)</li>
      <li><a href="user-manuals#install">Debians installationsguide</a></li>
      <li><a href="user-manuals#relnotes">Debians versionsfakta</a></li>
      <li><a href="user-manuals#refcard">Debians referenskort</a></li>
      <li><a href="user-manuals#debian-handbook">Handbok för Debianadministratörer</a></li>
      <li><a href="user-manuals#quick-reference">Debianreferens</a></li>
      <li><a href="user-manuals#securing">Securing Debian Manual</a></li>
      <li><a href="user-manuals#aptitude">aptitude användarmanual</a></li>
      <li><a href="user-manuals#apt-guide">APT User's Guide</a></li>
      <li><a href="user-manuals#java-faq">Debian GNU/Linux and Java FAQ</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Debian Hamradio Maintainer’s Guide</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Manualer för utvecklare</a></h4>

    <ul>
      <li><a href="devel-manuals#policy">Debians policyhandbok</a></li>
      <li><a href="devel-manuals#devref">Debians utvecklarreferens</a></li>
      <li><a href="devel-manuals#debmake-doc">Guide för Debian Maintainers</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Introduktion till Debianpaketering</a></li>
      <li><a href="devel-manuals#menu">Debians menysystem</a></li>
      <li><a href="devel-manuals#d-i-internals">De inre delarna av Debian Installer</a></li>
      <li><a href="devel-manuals#dbconfig-common">Guide för paketunderhållare som använder databaser</a></li>
		<li><a href="devel-manuals#dbapp-policy">Policy för paket som använder databaser</a></li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="misc-manuals#history">Läs Debianprojektets historik</a></button></p>


<h2><a id="other">Andra, kortare dokument</a></h2>

<p>Följande dokument innehåller snabbare, kortare instruktioner:</p>


<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Om du har kontrollerat
ovanstående resurser och fortfarande inte kan hitta svar på dina frågor eller
lösningar på dina problem rörande Debian, ta titt på vår <a href="../support">supportsida</a>.</p>
</aside>

  <dt><strong>Manualsidor</strong></dt>
    <dd>Traditionellt dokumenteras alla Unixprogram med
        <em>manualsidor</em>, referensmanualer som nås via kommandot
        <tt>man</tt>.
        De är vanligen inte avsedda för nybörjare.
        Du kan söka efter och läsa manualsidor i Debian via
        <a href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>.
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">info-filer</a></strong></dt>
    <dd>
      Många GNU-program dokumenteras med <em>info-filer</em> istället för
      man-sidor.
      Dessa filer innehåller detaljerad information om programmet, flaggor
      och användningsexempel och kan nås via kommandot <tt>info</tt>.
    </dd>

  <dt><strong>README-filer</strong></dt>
    <dd>README-filer är enkla textfiler som beskriver ett enstaka objekt,
       vanligtvis ett paket. Du kan hitta en mängd av dessa i
       underkatalogerna under <tt>/usr/share/doc/</tt> på ditt Debiansystem.
       Utöver README-filer kan dessa kataloger innehålla konfigurationsexempel.
       Vänligen notera att större programs dokumentation typiskt tillhandahålls
       i separata paket (med samma namn som originalpaketet, men med ändelsen
       <em>-doc</em>.
    </dd>

</dl>
