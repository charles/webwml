#use wml::debian::template title="Programme de Jonathan Carter" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="bd75b66353e8c9ba66efabecaa6872df63f53ba3" maintainer="Jean-Pierre Giraud"

<DIV class="main">
<TABLE CLASS="title">
<TR><TD>
<BR>
<H1 CLASS="titlemain"><BIG><B>Jonathan Carter</B></BIG><BR>
    <SMALL>Programme de DPL</SMALL><BR>
    <SMALL>15/03/2021</SMALL>
</H1>
<H3>
<A HREF="mailto:jcc@debian.org"><TT>jcc@debian.org</TT></A><BR>
<A HREF="https://jonathancarter.org"><TT>https://jonathancarter.org</TT></A><BR>
<A HREF="https://wiki.debian.org/highvoltage"><TT>https://wiki.debian.org/highvoltage</TT></A>
</H3>
</TD></TR>
</TABLE>

<H2 CLASS="section">1. Informations personnelles</H2>

<P>Hello, mon nom est Jonathan Carter, connu aussi comme <EM>highvoltage</EM>, avec comme pseudo
dans Debian <EM>jcc</EM>, et je suis candidat à un second mandat de DPL. </P>

<P>Je suis un développeur Debian de 39 ans ayant plusieurs domaines d’intérêt dans le projet Debian.
Comme beaucoup d'autres individus et organisations qui contribuent à Debian, je participe au projet
parce que j'apprécie les produits de nos travaux et j'aimerai que cela continue à exister, parce que
Debian reste seule à fournir une distribution débarrassée des contraintes de licences strictes, tout
en fournissant des versions stables et des mises à jour de sécurité en même temps qu'une très large
sélection de paquets logiciels. Je crois que Debian est un projet logiciel extrêmement important et
que son contrat social ajoute au projet un caractère unique en mettant en avant de façon explicite les
priorités de nos utilisateurs. Nous ne dépendons pas d'actionnaires ou du résultat financier du
trimestre, mais nous travaillons plutôt tous ensemble pour essayer de trouver les meilleures solutions
pour nos utilisateurs. Bien que cela ne soit pas toujours facile ou simple, je trouve cela très gratifiant
et que faire cela vaut la peine d'être fait.</P>


<H2 CLASS="section">2. Pourquoi suis-je candidat au poste de DPL</H2>

<P>
Durant mon dernier mandat, je visais à apporter un sentiment de stabilité et de normalité au projet. En
dépit du chaos qui a régné dans le monde autour de nous, et avec l'aide d'autres membres du projet, je
pense que nous avons réussi à établir cette stabilité. Dans l'ensemble, notre projet possède de nombreux
points forts, nous avons une communauté active et dynamique, nous avons des ressources suffisantes pour
porter le projet pendant un moment, nous sommes également en bonne voie de produire une nouvelle version
stable cette année avec le gel initial de Debian 11 arrivant comme prévu.
</P>

<P>
J'aimerais assister à une poussée de croissance de Debian, mais je ne pense pas que le projet soit prêt pour
cela. Nous avons déjà beaucoup crû au fil des ans, mais d'un point de vue administratif, nous n'avons pas
suivi cette croissance. Nous avons actuellement plusieurs points faibles qui, à mon avis, doivent être
corrigés avant que le projet puisse continuer sa croissance, sinon cela pourrait avoir des conséquences
indésirables à l'avenir.
</P>

<P>
Dans ce mandat, j'aimerais poursuivre le travail que j'ai commencé durant le mandat précédent, en mettant
un accent supplémentaire à l'amélioration de nos problèmes administratifs.
</P>


<H2 CLASS="section">3. Approche</H2>

<H3 CLASS="subsection"> 3.1. Corriger les points faibles administratifs </H3>

<P><B>Améliorer la comptabilité.</B> Il est en réalité très difficile de suivre combien d'argent nous
avons et combien nous dépensons (et à quoi servent ces dépenses). Il n'y a pas beaucoup de transparence
sur nos finances, pas même pour les membres du projet. J'ai donné quelques nouvelles sur les finances
sur la liste debian-private et inclus certaines d'entre elles dans mes communications <q>Brèves du chef
du projet Debian</q>, mais je crois que c'est vraiment quelque chose à laquelle notre équipe de trésorerie
devrait veiller. Habituellement, ces rapports s'appuient sur les documents publics de nos organismes
habilités (« Trusted Organizations ») qui sont parfois vieux de plusieurs mois. L'équipe de trésorerie
a quelques excellentes idées pour résoudre certains des problèmes, comme une comptabilité avec des postes
dont les codes sont partagés entre les organismes habilités, de telle sorte que nous puissions obtenir de
meilleurs rapports en temps réel. Globalement, j'aimerais que l'équipe de trésorerie ait une meilleure
capacité pour faire ce qu'elle doit faire, ce qui, dans l'idéal, allègerait un peu la charge du DPL, et je vais
traiter de certains de ces problèmes dans les points suivants, dans la mesure où certains d'entre eux sont
indissociables.</P>

<P><B>Mettre en œuvre une politique de dépenses.</B> Pour réaliser notre travail dans Debian, nous avons
besoin de dépenser un peu d'argent du projet. Parfois c'est pour des réunions (transport, collations,
séjour, etc.), pour du matériel (équipement spécialisé, équipement personnel, coûts d'hébergement, etc.)
ou pour des raisons administratives (comme des noms de domaine, des droits de marque, des frais
légaux, etc.). J'aimerais mettre en œuvre une politique facilitant la compréhension par les développeurs
Debian de quel argent ils peuvent disposer et comment. Durant mon mandat, j'ai appris qu'il y avait
beaucoup d'incohérences parmi les membres de Debian sur ce qu'ils croyaient qui pouvait être fait avec
l'argent de Debian, faisant que certains membres n'utilisent pas les ressources du projet qui auraient
pu être employées au bénéfice du projet. Je crois qu'une politique adéquate pourrait encourager à mieux
dépenser et favoriser la croissance du projet. Je pense également que si nous avions une politique de
dépenses correcte, nous pourrions déléguer l’approbation des dépenses. Par exemple, nous pourrions avoir
une liste de prérequis qui, s'ils sont remplis, permettent aux délégués d'approuver la dépense sans
intervention du DPL, réduisant ainsi la charge du DPL. Je crois que ce serait une tâche qui pourrait
naturellement revenir à l'équipe de trésorerie, cependant, si elle pense que ce n'est pas logique, une
nouvelle délégation pourrait être créée.</P>

<P><B>Organismes habilités.</B> Nos relations avec les organismes habilités nécessitent davantage de
travail. L'année dernière, nous avons eu un conflit (au moins mineur) avec SPI aux sujets des frais
administratifs pesant sur les dons à Debian. Il apparaît qu'il y avait au moins deux accords différents
(contradictoires) conclus entre différents groupes de personnes qui avaient eu des discussions sur le
sujet. Dans le passé, nous avons pu être suffisamment petits pour que des <q>gentleman agreements</q> oraux
aient pu fonctionner, mais nous avons clairement crû suffisamment pour que nous ayons absolument besoin
que tous les accords soient passés par écrit. La situation concernant les dons à la Debconf a déjà provoqué
suffisamment de griefs, mais malheureusement il y a encore pire. D'un point de vue légal, les membres
impliqués souhaitaient représenter Debian, mais dans la mesure où Debian n'est nulle part constituée en
organisation enregistrée, nous pensions qu'il serait naturel de se replier sur SPI comme notre organisation
enregistrée dans ce dossier. L'entreprise légale voulait la convention ou le contrat liant Debian et SPI
pour poursuivre, mais de façon alarmante, ni moi-même ni SPI n'avons pu trouver de document et il ne
semble pas exister. Nous avons des critères pour les organismes habilités sur notre wiki, mais cela
n'équivaut pas à un accord juridique entre deux parties. Deux organismes habilités ont aussi disparus et
nous n'avons aucune idée de ce que sont devenus les fonds qu'ils détenaient. Nous avons fait tous les
efforts possibles pour contacter les individus responsables et essayer d'obtenir des réponses.
Malheureusement, j'ai peur que l'absence de quelconques accords formels puisse signifier que nous avons
perdu de l'argent définitivement. J'accorde une pleine confiance à nos organismes habilités actuels, qui
sont SPI, Debian France et Debian.ch, mais je crois que nous avons besoin de mieux formaliser nos
relations avec nos organismes habilités et également de mieux appliquer à ces organismes les exigences
listées sur notre <a href="https://wiki.debian.org/Teams/DPL/TrustedOrganizationCriteria">wiki</a>.
</P>

<P><B>Envisager l'enregistrement formel de l'organisation Debian</B>. J'aimerais lancer la discussion
sur l'enregistrement de Debian comme organisation formelle. L'an dernier, la campagne de chef de projet
de Brian Gupta tournait largement autour de l'établissement d'une Fondation pour le projet Debian. Je
suis d'accord avec beaucoup de ses raisons en faveur de la poursuite dans cette voie. C'est probablement
le moment de dire clairement que ma campagne de DPL n'est pas un référendum sur ce sujet pas plus qu'une
promesse de campagne, je crois que c'est un sujet que nous devons traiter sérieusement collectivement en
tant que projet. Plus haut j'ai mentionné que je pense que nous avons besoin de meilleurs accords avec
nos organisations habilitées. Certaines personnes possédant des compétences juridiques (bien que non
juristes) m'ont rappelé qu'un accord entre une association non enregistrée de bénévoles et un organisme
habilité ne veut pas dire grand-chose dans beaucoup de pays et qu'il faudrait que nous passions cet
accord en tant qu'organisation enregistrée pour que cela ait du sens. D'après ce que j'ai lu jusqu'à
présent, une Fondation serait surdimensionnée pour les objectifs de Debian. Cela nécessiterait un conseil
d'administration et exigerait des contrôles rigoureux. D'après ce que je comprends, une simple organisation
sans but lucratif déclarée, dont les membres seraient les membres du projet Debian, serait peut-être
bien plus appropriée. Et d'après ce que je comprends, la déclaration de cette organisation pourrait aussi
réduire la responsabilité juridique individuelle au sein du projet. Je crois que nous devrions dépenser
quelques moyens pour des consultations pour trouver ce qui pourrait être le mieux pour le projet et pour
que nous puissions décider collectivement comment nous souhaitons procéder. Cela pourrait probablement
aboutir au vote d'une résolution générale.</P>


<H3 CLASS="subsection"> 3.2. Poursuite des objectifs du mandat actuel </H3>


<P><B>Équipes locales.</B> Je suis très heureux que l'initiative des équipes locales ait été relancée
lors de DebConf 20. J'aimerais y insuffler encore plus d'énergie de manière à ce que nous soyons en
mesure de tenir beaucoup plus de réunions en présentiel dès qu'on pourra le faire à nouveau sans risque
et que les équipes locales aient tout ce dont elles ont besoin pour attirer les gens curieux dans leurs
manifestations. J'aimerais que la gestion des équipes locales se développe en une délégation qui pourrait
s'emparer d'une partie des responsabilités du DPL quand il s'agit de décisions concernant les équipes
locales. Je crois toujours que l'organisation de réunions et d'événements par les équipes locales est une
bonne stratégie pour accroître à la fois la diversité et la participation dans le projet et devrait être
une priorité.</P>

<P><B>Continuer à améliorer l'accueil.</B> J'aimerais remercier particulièrement Enrico Zini pour
le travail qu'il a entrepris l'année passée pour simplifier le processus d'admission des nouveaux
responsables, qui a déjà de façon certaine apporté d'importantes améliorations et qui a très
probablement eu pour résultat l'admission de plus de DD et DM que nous n'en aurions eu sans ces
améliorations. Je pense vraiment que nous pouvons faire encore mieux et avoir des documents et des
vidéos d'introduction sur les outils que nous utilisons et sur leur fonctionnement (comme Salsa, le
wiki, les listes de diffusions, etc.). J'aimerais avoir de nouvelles suggestions du projet pour
savoir si nous pourrions envisager des concours ou des récompenses pour ce type de contenu.</P>

<H2> 4. Remerciement </H2>

<UL>
<LI>J’ai utilisé <a href="https://www.debian.org/vote/2010/platforms/zack">la mise en page du programme de Zack</a> comme base pour celle-ci.</LI>
</UL>

<!--
<H2> A. Réfutations </H2>

<H3> A.1. Sruthi Chandran </H3>

<P>
</P>
-->

<H2> A. Journal des modifications </H2>

<P> Les versions de ce programme sont gérées dans un <a href="https://salsa.debian.org/jcc/dpl-platform">dépôt git.</a> </P>

<UL>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/3.0.0">3.0.0</a> : Nouveau programme pour les élections 2021 du DPL.</LI>
</UL>

<BR>

</DIV> <!-- END MAIN -->


