# templates.sk.po
# Copyright (C) 2004 Free Software Foundation, Inc.
# Stanislav Valasek <valasek@fastmail.fm>, 2004.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2013, 2017.
msgid ""
msgstr ""
"Project-Id-Version: templates.sk.po 1.1\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-07-12 16:39+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Webstránky Debianu"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Vyhľadávanie na webe Debianu."

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "Hľadať na webe Debianu"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Áno"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Nie"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Projekt Debian"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian GNU/Linux je slobodný operačný systém a distribúcia slobodného "
"softvéru. Spravujú a aktualizujú ju mnohí používatelia, ktorí dobrovoľne "
"prispievajú svojím úsilím a voľným časom."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr ""
"debian, GNU, linux, unix, open source, free, DFSG, slobodný softvér, zadarmo"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "Späť na <a href=\"m4_HOME/\">úvodnú stránku projektu Debian</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Domovská stránka"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Preskočiť Quicknav"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "O&nbsp;Debiane"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "O&nbsp;Debiane"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Napíšte nám"

#: ../../english/template/debian/common_translation.wml:37
#, fuzzy
msgid "Legal Info"
msgstr "Informácie o vydaniach"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr ""

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Sponzorstvo"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Udalosti"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Novinky"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Distribúcia"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Podpora"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr ""

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Pre vývojárov"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Dokumentácia"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Bezpečnostné správy"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Hľadať"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "žiadne"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Choď"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "celosvetovo"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Prehľad stránok"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Ostatné"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Ako&nbsp;získať&nbsp;Debian"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "Blog o Debiane"

#: ../../english/template/debian/common_translation.wml:94
#, fuzzy
#| msgid "Debian Project News"
msgid "Debian Micronews"
msgstr "Novinky projektu Debian"

#: ../../english/template/debian/common_translation.wml:97
#, fuzzy
#| msgid "Debian Project"
msgid "Debian Planet"
msgstr "Projekt Debian"

#: ../../english/template/debian/common_translation.wml:100
#, fuzzy
msgid "Last Updated"
msgstr "Posledná zmena"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Prosím, komentáre, kritiku a návrhy týkajúce sa týchto webových stránok "
"posielajte do našej <a href=\"mailto:debian-doc@lists.debian.org"
"\">konferencie</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "nepotrebné"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "nedostupné"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "N/A"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "vo vydaní 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "vo vydaní 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "vo vydaní 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "vo vydaní 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "vo vydaní 2.2"

#: ../../english/template/debian/footer.wml:84
msgid ""
"See our <a href=\"m4_HOME/contact\">contact page</a> to get in touch. Web "
"site source code is <a href=\"https://salsa.debian.org/webmaster-team/webwml"
"\">available</a>."
msgstr ""

#: ../../english/template/debian/footer.wml:87
msgid "Last Modified"
msgstr "Posledná zmena"

#: ../../english/template/debian/footer.wml:90
msgid "Last Built"
msgstr "Posledné zostavenie"

#: ../../english/template/debian/footer.wml:93
msgid "Copyright"
msgstr "Autorské práva"

#: ../../english/template/debian/footer.wml:96
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> a iné;"

#: ../../english/template/debian/footer.wml:99
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"Prečítajte si <a href=\"m4_HOME/license\" rel=\"copyright\">licenčné "
"podmienky</a>"

#: ../../english/template/debian/footer.wml:102
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian je registrovaná <a href=\"m4_HOME/trademark\">obchodná známka</a> "
"organizácie Software in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Stránku je možné zobraziť tiež v&nbsp;týchto jazykoch:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "Ako nastaviť <a href=m4_HOME/intro/cn>implicitný jazyk</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr ""

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr ""

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Debian a jazyky"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Partneri"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Týždenník Debianu"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Týždenník"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Novinky projektu Debian"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Novinky projektu"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Informácie o vydaniach"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Balíky Debianu"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Stiahnuť"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;na&nbsp;CD"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Knihy o Debiane"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "Debian Wiki"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Archívy konferencií"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Konferencie"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Spoločenská zmluva"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Zásady správania sa"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - Univerzálny operačný systém"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Mapa webstránok Debianu"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Databáza vývojárov"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "Často kladené otázky o Debiane"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Príručka politiky Debianu"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Príručkka vývojárov"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Príručka nových správcov balíkov"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Chyby kandidátskych vydaní (RC)"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Správy Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Archívy používateľských konferencií"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Archívy vývojárskych konferencií"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Archívy 118n/l10n konferencií"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Archívy konferencií portov"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Archívy konferencií Systému sledovania chýb (BTS)"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Archívy ostatných konferencií"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Free software"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Vývoj"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Pomôžte Debianu"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Hlásenie&nbsp;chýb"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Porty/architektúry"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Inštalačná príručka"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Predajcovia&nbsp;CD"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "Obrazy CD/USB"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Sieťová inštalácia"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Predinštalované"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Projekt Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
#, fuzzy
#| msgid "Alioth &ndash; Debian GForge"
msgid "Salsa &ndash; Debian Gitlab"
msgstr "Alioth &ndash; Debian GForge"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Zaistenie kvality"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Systém sledovania balíkov"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Prehľad balíkov Vývojárov Debianu"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Debian"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Tento rok žiadne položky."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "navrhované"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "v diskusii"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "hlasovanie prebieha"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "skončené"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "stiahnuté"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Budúce udalosti"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Minulé udalosti"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(nová revízia)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "Správa"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr ""

#: ../../english/template/debian/redirect.wml:14
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s pre %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Pozn.:</em> <a href=\"$link\">Pôvodný dokument</a> je novší ako tento "
"preklad."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Upozornenie! Tento preklad je zastaralý, pozrite si prosím <a href=\"$link"
"\">pôvodný dokument</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>Pozn.:</em> Pôvodný dokument tohto prekladu už neexistuje."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Späť na stránku <a href=\"../\">Kto používa Debian?</a>."

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s  &ndash; %s, Verzia %s: %s"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s  &ndash; %s: %s"

#~ msgid "%s days in adoption."
#~ msgstr "adoptovaný pre %s dňami."

#~ msgid "%s days in preparation."
#~ msgstr "v príprave %s dní."

#~ msgid "&middot;"
#~ msgstr "&middot;"

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr ""
#~ "Sú dostupné <a href=\"../../\">staršie vydania</a> tohto týždenníka."

#~ msgid "<get-var url /> (dead link)"
#~ msgstr "<get-var url /> (nefunkčný odkaz)"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Koordinátor</th><th>Projektu</th>"

#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "<void id=\"dc_artwork\" />Umenie"

#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "<void id=\"dc_download\" />Stiahnuť"

#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "<void id=\"dc_mirroring\" />Zrkadlá"

#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "<void id=\"dc_misc\" />Rôzne"

#~ msgid "<void id=\"dc_pik\" />Pseudo Image Kit"
#~ msgstr "<void id=\"dc_pik\" />Sada pseudo obrazov"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />Info o vydaniach obrazov"

#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "<void id=\"dc_rsyncmirrors\" />Zrkadlá rsync"

#~ msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgstr "<void id=\"dc_torrent\" />Stiahnuť pomocou torrent"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />faq"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "<void id=\"misc-bottom\" />rozličné"

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Týždenník Debianu (DWN) vydávajú <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr "<void id=\"plural\" />Preložili ho %s."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Toto vydanie Týždenníka Debianu (DWN) vydal <a href="
#~ "\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr "<void id=\"pluralfemale\" />Preložili ho %s."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Týždenník Debianu (DWN) vydáva <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr "<void id=\"singular\" />Preložil ho %s."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Toto vydanie Týždenníka Debianu (DWN) vydal <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr "<void id=\"singularfemale\" />Preložila ho %s."

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Späť na stránku <a href=\"./\">Konzultantov Debianu</a>."

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Späť na stránku <a href=\"./\">Zoznam prednášajúcich o Debiane</a>."

#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr ""
#~ "Späť na: ďalšie <a href=\"./\">novinky Debianu</a> || <a href=\"m4_HOME/"
#~ "\">úvodnú stránku projektu Debian</a>."

#~ msgid "Buy CDs or DVDs"
#~ msgstr "Kúpiť CD alebo DVD"

#~ msgid "DFSG"
#~ msgstr "DFSG"

#~ msgid "DFSG FAQ"
#~ msgstr "DFSG FAQ"

#~ msgid "DLS Index"
#~ msgstr "Index DLS"

#~ msgid "Date"
#~ msgstr "Dátum"

#~ msgid "Date published"
#~ msgstr "Dátum zverejnenia"

#~ msgid "Debate"
#~ msgstr "Diskusia"

#~ msgid "Debian CD team"
#~ msgstr "Debian CD tím"

#~ msgid "Debian Involvement"
#~ msgstr "Zapojenie sa do Debianu"

#~ msgid "Debian-Legal Archive"
#~ msgstr "Archív Debian-Legal"

#~ msgid "Decided"
#~ msgstr "Rozhodnuté"

#~ msgid "Discussion"
#~ msgstr "Diskusia"

#~ msgid "Download calendar entry"
#~ msgstr "Stiahnuť záznam kalendára"

#~ msgid "Download via HTTP/FTP"
#~ msgstr "Stiahnuť pomocou HTTP/FTP"

#~ msgid "Download with Jigdo"
#~ msgstr "Stiahnuť pomocou Jigdo"

#~ msgid ""
#~ "English-language <a href=\"/MailingLists/disclaimer\">public mailing "
#~ "list</a> for CDs/DVDs:"
#~ msgstr ""
#~ "Anglická <a href=\"/MailingLists/disclaimer\">verejná konferencia</a> o "
#~ "CD/DVD:"

#~ msgid "Free"
#~ msgstr "Slobodný"

#~ msgid "Have you found a problem with the site layout?"
#~ msgstr "Narazili ste na problém so vzhľadom stránky?"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "Diskutuje&nbsp;sa"

#~ msgid "Justification"
#~ msgstr "Zdôvodnenie"

#~ msgid "Latest News"
#~ msgstr "Posledné novinky"

#~ msgid "License"
#~ msgstr "Licencia"

#~ msgid "License Information"
#~ msgstr "Licenčné informácie"

#~ msgid "License text"
#~ msgstr "License text"

#~ msgid "License text (translated)"
#~ msgstr "Text licencie (preložený)"

#~ msgid "List of Consultants"
#~ msgstr "Zoznam konzultantov"

#~ msgid "List of Speakers"
#~ msgstr "Zoznam prednášajúcich"

#~ msgid "Main Coordinator"
#~ msgstr "Hlavný koordinátor"

#~ msgid "Minimum Discussion"
#~ msgstr "Minimálna diskusia"

#~ msgid "More Info"
#~ msgstr "Ďalšie informácie"

#~ msgid "More information"
#~ msgstr "Ďalšie informácie"

#~ msgid "More information:"
#~ msgstr "Ďalšie informácie:"

#~ msgid "Network Install"
#~ msgstr "Sieťová inštalácia"

#~ msgid "No Requested packages"
#~ msgstr "Žiadne Žiadané balíky"

#~ msgid "No help requested"
#~ msgstr "Nevyžaduje sa žiadna pomoc"

#~ msgid "No orphaned packages"
#~ msgstr "Žiadne osirotené balíky"

#~ msgid "No packages waiting to be adopted"
#~ msgstr "Žiadne balíky čakajúce na adopciu"

#~ msgid "No packages waiting to be packaged"
#~ msgstr "Žiadne balíky čakajúce na zabalenie"

#~ msgid "No requests for adoption"
#~ msgstr "Žiadne žiadosti o adopciu"

#~ msgid "Nobody"
#~ msgstr "Nikto"

#~ msgid "Nominations"
#~ msgstr "Nominácie"

#~ msgid "Non-Free"
#~ msgstr "Neslobodný"

#~ msgid "Not Redistributable"
#~ msgstr "Nešíriteľný"

#~ msgid "Original Summary"
#~ msgstr "Pôvodné zhrnutie"

#~ msgid "Other"
#~ msgstr "Iné"

#~ msgid "Outcome"
#~ msgstr "Výsledok"

#~ msgid "Platforms"
#~ msgstr "Platformy"

#~ msgid "Rating:"
#~ msgstr "Hodnotenie:"

#~ msgid "Related Links"
#~ msgstr "Súvisiace odkazy"

#~ msgid "Report it!"
#~ msgstr "Nahláste ho!"

#~ msgid ""
#~ "See the <a href=\"./\">license information</a> page for an overview of "
#~ "the Debian License Summaries (DLS)."
#~ msgstr ""
#~ "Prehľad Zhrnutí licencií Debianu (DLS - Debian License Summaries) nájdete "
#~ "na stránke <a href=\"./\">license information</a>."

#~ msgid ""
#~ "See the Debian <a href=\"m4_HOME/contact\">contact page</a> for "
#~ "information on contacting us."
#~ msgstr ""
#~ "Ak nás chcete kontaktovať, pozrite se najskôr na <a href=\"m4_HOME/contact"
#~ "\">kontaktnú stránku</a>."

#~ msgid "Select a server near you: &nbsp;"
#~ msgstr "Zvoľte si najbližší server: &nbsp;"

#~ msgid "Summary"
#~ msgstr "Zhrnutie"

#~ msgid "Taken by:"
#~ msgstr "Rezervoval si:"

#~ msgid ""
#~ "The original summary by <summary-author/> can be found in the <a href="
#~ "\"<summary-url/>\">list archives</a>."
#~ msgstr ""
#~ "Pôvodné zhrnutie od <summary-author/> nájdete v <a href=\"<summary-url/>"
#~ "\">archívoch konferencie</a>."

#~ msgid "This summary was prepared by <summary-author/>."
#~ msgstr "Toto zhrnutie pripravil <summary-author/>."

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"http://lists."
#~ "debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
#~ msgstr ""
#~ "Ak chcete dostávať tento týždenník emailom, <a href=\"http://lists.debian."
#~ "org/debian-news/\">prihláste sa do konferencie debian-news</a>."

#, fuzzy
#~ msgid ""
#~ "To report a problem with the web site, please e-mail our publicly "
#~ "archived mailing list <a href=\"mailto:debian-www@lists.debian.org"
#~ "\">debian-www@lists.debian.org</a> in English.  For other contact "
#~ "information, see the Debian <a href=\"m4_HOME/contact\">contact page</a>. "
#~ "Web site source code is <a href=\"https://salsa.debian.org/webmaster-team/"
#~ "webwml\">available</a>."
#~ msgstr ""
#~ "Ak chcete nahlásiť problém týkajúci sa webových stránok, pošlite e-mail "
#~ "na adresu <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
#~ "debian.org</a> našej konferencie, ktorej archív je verejný. Ďalšie "
#~ "kontakty získate na stránke <a href=\"m4_HOME/contact\">kontaktov</a> "
#~ "Debianu. Je k dispozícii aj <a href=\"m4_HOME/devel/website/using_cvs"
#~ "\">zdrojový kód webu Debianu</a>."

#~ msgid "Upcoming Attractions"
#~ msgstr "Najbližšie zaujímavosti"

#~ msgid "Version"
#~ msgstr "Verzia"

#~ msgid "Visit the site sponsor"
#~ msgstr "Navštívte sponzora"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Hlasovanie&nbsp;prebieha"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Čaká&nbsp;sa&nbsp;na&nbsp;sponzorov"

#~ msgid "When"
#~ msgstr "Kedy"

#~ msgid "Where"
#~ msgstr "Kde"

#~ msgid "Withdrawn"
#~ msgstr "Stiahnuté"

#~ msgid "buy"
#~ msgstr "kúpiť"

#~ msgid "debian_on_cd"
#~ msgstr "debian_on_cd"

#~ msgid "free"
#~ msgstr "slobodný"

#~ msgid "http_ftp"
#~ msgstr "http_ftp"

#~ msgid "in adoption since today."
#~ msgstr "adoptovaný od dnes."

#~ msgid "in adoption since yesterday."
#~ msgstr "adoptovaný od včera"

#~ msgid "in preparation since today."
#~ msgstr "v príprave od dnes."

#~ msgid "in preparation since yesterday."
#~ msgstr "v príprave od včera."

#~ msgid "jigdo"
#~ msgstr "jigdo"

#~ msgid "link may no longer be valid"
#~ msgstr "odkaz už nemusí byť platný"

#~ msgid "net_install"
#~ msgstr "sieťová_inštalácia"

#~ msgid "non-free"
#~ msgstr "neslobodný"

#~ msgid "not redistributable"
#~ msgstr "nešíriteľný"

#~ msgid "package info"
#~ msgstr "info o balíku"

#~ msgid "requested %s days ago."
#~ msgstr "vyžiadaný pred %s dňami."

#~ msgid "requested today."
#~ msgstr "vyžiadaný dnes."

#~ msgid "requested yesterday."
#~ msgstr "vyžiadaný včera."
