#use wml::debian::template title="Pacotes que precisam de trabalho e futuros pacotes"
#use wml::debian::toc
#include "$(ENGLISHDIR)/devel/wnpp/wnpp.data"
#use wml::debian::translation-check translation="f06611a37d96c9834f44869a72ad012156a603e4"

<define-tag toc-title-formatting endtag=required><h3>%body</h3></define-tag>

<p>Pacotes que precisam de trabalho e futuros pacotes, WNPP abreviado em inglês,
é uma lista de pacotes que precisam de novos(as) mantenedores(as) e potenciais
pacotes no Debian.
Para acompanhar de perto o status real dessas coisas, o WNPP atualmente é
operado como um pseudopacote no
<a href="https://bugs.debian.org/">sistema de acompanhamento de bugs (BTS)</a>.</p>


<hrline />

<p><a href="work_needing">Pacotes que precisam de um(a) novo(a) mantenedor(a)</a>:
</p>
<ul>
  <li>
      <a href="rfa_bypackage"><rfa_number> pacotes prontos para adoção</a>,
      ordenados por <a href="rfa_bymaint">mantenedor(a)</a>
      ou <a href="rfa_byage">idade</a>
  </li>
  <li>
      <a href="orphaned"><orphaned_number> pacotes órfãos</a>,
      ordenados por <a href="orphaned_byage">idade</a>
  </li>
  <li>
      <a href="being_adopted"><adopted_number> pacotes atualmente sendo adotados</a>,
      ordenados por <a href="being_adopted_byage">idade</a>
      ou <a href="being_adopted_byactivity">atividade</a>
  </li>
</ul>

<p>
<a href="help_requested"><help_number> pacotes para os quais ajuda foi requisitada</a>,
ordenados por <a href="help_requested_byage">idade</a>
ou <a href="help_requested_bypop">popularidade</a>
</p>

<p><a href="prospective">Futuros pacotes</a>:</p>
<ul>
  <li>
      <a href="being_packaged"><packaged_number> pacotes sendo trabalhados</a>,
      ordenados por <a href="being_packaged_byage">idade</a>
      ou <a href="being_packaged_byactivity">atividade</a>
  </li>
  <li>
      <a href="requested"><requested_number> pacotes requisitados</a>,
      ordenados por <a href="requested_byage">idade</a>
  </li>
</ul>

<p> Nota: essas listas são atualizadas seis vezes por dia; para obter
informações mais atualizadas, por favor verifique as entradas do
<a href="https://bugs.debian.org/wnpp">pseudopacote wnpp</a> no BTS.</p>

<p>Você pode pesquisar as informações acima por pacote, descrição ou tipo no
site web de <a href="https://wnpp.debian.net">pesquisa WNPP</a>.</p>

<p>Você pode navegar pelas informações acima divididas em várias categorias
(baseadas nas <a href="https://debtags.debian.org/">debtags</a>) no site web
<a href="https://wnpp-by-tags.debian.net">WNPP-por-tags</a>.</p>

<hrline />

<h2>Usando o WNPP</h2>

<toc-display />

<p>Como o WNPP usa o BTS, todo(a) desenvolvedor(a) já conhece os detalhes
técnicos, como envio de novas informações, modificação de informações ou
fechamento de solicitações pendentes. Por outro lado, para alcançar o mais alto
nível de automação, alguns procedimentos devem ser observados.</p>

<p>Para enviar novas informações, um bug deve ser arquivado contra o
<a href="https://bugs.debian.org/wnpp">pseudopacote wnpp</a> para cada
(potencial) pacote que é afetado. Por favor observe que você deve enviar apenas
um bug por pacote-fonte em vez de um bug para cada pacote binário criado a
partir de um pacote-fonte.</p>

<toc-add-entry>Adicionando novas entradas com o <q>reportbug</q></toc-add-entry>

<p>Você pode usar o <a href="https://packages.debian.org/reportbug">reportbug</a>
(<kbd>apt-get install reportbug</kbd>):</p>

<samp>
  $ reportbug --email <var>nome-de-usuário@domínio.tld</var> wnpp<br />
  Using '<var>Seu nome &lt;nome-de-usuário@domínio.tld&gt;</var>' as your from address.<br />
  Getting status for wnpp...<br />
  Querying Debian bug tracking system for reports on wnpp<br />
  (Use ? for help at prompts.)<br />
  ...<br />
</samp>

<p>Você verá uma lista de bugs relatados contra o WNPP, os quais você deve ler
para evitar um segundo relatório para o mesmo pacote.</p>
<p>Após a lista de bugs, é perguntando a você sobre o tipo de solicitação: </p>

<samp>
What sort of request is this?<br />
<br />
1 ITP  This is an <q>Intent To Package</q>. Please submit a package description<br />
       along with copyright and URL in such a report.<br />
<br />
2 O    The package has been <q>Orphaned</q>. It needs a new maintainer as soon<br />
       as possible.<br />
<br />
3 RFA  This is a <q>Request for Adoption</q>. Due to lack of time, resources,<br />
       interest or something similar, the current maintainer is asking for<br />
       someone else to maintain this package. They will maintain it in<br />
       the meantime, but perhaps not in the best possible way. In short:<br />
       the package needs a new maintainer.<br />
<br />
4 RFH  This is a <q>Request For Help</q>. The current maintainer wants to continue<br />
       to maintain this package, but he/she needs some help to do this, because<br />
       his/her time is limited or the package is quite big and needs several<br />
       maintainers.<br />
<br />
5 RFP  This is a <q>Request For Package</q>. You have found an interesting piece<br />
       of software and would like someone else to maintain it for Debian.<br />
       Please submit a package description along with copyright and URL in<br />
       such a report.<br />
<br />
Choose the request type: <br />
</samp>

<p>Após sua seleção, você será solicitado(a) a fornecer o nome do pacote:</p>

<samp>
Choose the request type: <var>x</var><br />
Please enter the proposed package name: <var>NOMEDOPACOTE</var><br />
Checking status database...<br />
</samp>

<ul>
<li><p>Se a sua solicitação for do tipo ITP (1) ou RFP (5), você será
   solicitado(a) a fornecer uma descrição curta e algumas informações sobre o
   pacote:</p>

<samp>
Please briefly describe this package; this should be an appropriate short
description for the eventual package:<br />
&gt; <var>UMA DESCRIÇÃO</var><br />
<br />
Subject: ITP: <var>NOMEDOPACOTE -- UMA DESCRIÇÃO</var><br />
Package: wnpp<br />
Version: N/A; reported 2002-01-30<br />
Severity: wishlist<br />
<br />
* Package name    : <var>NOMEDOPACOTEE</var><br />
  Version         : <var>x.y.z</var><br />
  Upstream Author : <var>Nome &lt;alguém@algum.org&gt;</var><br />
* URL             : <var>http://www.algum.org/</var><br />
* License         : <var>(GPL, LGPL, BSD, MIT/X, etc.)</var><br />
  Description     : <var>UMA DESCRIÇÃO</var><br />
<br />
<br />
-- System Information<br />
...<br />
</samp>

<p>Abaixo da linha <q>Description</q> você deve fornecer mais informações sobre
o pacote.</p></li>

<li><p>Se sua solicitação for do tipo O (2) ou RFA (3) você deve escrever o
nome do pacote.</p>

<samp>
Choose the request type: <var>x</var><br />
Please enter the name of the package: <var>NOMEDOPACOTE</var><br />
Checking status database...<br />
<br />
Subject: O: <var>NOMEDOPACOTE -- DESCRIÇÃO CURTA</var><br />
Package: wnpp<br />
Version: N/A; reported 2002-01-30<br />
Severity: normal<br />
<br />
<br />
<br />
-- System Information<br />
...<br />
</samp>

<p>Você deve adicionar algumas informações sobre a manutenção do pacote,
a situação do(a) autor(a) original do software, e talvez um motivo pelo qual você está
se desfazendo do pacote.</p></li>

</ul>

<p>Em seguida você será perguntado(a) se deseja enviar sua solicitação:</p>

<samp>
Report will be sent to Debian Bug Tracking System &lt;submit@bugs.debian.org&gt;<br />
Submit this bug report (e to edit) [Y|n|i|e|?]? <br />
</samp>


<toc-add-entry>Adicionando novas entradas por e-mail</toc-add-entry>

<p>Também é possível enviar relatórios/bugs contra o WNPP por e-mail.
O formato do envio deve ser assim: </p>

<samp>
  Para: submit@bugs.debian.org<br />
  Assunto: <var><a href="#tag-severity">[TAG (veja abaixo)]</a></var>: <var>nome do pacote</var> -- <var>descrição curta do pacote</var><br />
  <br />
  Package: wnpp<br />
  Severity: <var><a href="#tag-severity">[SEVERIDADE (veja abaixo)]</a></var><br />
  <br />
  <var>Algumas informações sobre o pacote.</var> Se for um ITP ou RFP, é
  necessário fornecer uma URL de onde o pacote (o .deb ou o fonte original)
  pode ser baixado, além de informações sobre sua licença.
</samp>

<p>As tags a serem usadas e suas gravidades correspondentes são:</p>

<table>
<colgroup span="3">
<col width="10%">
<col width="10%">
<col width="80%">
</colgroup>
<thead>
<tr>
  <th valign="top"><a name="tag-severity">TAG</a></th>
  <th valign="top">SEVERIDADE</th>
  <th valign="top">EXPLICAÇÃO</th>
</tr>
</thead>
<tbody>
<tr>
  <th valign="top"><a name="tag-o">O</a></th>
  <td valign="top"><em>normal</em></td>
  <td>Esse pacote está ficando órfão (<q>Orphaned</q>). Ele precisa de um(a)
      novo(a) mantenedor(a) assim que possível. Se o pacote tiver uma
      prioridade maior ou igual ao padrão, a gravidade deve ser definida como
      "important" (importante).
  </td>
</tr>
<tr>
  <th valign="top"><a name="tag-rfa">RFA</a></th>
  <td valign="top"><em>normal</em></td>
  <td>Isto é uma requisição de adoção (<q>Request for Adoption</q>). Devido à
      falta de tempo, recursos, interesse ou algo semelhante, o(a) atual
      mantenedor(a) está pedindo a alguém que mantenha esse pacote. Enquanto
      isso, ele(a) o manterá, mas talvez não da melhor maneira possível. Em
      resumo: o pacote precisa de um(a) novo(a) mantenedor(a).
  </td>
</tr>
<tr>
  <th valign="top"><a name="tag-rfh">RFH</a></th>
  <td valign="top"><em>normal</em></td>
  <td>Isto é uma requisição de ajuda (<q>Request For Help</q>). O(A) atual
      mantenedor(a) deseja continuar mantendo este pacote, mas precisa de
      ajuda para fazer isso, porque seu tempo é limitado ou o pacote é bastante
      grande e precisa de vários(as) mantenedores(as).
  </td>
</tr>
<tr>
  <th valign="top"><a name="tag-itp">ITP</a></th>
  <td valign="top"><em>wishlist</em></td>
  <td>Isso é uma intenção de empacotar (<q>Intent To Package</q>). Por favor
      envie uma descrição do pacote juntamente com o copyright e a URL nesse
      relatório.
  </td>
</tr>
<tr>
  <th valign="top"><a name="tag-rfp">RFP</a></th>
  <td valign="top"><em>wishlist</em></td>
  <td>Isto é uma requisição de empacotamento (<q>Request For Package</q>).
      Alguém encontrou um software interessante e gostaria que outra pessoa o
      mantivesse empacotado no Debian. Por favor envie uma descrição do software
      juntamente com o copyright e a URL nesse relatório.
  </td>
</tr>
<tr>
  <th valign="top"><a name="tag-ita">ITA</a></th>
  <td valign="top"><em>normal</em></td>
  <td>Esta é uma intenção de adotar (<q>Intent To Adopt</q>). Indica que o(a)
      remetente ou o(a) dono(a) do bug pretende se tornar o(a) mantenedor(a)
      de um pacote órfão ou entregue para adoção. <strong>Você não deve enviar
      diretamente um relatório com esta tag. Os relatórios/bugs do ITA devem ser
      convertidos dos relatórios RFA ou O.</strong> Consulte o procedimento para
      <a href="#howto-rfa">RFA</a> e <a href="#howto-o">O</a> bugs abaixo.
  </td>
</tr>

</tbody>
</table>

<p>Um exemplo de relatório de bug pode ser encontrado
<a href="https://bugs.debian.org/927076">aqui</a>.
</p>


<toc-add-entry>Removendo entradas</toc-add-entry>

<p>Os procedimentos para fechar esses bugs são os seguintes:
</p>

<table>
<colgroup span="2">
<col width="10%">
<col width="90%">
</colgroup>
<tr>
  <th valign="top"><a name="howto-o">O</a></th>
   <td><p>Se você vai adotar um pacote, altere o título do bug para substituir o
       <q>O</q> por <q>ITA</q>, para que outras pessoas saibam que o pacote
       está sendo adotado e se evite sua remoção automática do repositório, e
       defina-se como o(a) dono(a) (owner) do bug. Para realmente adotar o
       pacote, faça o upload com o seu nome no campo Maintainer: e coloque algo
       como
      <code>
      * New maintainer (Closes: #<var>número-do-bug</var>)
      </code>
      no changelog do pacote para fechar automaticamente esse bug assim que o
      pacote for aceito no repositório; onde <var>número-do-bug</var> deve ser
      substituído pelo número do relatório do bug correspondente.
      Além disso, antes de realmente fazer o upload de uma nova versão do pacote
      com você como mantenedor(a), verifique se há uma nova versão do(a)
      autor(a) original e tente corrigir os bugs pendentes.</p>
  </td>
</tr>
<tr>
  <th valign="top"><a name="howto-rfa">RFA</a></th>
  <td><p>Se você estiver adotando um pacote, altere o título do bug para
      substituir <q>RFA</q> por <q>ITA</q>, para que outras pessoas saibam que
      o pacote está sendo adotado e para impedir sua remoção automática do
      repositório, e defina-se como o(a) dono(a) (owner) do bug. Para
      realmente adotar o pacote, faça o upload com o seu nome no campo
      Maintainer: e feche esse bug quando o pacote for aceito no repositório.
      </p>

      <p>Se você como mantenedor(a) do pacote decidir tornar órfão o pacote que
      marcou com <q>RFA</q>, por favor altere o título do relatório de bug e
      substitua <q>RFA</q> por <q>O</q>. Se você desistir da sua solicitação,
      por favor feche o bug.</p>
  </td>
</tr>
<tr>
  <th valign="top"><a name="howto-rfh">RFH</a></th>
  <td><p>Normalmente esse bug deve ser fechado apenas pelo(a) remetente, ou
      seja, pelo(a) mantenedor(a) do pacote, se o considerar obsoleto, porque
      uma ou mais pessoas ofereceram (e forneceram) seu suporte, ou porque agora
      acredita que podem lidar com o pacote sozinho(a).
      </p>

      <p>Se você, como mantenedor(a) do pacote, decide alterar o <q>RFH</q> para
      uma solicitação de adoção (<q>RFA</q>) ou se deseja tornar o pacote órfão
      (<q>O</q>), por favor altere o título do bug em vez de fechá-lo e
      registrar um novo bug.</p>
  </td>
</tr>
<tr>
  <th valign="top"><a name="howto-itp">ITP</a></th>
  <td><p>Empacote o software, faça o upload e feche esse bug assim que o pacote
      for aceito no repositório.
      </p>

      <p>Se você mudar de ideia e não quiser mais empacotar o software, feche
      o bug ou altere o título reclassificando-o como <q>RFP</q>, como achar melhor.</p>

      <p>Se você tiver problemas para empacotar o software (por exemplo, ele
      depende de outro software que ainda não foi empacotado e que você não tem
      tempo para empacotá-lo), convém registrar esses problemas como informações
      adicionais no <q>ITP</q>, para que fique claro o que está acontecendo com seus
      esforços de empacotamento.</p>
  </td>
</tr>
<tr>
  <th valign="top"><a name="howto-rfp">RFP</a></th>
  <td><p>Se você pretende empacotar esse software, altere o título do relatório
      de bug para substituir <q>RFP</q> por <q>ITP</q>, para que outras pessoas
      saibam que o software já está sendo empacotado e defina-se como o(a)
      dono(a) (owner) do bug.
      Em seguida, empacote o software, faça o upload e feche esse bug assim que
      o pacote for aceito no repositório.</p>
  </td>
</tr>
</table>

<p> Se você acha que a lista de discussão dos(as) desenvolvedores(as) deve
saber sobre seu  <q>ITP</q>,  <q>RFA</q> ou qualquer outra coisa, adicione o
cabeçalho</p>
<pre>X-Debbugs-CC: debian-devel@lists.debian.org</pre>
<p>a mensagem.</p>

<p>Naturalmente, a maneira mais fácil de fechar esses bugs é incluir uma
entrada no changelog do pacote dizendo o que você fez e adicionar
<tt>(closes:&nbsp;bug#nnnnn)</tt> nele. Dessa forma o bug será fechado
automaticamente após o novo pacote ser aceito no repositório.</p>

<p>
<strong>Atenção:</strong> se você precisar reatribuir, mudar o título ou
definir o(a) dono(a) dos relatórios de bugs, você deve fazê-lo enviando um
e-mail para o bot de controle do BTS
<a href="$(HOME)/Bugs/server-control">diretamente</a>, ou enviando por e-mail o
número do relatório @bugs.debian.org e usando
<a href="$(HOME)/Bugs/Reporting#control">pseudocabeçalhos Control</a>,
e <strong>não</strong> enviando novos relatórios de bugs.
</p>

<p> Nota: se um pacote permanecer órfão por muito tempo, examinaremos a
situação para determinar se o pacote ainda é necessário &mdash; se não for,
os(as) mantenedores(as) do FTP serão solicitados(as) a remover o pacote do
unstable (instável).</p>

<p>Se por algum motivo você precisar entrar em contato com os(as)
mantenedores(as) do WNPP, eles(as) podem ser encontrados(as) em
<email wnpp@debian.org>.</p>
