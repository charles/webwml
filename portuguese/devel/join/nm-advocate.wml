#use wml::debian::template title="Advogando a favor de um(a) futuro(a) membro(a"
#use wml::debian::translation-check translation="848f05ee39ce74ed77b065b215fbbefc6aa4532c"

<p>Observação: "advogado(a)" no contexto do Debian não é o(a) profissional da
advocacia, mas alguém que "advoga" a favor de um(a) candidato(a) de maneira não
profissional.</p>

<p> Antes de advogar a favor de um(a) futuro(a) membro(a), verifique se ele(a)
satisfaz todas as coisas listadas no
<a href="./nm-checklist">checklist NM</a>.
</p>

<p>Você deve <em>apenas</em> advogar alguém se achar que ele(a) está pronto(a)
para ser um(a) membro(a) do projeto. Isso significa que você deve
conhecê-lo(a) há algum tempo e pode julgar o seu trabalho. É importante que
o(a) futuro(a) membro(a) trabalhe no projeto há algum tempo. Algumas maneiras
pelas quais o(a) futuro(a) membro(a) pode contribuir incluem: </p>

<ul>
<li>mantendo um pacote (ou vários),</li>
<li>ajudando usuários(as),</li>
<li>trabalhando na publicidade,</li>
<li>fazendo traduções,</li>
<li>organizando a DebConf,</li>
<li>enviando patches,</li>
<li>contribuindo em relatórios de bugs,</li>
<li>... e muito mais!</li>
</ul>

<p>Essas formas de ajudar não são mutuamente exclusivas, é claro! Muitos(as)
futuros(as) membros(as) trabalharão em várias áreas do projeto e nem
todos(as) serão empacotadores(as). Lembre-se,
<a href="https://www.debian.org/vote/2010/vote_002">o Debian dá boas-vindas
a contribuidores(as) não empacotadores(as) como membros(as) do projeto</a>.
Para uma visão geral mais ampla de um(a) determinado(a) contribuidor(a), você
pode estar interessado em <a href="http://ddportfolio.debian.net/">ferramentas
que agregam algumas partes visíveis publicamente do projeto</a>. Seu
conhecimento pessoal do(a) candidato(a) também é importante aqui.
</p>

<p>O(A) futuro(a) membro(a) deve estar familiarizado(a) com as maneiras
especiais de fazer coisas do Debian e já deve ter contribuído ativamente e
efetivamente para o projeto. Mais importante, ele(a) deve estar comprometido(a)
com os ideais e a estrutura do projeto Debian. Pergunte a si mesmo(a) se você
deseja vê-lo(a) no Debian &ndash; se você acha que ele(a) deve ser um(a)
desenvolvedor(a) Debian, vá em frente e incentive-o(a) a se candidatar.
</p>

<p>Em seguida execute os seguintes passos: concorde em recomendar o(a) futuro(a)
membro(a) e deixe ele(a) se
<a href="https://nm.debian.org/public/newnm">inscrever</a>. Depois disso,
você deve clicar no nome dele(a) na
<a href="https://nm.debian.org/process/">lista de candidatos(as)</a> e acessar
a página <q>advogue em favor dessa candidatura</q>. Digite seu login Debian e
pressione o botão <q>Advocate</q>. Você receberá um e-mail com uma chave de
autenticação, a qual deverá enviar uma resposta assinada com OpenPGP. Depois
disso, o(a) candidato(a) será adicionado(a) à fila de espera por um(a) gestor
de candidatura com quem ele(a) passará por todas as etapas das verificações de
novos(as) membros(as).
</p>

<p>
Veja também a página wiki com
<a href="https://wiki.debian.org/FrontDesk/AdvocacyTips">dicas para advogar</a>.
</p>
