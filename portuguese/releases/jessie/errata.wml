#use wml::debian::template title="Debian 8 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="ff4acdb89311338bebdbb71b28dcc479d29029e3"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemas conhecidos</toc-add-entry>
<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>A equipe de segurança do Debian emite atualizações para pacotes na versão
estável (stable), nos quais eles(as) identificaram problemas relacionados à
segurança. Por favor consulte as <a href="$(HOME)/security/">páginas de segurança</a>
para obter informações sobre quaisquer problemas de segurança identificados no
<q>jessie</q>.</p>

<p>Se você usa o APT, adicione a seguinte linha ao <tt>/etc/apt/sources.list</tt>
para poder acessar as atualizações de segurança mais recentes:</p>

<pre>
  deb http://security.debian.org/ jessie/updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>

<toc-add-entry name="pointrelease">Lançamentos pontuais</toc-add-entry>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão já lançada é atualizada. Geralmente, as atualizações são indicadas
como lançamentos pontuais.</p>

<ul>
  <li>A primeira versão pontual, 8.1, foi lançada em
      <a href="$(HOME)/News/2015/20150606">6 de junho de 2015</a>.</li>
  <li>A segunda versão pontual, 8.2, foi lançada em
      <a href="$(HOME)/News/2015/20150905">5 de setembro de 2015</a>.</li>
  <li>A terceira versão pontual, 8.3, foi lançada em
      <a href="$(HOME)/News/2016/20160123">23 de janeiro de 2016</a>.</li>
  <li>A quarta versão pontual, 8.4, foi lançada em
      <A href="$(HOME)/News/2016/20160402">2 de abril de 2016</a>.</li>
  <li>A quinta versão pontual, 8.5, foi lançada em
      <A href="$(HOME)/News/2016/20160604">4 de junho de 2016</a>.</li>
  <li>A sexta versão pontual, 8.6, foi lançada em
      <A href="$(HOME)/News/2016/20160917">17 de setembro de 2016</a>.</li>
  <li>A sétima versão pontual, 8.7 was released on
      <A href="$(HOME)/News/2017/20170114">14 de janeiro de 2017</a>.</li>
  <li>A oitava versão pontual, 8.8, foi lançada em
      <A href="$(HOME)/News/2017/20170506">6 de maio de 2017</a>.</li>
  <li>A nona versão pontual, 8.9, foi lançada em
      <A href="$(HOME)/News/2017/2017072202">22 de julho de 2017</a>.</li>
  <li>A décima versão pontual, 8.10, foi lançada em
      <a href="$(HOME)/News/2017/20171209">9 de dezembro de 2017</a>.</li>
 <li>A décima primeira versão pontual, 8.11, foi lançada em
       <a href="$(HOME)/News/2018/20180623">23 de junho de 2018</a>,</li>
</ul>

<ifeq <current_release_jessie> 8.0 "

<p>Ainda não há lançamentos pontuais para o Debian 8.</p>" "

<p>Consulte o <a
href=http://http.us.debian.org/debian/dists/jessie/ChangeLog>\
ChangeLog</a>
para ver mais detalhes sobre as mudanças entre o 8.0 e o <current_release_jessie/>.</p>"/>

<p>As correções na versão estável (stable) lançada geralmente passam por
um longo período de testes antes de serem aceitas no repositório.
No entanto, essas correções estão disponíveis no repositório
<a href="http://ftp.debian.org/debian/dists/jessie-proposed-updates/">\
dists/jessie-proposed-updates</a>
de qualquer espelho do repositório Debian.</p>

<p>Sevocê usa o APT para atualizar seus pacotes, poderá instalar as
atualizações propostas adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# Atualizações propostas para a versão 8
  deb http://ftp.us.debian.org/debian jessie-proposed-updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt-get update</kbd> seguido por
<kbd>apt-get upgrade</kbd>.</p>

<toc-add-entry name="installer">Sistema de instalação</toc-add-entry>

<p>
Para obter informações sobre erratas e atualizações do sistema de instalação,
consulte a página de informações de instalação.
</p>
