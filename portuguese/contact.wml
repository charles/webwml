#use wml::debian::template title="Como entrar em contato conosco" NOCOMMENTS="yes"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#generalinfo">Informações gerais</a></li>
  <li><a href="#installuse">Instalando e usando o Debian</a></li>
  <li><a href="#press">Publicidade &amp; Imprensa</a></li>
  <li><a href="#events">Eventos &amp; Conferências</a></li>
  <li><a href="#helping">Ajudando o Debian</a></li>
  <li><a href="#packageproblems">Relatando problemas em pacotes Debian</a></li>
  <li><a href="#development">Desenvolvimento Debian</a></li>
  <li><a href="#infrastructure">Problemas com a infraestrutura Debian</a></li>
  <li><a href="#harassment">Problemas com assédio</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Pedimos gentilmente a você
que envie inicialmente perguntas em <strong>inglês</strong>, pois esse é o
idioma que a maioria de nós fala. Se isso não for possível, por favor peça ajuda
em nossa
<a href="https://lists.debian.org/debian-user-portuguese/">listas de discussão de usuários(as)</a>,
que está disponível em português.</p>
</aside>

<p>
O Debian é uma grande organização e há muitas maneiras de entrar em contato
com membros(as) do projeto e outros(as) usuários(as) Debian. Essa página resume
os meios frequentemente requisitados de contato; não é de forma alguma exaustiva.
Por favor, consulte o restante das páginas web para outros métodos de
contato.
</p>

<p>
Por favor, note que a maioria dos endereços de e-mail abaixo representa listas
de discussão abertas, com arquivos públicos. Leia o
<a href="$(HOME)/MailingLists/disclaimer">aviso</a> antes de enviar quaisquer
mensagens.
</p>

<h2 id="generalinfo">Informações gerais</h2>

<p>
A maioria das informações sobre o Debian é coletada no nosso site web,
<a href="$(HOME)">https://www.debian.org</a>, então, por favor, navegue e
<a href="$(SEARCH)">pesquise</a> pelo site antes de nos contatar.
</p>

<p>
Você pode enviar perguntas sobre o projeto Debian para a lista de discussão
<email debian-project@lists.debian.org>. Por favor, não faça perguntas gerais
sobre Linux nesta lista; continue lendo para obter mais informações sobre outras
listas de discussão.
</p>

<p>
Para perguntas sobre o projeto Debian no Brasil, se inscreva na lista de
discussão
<a href="https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-geral"><em>debian-br-geral</em></a>
e depois envie mensagens para
<email debian-br-geral@alioth-lists.debian.net>.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">Leia a nossa FAQ</a></button></p>

<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Instalando e usando o Debian</h2>

<p>
Se você tem certeza que a documentação na mídia de instalação e no nosso
site web não tem uma solução para seu problema, há uma lista de discussão de
usuários(as) muito ativa onde usuários(as) do Debian e os(as)
desenvolvedores(as) podem responder às suas perguntas.
Aqui você pode fazer todos os tipos de perguntas sobre os seguintes tópicos:
</p>

<ul>
  <li>Instalação</li>
  <li>Configuração</li>
  <li>Hardware suportado</li>
  <li>Administração de máquina</li>
  <li>Uso do Debian</li>
</ul>

<p>
Por favor <a href="https://lists.debian.org/debian-user/">se inscreva</a> na
na lista e mande as suas dúvidas para <email debian-user@lists.debian.org>.
</p>

<p>
Adicionalmente, há listas de discussão de usuários(as) para falantes de
diversos idiomas. Veja as informações de inscrição para
<a href="https://lists.debian.org/users.html#debian-user">listas de discussão internacionais</a>.
</p>

<p>
Para participar da nossa lista de discussão em português, se inscreva na lista
<a href="https://lists.debian.org/debian-user-portuguese"><em>debian-user-portuguese</em></a>
e depois envie mensagens para
<email debian-user-portuguese@lists.debian.org>.
</p>

<p>
Você pode navegar em nosso
<a href="https://lists.debian.org/">arquivo das listas de discussão</a> ou
<a href="https://lists.debian.org/search.html">pesquisar </a> os arquivos sem
necessidade de inscrição.
</p>

<p>
Se você acha que descobriu um bug no nosso sistema de instalação, por favor
escreva para a lista de discussão <email debian-boot@lists.debian.org>.
Você também pode enviar um
<a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">relatório de bug</a>
contra o pseudopacote
<a href="https://bugs.debian.org/debian-installer">debian-installer</a>.
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">Publicidade &amp; Imprensa</h2>

<p>
A <a href="https://wiki.debian.org/Teams/Publicity">equipe de publicidade do Debian</a>
modera as notícias e os anúncios em todos os meios oficiais do Debian. Por
exemplo, em algumas listas de discussão, no blog, no site web micronews e nos
canais das redes sociais oficiais.
</p>

<p>
Se você está escrevendo sobre o Debian e precisa de ajuda ou informações, por
favor entre em contato com a
<a href="mailto:press@debian.org">equipe de publicidade</a>. Este também é o
endereço certo se você planeja enviar notícias para nossa própria página de
notícias.
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">Eventos &amp; Conferências</h2>

<p>
Por favor envie convites para
<a href="$(HOME)/events/">conferências</a>, exibições ou outros eventos para
<email events@debian.org>.
</p>

<p>
Para eventos realizados no Brasil, se inscreva na lista de discussão
<a href="https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-eventos"><em>debian-br-eventos</em></a>
e depois envie mensagens para
<email debian-br-eventos@alioth-lists.debian.net>.
</p>

<p>
Pedidos de folhetos, pôsteres e participação na Europa devem ser enviados para
a lista de discussão <email debian-events-eu@lists.debian.org>.
</p>

<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">Ajudando o Debian</h2>

<p>
Existem muitas possibilidades para apoiar o projeto Debian e contribuir para a
distribuição. Se quiser oferecer ajuda, por favor visite antes nossa
página <a href="devel/join/">como ingressar no Debian</a>.
</p>

<p>
Se deseja manter um espelho do Debian, veja as páginas sobre
<a href="mirror/">espelhos do Debian</a>. Novos espelhos são cadastrados usando
<a href="mirror/submit">este formulário</a>. Problemas com espelhos existentes
podem ser enviados para <email mirrors@debian.org>.
</p>

<p>
Se quiser vender CDs/DVDs do Debian, veja as
<a href="CD/vendors/info">informações para vendedores(as) de CD</a>. Para
aparecer na lista de distribuidores(as) de CD, por favor, use
<a href="CD/vendors/adding-form">este formulário</a>.
</p>

<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">Relatando problemas em pacotes Debian</h2>

<p>
Se deseja registrar um bug contra um pacote Debian, temos um sistema
de acompanhamento de bugs onde você pode facilmente relatar seu problema. Por
favor, leia as
<a href="Bugs/Reporting">instruções para enviar relatórios de bugs</a>.
</p>

<p>
Se você quer simplesmente se comunicar com o(a) mantenedor(a) de um pacote
Debian, use o endereço de e-mail especial configurado
para cada pacote. Qualquer e-mail enviado para
&lt;<var>nome do pacote</var>&gt;@packages.debian.org será encaminhado para
o(a) mantenedor(a) responsável por esse pacote.
</p>

<p>
Se quiser avisar aos(às) desenvolvedores(as) de um problema de
segurança no Debian de uma maneira discreta, envie um e-mail para
<email security@debian.org>.
</p>

<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">Desenvolvimento do Debian</h2>

<p>
Se você tem perguntas sobre o desenvolvimento do Debian, temos várias
<a href="https://lists.debian.org/devel.html">listas de discussão</a> para isso.
Aqui você pode entrar em contato com nossos(as) desenvolvedores(as).
</p>

<p>
Existe também uma lista de discussão geral para desenvolvedores(as):
<email debian-devel@lists.debian.org>.
Por favor, siga este <a href="https://lists.debian.org/debian-devel/">link</a>
para se inscrever nela.
</p>

<p>
Para conversar com desenvolvedores(as) em português, se inscreva na lista de
discussão
<a href="https://lists.debian.org/debian-devel-portuguese/"><em>debian-devel-portuguese</em></a>
e depois envie mensagens para
<email debian-devel-portuguese@lists.debian.org>.
</p>

<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Problemas com a infraestrutura do Debian</h2>

<p>
Para relatar um problema em um serviço do Debian, você pode normalmente
<a href="Bugs/Reporting">reportar um bug</a> contra o
<a href="Bugs/pseudo-packages">pseudopacote</a> apropriado.
</p>

<p>
Alternativamente, você pode mandar um e-mail para um dos seguintes endereços:
</p>

<define-tag btsurl>package: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>Editores(as) das páginas web</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>Web pages translators</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>Administradores(as) das listas de discussão e mantenedores(as) dos arquivos</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org></dd>
<dt>Administradores(as) do sistema de acompanhamento de bugs</dt>
  <dd><btsurl bugs.debian.org><br />
      <email owner@bugs.debian.org></dd>
</dl>

<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">Problemas com assédio</h2>

<p>
O Debian é uma comunidade de pessoas que valoriza o respeito. Se você for
vítima de comportamento inadequado, se alguém do projeto feriu você ou se você
sentir que foi assediado(a), entre em contato com a equipe Comunidade:
<email community@debian.org>
</p>
